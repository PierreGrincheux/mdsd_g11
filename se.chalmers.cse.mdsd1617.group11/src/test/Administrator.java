package test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.EList;
import org.junit.Test;
import org.junit.*;

import se.chalmers.cse.mdsd1617.group11.Admin.*;
import se.chalmers.cse.mdsd1617.group11.Admin.impl.*;
import se.chalmers.cse.mdsd1617.group11.Room.Room;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;
import se.chalmers.cse.mdsd1617.group11.Query.*;
import se.chalmers.cse.mdsd1617.group11.Query.impl.*;

public class Administrator {
	
	AdminController admin;
	QueryController recept;
	
	@Before
	public void setUp() throws Exception {
		admin = AdminFactoryImpl.init().createAdminController();
		admin.startup(0);
		recept = QueryFactoryImpl.init().createQueryController();
	}

	@Test
	public void addRoomType() {
		// UC 2.2.1 - ADD ROOM TYPE
		// The actor provides name, price, number of beds and additional features of the	
		// room	 type. On confirmation the type is added unless a room type with the same name exists already
		
		// Adds a room and checks if attributes can be fetched and are correct
		assertTrue(admin.addRoomType("Nice room type",100.0,2, "feature"));
		EList<RoomType> listType = admin.getIadminroom().getRoomTypes();
		assertTrue(listType.get(0).getName().equals("Nice room type"));
		assertTrue(listType.get(0).getPrice() == 100.0);
		assertTrue(listType.get(0).getNbrOfBeds() == 2);
		assertTrue(listType.get(0).getFeatures().equals("feature"));
		
		// Continues in addRoomTypeWithSameName()
	}
	
	@Test
	public void addRoomTypeWithSameName(){
		// Continues from UC 2.2.1 - ADD ROOM TYPE
		// Add with a roomtype name that already exists. 
		
		int sizeBefore = admin.getIadminroom().getRoomTypes().size();
		assertTrue(admin.addRoomType("StandardRoom",100.0,2, "feature"));
		
		// Should not be able to add a roomtype with same name
		assertFalse(admin.addRoomType("StandardRoom",100.0,2, "feature"));
		
		EList<RoomType> listType = admin.getIadminroom().getRoomTypes();
		assertTrue(listType.get(0).getName().equals("StandardRoom"));
		assertTrue(admin.getIadminroom().getRoomTypes().size() == sizeBefore+1);
	}
	
	@Test
	public void upadteRoomType() {
		// UC 2.2.2 - UPDATE ROOM TYPE
		// The actor selects a room type and provides updated information (according to	
		// what	can	be	supplied in	UC	2.2.1).
		
		// Add a roomtype
		assertTrue(admin.addRoomType("Nice room type",100.0,2, "feature"));
		EList<RoomType> listType = admin.getIadminroom().getRoomTypes();
		
		// Try to update a roomType and checks if it updates
		assertTrue(admin.updateRoomType("Nice room type", "Very Nice", 4 ,150.0,"new feature"));
		listType = admin.getIadminroom().getRoomTypes();
		RoomType roomType = listType.get(0);
		assertTrue(roomType.getName().equals("Very Nice"));
		assertTrue(roomType.getPrice() == 150.0);
		assertTrue(roomType.getNbrOfBeds() == 4);
		assertTrue(roomType.getFeatures().equals("new feature"));
		
		// Try to update a roomtype that doesnt exist
		assertFalse(admin.updateRoomType("Nice", "Very Nice Room", 4 ,150.0,"new feature"));
		roomType = listType.get(0);
		assertTrue(roomType.getName().equals("Very Nice"));
		assertTrue(roomType.getPrice() == 150.0);
		assertTrue(roomType.getNbrOfBeds() == 4);
		assertTrue(roomType.getFeatures().equals("new feature"));
		assertTrue(admin.getIadminroom().getRoomTypes().size()==1);
		
		// Add second room type
		assertTrue(admin.addRoomType("Nice room type",100.0,3, "features"));
		// Rename room type to already existing roomtype should not work
		assertFalse(admin.updateRoomType("Nice room type", "Very Nice", 4 ,150.0,"new feature"));
	}
	
	@Test
	public void removeRoomType() {
		// UC 2.2.3 - REMOVE ROOM TYPE
		// The actor provides the name of the room type to remove. The room type is	
		// removed	unless a room with that type exists.
		
		//Setup roomtype
		admin.addRoomType("Nice room type",100.0,2, "featre");
		EList<RoomType> listType = admin.getIadminroom().getRoomTypes();
		RoomType temp = listType.get(0);
		
		// Try to remove a rometype that has a room, which shouldn't be possible
		admin.addRoom(temp, 10);
		assertFalse(admin.removeRoomType(temp.getName()));
		assertTrue(admin.getIadminroom().getRoomTypes().size() == 1);
		
		// Remove room
		admin.removeRoom(10);
		
		// Try to remove a roomtype that doesnt exist and checks that nothing happens
		assertFalse(admin.removeRoomType("name doest exist"));
		listType = admin.getIadminroom().getRoomTypes();
		assertTrue(listType.size() == 1);
		
		// Try to remove a roomtype with no rooms
		admin.removeRoomType(temp.getName());
		listType = admin.getIadminroom().getRoomTypes();
		assertTrue(listType.size() == 0);
	}
	
	@Test
	public void addRoom() {
		// UC 2.2.4 - ADD ROOM
		// The actor provides the (physical) room number and type of the room to add. A	
		// room	with the given number and type is created, unless a	room with the same number exists.
		
		// Setup roomtype
		admin.addRoomType("Nice room type",100.0,2, "feature");
		admin.addRoomType("Nice room",100.0,2, "feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		RoomType roomType1 = admin.getIadminroom().getRoomTypes().get(1);
		
		// Try to add a room, checks if it gets added
		assertTrue(admin.addRoom(roomType, 10));
		assertTrue(admin.getIadminroom().getRooms().size() == 1);
		
		// Try to add another room with the same roomnbr, the room should not be added
		assertFalse(admin.addRoom(roomType, 10));
		assertTrue(admin.getIadminroom().getRooms().size() == 1);
		
		// Try to add with diffrent type but same number should not work
		assertFalse(admin.addRoom(roomType1, 10));
		
		// Try to add new room with new number
		assertTrue(admin.addRoom(roomType1, 11));
		assertTrue(admin.getIadminroom().getRooms().size() == 2);
	}
	
	@Test
	public void changeRoomTypeOfRoom() {
		// UC 2.2.5 - CHANGE ROOM TYPE OF ROOM
		// The	actor provides the	number of the room. He/She then selects	a new room type
		// from	all	existing types,	not	including the room’s current type.
		
		// Setup
		admin.addRoomType("Nice room type",100.0,2, "feature");
		admin.addRoomType("Nice room",100.0,2, "feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		RoomType roomType1 = admin.getIadminroom().getRoomTypes().get(1);
		
		// Add room
		assertTrue(admin.addRoom(roomType, 10));
		RoomType rT= admin.getOtherRoomTypes(10).get(0);
		
		// Test if the roomtype listed is the one not being used
		assertTrue(rT.getName().equals(roomType1.getName()));
		
		// Try to update 
		assertTrue(admin.changeRoomTypeOfRoom(10, rT));
		// Try to update room that does not exists
		assertFalse(admin.changeRoomTypeOfRoom(11, rT));
		// Test if the roomType has been changed
		assertTrue(admin.getIadminroom().getRoomTypeOfRoom(10).getName().equals(rT.getName()) );
		
	}
	
	@Test
	public void removeRoom() {
		// UC 2.2.6 - REMOVE ROOM
		// The	actor provides the	number of the	room to remove. The	room is removed	unless	
		// a booking is currently checked into that room.
		
		// Setup
		admin.addRoomType("room",100.0,2, "feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(roomType, 10);
		
		// Check ok to remove room
		assertTrue(admin.removeRoom(10));
		EList<Room> rooms = admin.getIadminroom().getRooms();
		assertTrue(rooms.size() == 0);
		
		// Setup again
		assertTrue(admin.addRoom(roomType, 10));
		rooms = admin.getIadminroom().getRooms();
		assertTrue(rooms.size() == 1);
		assertTrue(admin.getIadminroom().getRoomTypeOfRoom(10).getName().equals("room"));
	
		// Setup booking
		int bookingID = recept.initiateBooking("Filip", "161215", "161220", "Seholm");
		assertTrue(recept.addRoomToBooking("room", bookingID));
		assertTrue(recept.confirmBooking(bookingID));
		assertTrue(recept.checkInBooking(bookingID).size() > 0);
		
		// Try to remove booked room
		assertFalse(admin.removeRoom(10));	
	}
	
	@Test
	public void blockRoom() {
		// UC 2.2.7 - BLOCK ROOM
		// The actor provides the number of the room to	block. The room is marked as
		// blocked and cannot be booked.
		
		// Setup
		admin.addRoomType("Nice room",100.0,2,"feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(roomType, 10);
		
		// Block room
		assertTrue(admin.blockRoom(10));
		Room room = admin.getIadminroom().getRooms().get(0);
		assertTrue(room.getStatus() == "Blocked");
		
		//Try book blocked room should not work
		int bookingID = recept.initiateBooking("Filip", "161215", "161220", "Seholm");
		assertFalse(recept.addRoomToBooking("Nice room", bookingID));
	}
	
	@Test
	public void unBlockRoom() {
		// UC 2.2.8 - UNBLOCK ROOM
		// The actor provides the number of the	room to	unblock. The blocked marking is	
		// removed from	the	room, which	means it is	bookable again.
		
		// Setup
		admin.addRoomType("Nice room",100.0,2,"feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(roomType, 10);
		
		// Block and check it is blocked
		admin.blockRoom(10);
		Room room = admin.getIadminroom().getRooms().get(0);
		assertTrue(room.getStatus() == "Blocked");
		
		// Unblock and check it is unblocked
		admin.unblockRoom(10);
		assertTrue(room.getStatus() == "Available");
	}
	
	@Test
	public void startUpHotelSystem() {
		// UC 2.2.9 – STARTUP HOTEL SYSTEM
		// The actor provides the number of initial rooms to create. The hotel system is
		// initialized, cleaning up	any	existing hotel information (rooms, room	types,bookings,	etc.), with
		// the given number of rooms.	
		// Note: Any room type with	exactly	two	beds can be chosen during implementation
		
		// Test zero rooms
		admin.startup(0);
		assertTrue(admin.getIadminroom().getRooms().size() == 0);
		
		// Test 100 rooms
		admin.startup(100);
		assertTrue(admin.getIadminroom().getRoomTypes().get(0).getName().equals("Standard"));
		assertTrue(admin.getIadminroom().getRoomTypes().get(0).getNbrOfBeds() == 2);
		assertTrue(admin.getIadminroom().getRooms().size() == 100);
		
		// Make booking
		int bookingID = recept.initiateBooking("Filip", "161215", "161220", "Seholm");
		assertTrue(bookingID > 0);
		recept.checkInBooking(bookingID);
		recept.confirmBooking(bookingID);
		
		// Check if resets the hotel works
		admin.startup(0);
		assertTrue(admin.getIadminroom().getRoomTypes().size() == 0);
		assertTrue(admin.getIadminroom().getRooms().size() == 0);
		assertTrue(recept.getConfirmedBookings().size() == 0);
	}
}