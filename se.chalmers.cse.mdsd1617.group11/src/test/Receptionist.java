package test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group11.Admin.AdminController;
import se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminFactoryImpl;
import se.chalmers.cse.mdsd1617.group11.Booking.Booking;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group11.Query.*;
import se.chalmers.cse.mdsd1617.group11.Query.impl.*;
import se.chalmers.cse.mdsd1617.group11.Reservation.Extra;
import se.chalmers.cse.mdsd1617.group11.Room.Room;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;
import se.chalmers.cse.mdsd1617.group11.BankingModel.*;

public class Receptionist {
	
	QueryController receptionist;
	AdminController admin;
	BankComponent bankingComponent;
	
	@Before
	public void setUp() throws Exception {
		// New admin to create room
		admin = AdminFactoryImpl.init().createAdminController();
		admin.startup(0);
		
		receptionist = QueryFactoryImpl.eINSTANCE.createQueryController();

		// Add new roomtype and a new room
		admin.addRoomType("Nice Room",100.0,2, "feature");
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(roomType, 10);

		bankingComponent = BankingModelFactory.eINSTANCE.createBankComponent();
	}
	

	@Test
	public void makeBooking() {
		//Use case 2.1.1
		//The actor provides a period of time, and a first and last name. He/she chooses the	
		//amount of rooms per room type that he/she wants to book from the free rooms during that	
		//period and then confirms the booking.
		
		//Setup
		admin.addRoom(admin.getIadminroom().getRoomTypes().get(0), 11);
		
		//Test valid input data for initiating booking, returns id>0
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(id>=0);
		int id1 = receptionist.initiateBooking("Hobbe", "161215", "161220", "Nisse");
		assertTrue(id>=0);
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getPeople().getFirstname().equals("Kalle"));
		//Tests if id is different for two different bookings, returns true
		assertFalse(id==id1);
		//Test invalid name data for initiating booking, returns -1
		int id3 = receptionist.initiateBooking(null, "161215", "161220", null);
		assertTrue(id3==(-1));
		//Tests invalid date data for initiating booking, returns -1
		int id2 = receptionist.initiateBooking("Hobbe", "161220", "161215", "Nisse");
		assertTrue(id2 == (-1));
		id2 = receptionist.initiateBooking("Hobbe", "165050", "161215", "Nisse");
		assertTrue(id2 == (-1));
		assertTrue(receptionist.getBookingcontroller().getBooking(id2)==null);
		
		//Trys to confirm a booking without added rooms, return false
		assertFalse(receptionist.confirmBooking(id));
		//Trys to confirm a booking with added room, return true
		receptionist.addRoomToBooking("Nice Room", id);
		assertTrue(receptionist.confirmBooking(id));
		//all the add to room tests are done further done
		
	}
	
	@Test
	public void searchFreeRooms() {
		//Use case 2.1.2
		//The user provides a period of time and a minimum number of beds per room. The	
		//system returns a list of room types with description, price per room, number of free rooms and	
		//number of beds.
		
		//Setup
		// Create booking with one room and confirm
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
		admin.addRoomType("Standard",150.0,2, "feature");
		admin.addRoom(admin.getIadminroom().getRoomTypes().get(1), 11);
		
		// Test get free room before any of the two rooms are booked
		assertTrue(receptionist.getFreeRooms(2, "161201", "161210").get(0).getNumFreeRooms()==1);
		assertTrue(receptionist.getFreeRooms(2, "161201", "161210").get(1).getNumFreeRooms()==1);
		
		// Test get free when one of the two rooms are booked
		assertTrue(receptionist.getFreeRooms(2, "161206", "161219").get(0).getNumFreeRooms()==0);
		assertTrue(receptionist.getFreeRooms(2, "161206", "161219").get(1).getNumFreeRooms()==1);
		
		// Test get free room after any of the two rooms are booked
		assertTrue(receptionist.getFreeRooms(2, "161201", "161210").get(0).getNumFreeRooms()==1);
		assertTrue(receptionist.getFreeRooms(2, "161201", "161210").get(1).getNumFreeRooms()==1);
		
	}
	
	@Test
	public void checkInBooking() {
		//Use case 2.1.3
		//The user provides the	booking	ID. The	system shows the available (physical) rooms	
		//to choose from according to the booking information. The actor chooses the rooms and the	
		//system marks them as occupied and links them to the booking.	
		
		// Setup
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(type, 8);
		admin.addRoomType("Standard",100.0,4, "feature");
		admin.addRoom(admin.getIadminroom().getRoomTypes().get(1),7);
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
		Booking booking = receptionist.getBookingcontroller().getBooking(id);
		assertTrue(booking != null);
		
		//checks if we get the correct rooms from getting physical rooms
		assertTrue(receptionist.getFreeRoomsForBookingID(id).size() == 2);
		int roomnr1 = receptionist.getFreeRoomsForBookingID(id).get(0);
		int roomnr2 = receptionist.getFreeRoomsForBookingID(id).get(1);
		admin.getIadminroom().getRoomTypeOfRoom(roomnr1).equals("Nice Room");
		admin.getIadminroom().getRoomTypeOfRoom(roomnr2).equals("Nice Room");
		
		// choose one of the physical rooms and Checkin booking, return true
		assertTrue(receptionist.checkInBookingWithIDAndRoomNbr(id, 10));
		//Try to checkin another room when only one room is added to the booking, return false
		assertFalse(receptionist.checkInBookingWithIDAndRoomNbr(id, 8));
		//Try to checkin a room that is another roomtype
		assertFalse(receptionist.checkInBookingWithIDAndRoomNbr(id, 7));
		
		//Check if the booking is checked in when all rooms are checked in
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getStatus() == BookingStatus.CHECKEDIN);

		// Get room and checked that it is occupied
		EList<Room> rooms = admin.getIadminroom().getRooms();
		assertTrue(rooms.get(0).getStatus().equals("Occupied"));
		assertTrue(rooms.get(1).getStatus().equals("Available"));
		assertTrue(rooms.get(2).getStatus().equals("Available"));
	}
	
	@Test
	public void checkOutBooking() {
		// UC 2.1.4 - CHECK OUT BOOKING
		// The user provides booking ID	and his/her	credit card	details. The system	makes	
		// the payment (using the external payment service), considering all room prices and added extras,
		// and checks out the room.
		
		double roomPrice = 100;
		int stayLength = 30;
		
		// Add room-type and room to be able to create booking
		assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(admin.addRoom(type, 1));
		
		// Calculate dated
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		
		// Make booking
		int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(booking != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
		assertTrue(receptionist.confirmBooking(booking));
		EList<Integer> rooms = receptionist.checkInBooking(booking);
		assertTrue(rooms != null);
		assertTrue(rooms.size() == 1);
		
		// Start checkout
		double price = receptionist.initiateCheckout(booking);
		assertTrue(price == roomPrice * stayLength);
		
		// Payment is tested in checkOutBookingAndPay()
	}
	
	@Test
	public void checkOutBookingAndPay() {
		// Part of UC 2.1.4 - CHECK OUT BOOKING
		double roomPrice = 100;
		int stayLength = 30;
		
		// Prepare a room-type and room
		assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(admin.addRoom(type, 1));
		
		// Prepare dates
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		System.out.println(from);
		System.out.println(to);
		// Make booking
		int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(booking != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
		assertTrue(receptionist.confirmBooking(booking));
		EList<Integer> rooms = receptionist.checkInBooking(booking);
		assertTrue(rooms != null);
		assertTrue(rooms.size() == 1);
		
		// Start checkout
		double price = receptionist.initiateCheckout(booking);
		System.out.println(price);
		System.out.println(roomPrice * stayLength);
		assertTrue(price == roomPrice * stayLength);
		
		// Prepare bank card
		bankingComponent.removeCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm");
		assertTrue(bankingComponent.addCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm"));
		assertTrue(bankingComponent.makeDeposit("4221652621878912","143", 03, 18, "Filip", "Seholm", 10000) ==10000);
		
		// Pay for booking
		assertTrue(receptionist.payDuringCheckout("4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
		// Pay for extra is tested in addExtraCostToRoomAndPay()
		
	}
	
	@Test
	public void editBooking() {
		//Use case 2.1.5
		//Given a booking ID, the actor	can edit the booking information according to the	
		//user’s preferences. He/she can edit the room types and/or the	number of the rooms and the	
		//period of	time.
		
		//setup
		admin.addRoomType("Nice Room aswell",150,4, "features");
		RoomType roomType1 = admin.getIadminroom().getRoomTypes().get(0);
		RoomType roomType2 = admin.getIadminroom().getRoomTypes().get(1);
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		admin.addRoom(roomType1, 9);
		admin.addRoom(roomType2, 8);
		admin.addRoom(roomType2, 7);
		admin.addRoom(roomType1, 6);
		admin.addRoom(roomType2, 5);
		assertTrue(receptionist.addRoomToBooking(roomType1.getName(), id));
		assertTrue(receptionist.addRoomToBooking(roomType1.getName(), id));
		assertTrue(receptionist.addRoomToBooking(roomType2.getName(), id));
		assertTrue(receptionist.addRoomToBooking(roomType2.getName(), id));
		//edit room type to existing room type, return true
		assertTrue(receptionist.changeRoomType(id, roomType1.getName(), roomType2.getName(), 1));
		assertTrue(receptionist.getNbrOfRooms(id, roomType1.getName())==1);
		assertTrue(receptionist.getNbrOfRooms(id, roomType2.getName())==3);
		//edit to none existing room type, return false
		assertFalse(receptionist.changeRoomType(id, roomType1.getName(), "notexisting", 1));
		assertFalse(receptionist.changeRoomType(id, "notexisting", roomType1.getName(), 1));
		assertTrue(receptionist.getNbrOfRooms(id, roomType1.getName())==1);
		assertTrue(receptionist.getNbrOfRooms(id, roomType2.getName())==3);
		//change number of rooms, return true
		assertTrue(receptionist.changeNumberOfRooms(id, roomType1,3));
		assertTrue(receptionist.getNbrOfRooms(id, roomType1.getName())==3);
		//change number of rooms to invalid format, return false
		assertFalse(receptionist.changeNumberOfRooms(id, roomType1,-1));
		assertFalse(receptionist.changeNumberOfRooms(id, null,-1));
		assertTrue(receptionist.getNbrOfRooms(id, roomType1.getName())==3);
		//change period of time, return true
		Date dateFrom = createDate("161220");
		Date dateTo = createDate("161225");
		assertTrue(receptionist.changeReservationDate(id, dateFrom, dateTo));
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getReservedFrom().equals(dateFrom));
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getReservedTo().equals(dateTo));
		//change period of time to invalid time, return false
		assertFalse(receptionist.changeReservationDate(id, dateTo, dateFrom));
		assertFalse(receptionist.changeReservationDate(id, createDate("165050"), dateFrom));
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getReservedFrom().equals(dateFrom));
		assertTrue(receptionist.getBookingcontroller().getBooking(id).getReservedTo().equals(dateTo));
		// try to edit confirmed booking, return false
		receptionist.confirmBooking(id);
		assertFalse(receptionist.changeReservationDate(id, dateFrom, dateTo));
		assertFalse(receptionist.changeNumberOfRooms(id, roomType1,5));
		assertFalse(receptionist.changeRoomType(id, roomType1.getName(), roomType2.getName(), 1));
		// try to edit checked in booking, return false
		receptionist.checkInBooking(id);
		assertFalse(receptionist.changeReservationDate(id, dateFrom, dateTo));
		assertFalse(receptionist.changeNumberOfRooms(id, roomType1,5));
		assertFalse(receptionist.changeRoomType(id, roomType1.getName(), roomType2.getName(), 1));
		// try to edit checkedOut booking, return false
		receptionist.initiateCheckout(id);
		assertFalse(receptionist.changeReservationDate(id, dateFrom, dateTo));
		assertFalse(receptionist.changeNumberOfRooms(id, roomType1,5));
		assertFalse(receptionist.changeRoomType(id, roomType1.getName(), roomType2.getName(), 1));
		
		
	}
	
	@Test
	public void cancelBooking() {
		// UC 2.1.6 - CANCEL A BOOKING
		// The user	provides a booking ID. The actor marks the rooms in	that booking as free	
		// for the	given period and marks the booking as cancelled
		
		// Create booking with one room and confirm
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
						
		Booking booking = receptionist.getBookingcontroller().getBooking(id);
		assertTrue(booking != null);
		
		// Cancel booking
		assertTrue(receptionist.cancelBooking(id));
		
		// Make sure room is free after cancel
		assertTrue(admin.getIadminroom().getRooms().get(0).getStatus().equals("Available"));
	}
	
	@Test
	public void listBookings() {
		//UC 2.1.7 - LIST BOOKINGS
		//Goal: Display	a	list	of	all	the	bookings.
		//Actor(s): Receptionist
		//Description: The	 system	 shows	 all	 the	 confirmed	 bookings	 with	 their	 respective booking	 IDs,	
		//room	types	and	number	of	rooms	per	type,	and	period	of	time
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		int id = receptionist.initiateBooking("Derp", "161110", "161201", "Alot");
		assertTrue(id != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), id));
		assertTrue(receptionist.confirmBooking(id));
		EList<ReportBookingDTO> list = receptionist.listBookings();
		assertTrue(list.size() == 1);
		assertTrue(list.get(0).getRoommapentry().containsKey(type.getName()));
		assertTrue(list.get(0).getRoommapentry().get(type.getName()) == 1);
		assertTrue(list.get(0).getFrom() != null);
		assertTrue(list.get(0).getTo() != null);
		
		//Add additional booking with 2 room types.
		assertTrue(admin.addRoomType("Delux",1000.0,2, "feature"));
		RoomType newType = admin.getIadminroom().getRoomTypes().get(1);
		assertTrue(admin.addRoom(newType, 42));
		id =  receptionist.initiateBooking("Derp", "161101", "161102", "Alot");
		assertTrue(receptionist.addRoomToBooking(type.getName(), id));
		assertTrue(receptionist.addRoomToBooking(newType.getName(), id));
		assertTrue(receptionist.confirmBooking(id));
		list = receptionist.listBookings();
		assertTrue(list.size() == 2);
		assertTrue(list.get(1).getBookingID() == id);
		assertTrue(list.get(1).getRoommapentry().keySet().size() == 2);
		assertTrue(list.get(1).getRoommapentry().keySet().contains(type.getName()));
		assertTrue(list.get(1).getRoommapentry().get(type.getName()) == 1);
		assertTrue(list.get(1).getFrom() != null);
		assertTrue(list.get(1).getTo() != null);
		
		assertTrue(list.get(1).getRoommapentry().keySet().contains(newType.getName()));
		assertTrue(list.get(1).getRoommapentry().get(newType.getName()) == 1);
		assertTrue(list.get(1).getFrom() != null);
		assertTrue(list.get(1).getTo() != null);
		
		//Add multiple rooms of same type
		assertTrue(admin.addRoom(newType, 43));
		id = receptionist.initiateBooking("Derp", "160501", "160502", "Alot");
		assertTrue(receptionist.addRoomToBooking(newType.getName(), id));
		assertTrue(receptionist.addRoomToBooking(newType.getName(), id));
		
		assertFalse(list.size() == 3);
		assertTrue(receptionist.confirmBooking(id));
		list = receptionist.listBookings();
		assertTrue(list.size() == 3);
		
		assertTrue(list.get(2).getBookingID() == id);
		assertTrue(list.get(2).getRoommapentry().keySet().size() == 1);
		assertTrue(list.get(2).getRoommapentry().keySet().contains(newType.getName()));
		assertTrue(list.get(2).getRoommapentry().get(newType.getName()) == 2);
		assertTrue(list.get(2).getFrom() != null);
		assertTrue(list.get(2).getTo() != null);
	}
	
	@Test
	public void listOccupiedRoomsForADay() {
		// UC 2.1.8 - LIST OCCUPIED ROOMS FOR SPECIFIED DAY
		// Given a date, the system	shows all occupied rooms during that day, as well as the	
		// connected booking ID for each room.
		
		Date date = new Date();
		
		// Check that there are no occupied rooms when there are no bookings
		EList<ReportDTO> list = receptionist.getOccupiedRooms(date);
		assertTrue(list.size() == 0);
		
		// Create booking
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
		
		// Check during booking
		date = this.createDate("161214");
		list = receptionist.getOccupiedRooms(date);
		assertTrue(list.size() == 0);
		
		// Check during booking
		date = this.createDate("161216");
		list = receptionist.getOccupiedRooms(date);
		assertTrue(list.size() == 1);
		assertTrue(list.get(0).getBookings() == id);
		
		// Check after booking
		date = this.createDate("161221");
		list = receptionist.getOccupiedRooms(date);
		assertTrue(list.size() == 0);
	}
	
	@Test
	public void listCheckIns() {
		// UC 2.1.9 - LIST CHECK INS FOR A SPECIFIC DAY(S)
		// The system shows	all	the	check ins that were	made at the	specified day or during a
		// specified period	of time. The dates cannot be in the	future
		
		// Prepare rooms and bookings
		double roomPrice = 100;
		int stayLength = 30;
		assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(admin.addRoom(type, 1));
		
		// Calc dates
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		Date checkFrom = cal.getTime();
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		
		// Make booking
		int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(booking != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
		assertTrue(receptionist.confirmBooking(booking));
		
		// Test get checkins before checked in
		assertTrue(receptionist.getCheckIns(checkFrom).size() == 0);
		
		// Check in booking
		EList<Integer> rooms = receptionist.checkInBooking(booking);
		assertTrue(rooms.size() == 1);
		
		// Test get checkins when checked in
		assertTrue(receptionist.getCheckIns(checkFrom).size() == 1);
		
		// Test get checkins with future dates
		Date future = createDate("170210");
		assertTrue(receptionist.getCheckIns(future).size() == 0);
	}
	
	@Test
	public void listCheckouts() {
		//Use case 2.1.10
		//The system shows all the check outs that were made at the specified day during
		//a specified period of time. The dates cannot be in the future.
		
		//Setup
		
			// Prepare rooms and bookings
			double roomPrice = 100;
			int stayLength = 30;
			assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
			RoomType type = admin.getIadminroom().getRoomTypes().get(0);
			assertTrue(admin.addRoom(type, 1));
				
			// Calc dates
			Calendar cal = Calendar.getInstance();
			String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
			cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
			Date checkFrom = cal.getTime();
			String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
				
			// Make booking
			int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
			assertTrue(booking != -1);
			assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
			assertTrue(receptionist.confirmBooking(booking));
				
			// Check in booking
			EList<Integer> rooms = receptionist.checkInBooking(booking);
			assertTrue(rooms.size() == 1);
			
		// Test list checkouts before checking out, returns false
		assertTrue(receptionist.getCheckedOutBookings(checkFrom,createDate(to)).size() == 0);
		
		// Test list checkouts after checking out with correct date, returns true
		receptionist.initiateCheckout(booking);
		assertTrue(receptionist.getCheckedOutBookings(createDate(to),createDate(to)).size() == 1);
		
		Date future = createDate("170210");
		assertTrue(receptionist.getCheckedOutBookings(future,future).size() == 0);
		
		//Test checkIn with dates that don't have a checkout, returns false
		assertTrue(receptionist.getCheckedOutBookings(createDate(from),createDate(from)).size() == 0);
			
	}
	
	@Test
	public void checkInRoom() {
		// UC 2.1.11 - CHECK IN ROOM
		// The user	provides the booking ID	and	a room type. If one	or more	rooms of that	
		// type	have been booked by	the	given booking and not yet check	in,	the	system marks one of	the	
		// free	(physical) rooms of	that type as occupied and links	it to the booking.
		// Note: The difference	to UC 2.1.3	is that	a single room is checked in, not the	entire booking. Also,	
		// physical	rooms cannot be chosen in this use	case.
		
		// Create booking with one room and confirm
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
				
		Booking booking = receptionist.getBookingcontroller().getBooking(id);
		assertTrue(booking != null);
				
		// Checkin booking
		assertTrue(receptionist.checkInRoom("Nice Room", id) > 0);
				
		// Get room and checked that it is occupied
		EList<Room> rooms = admin.getIadminroom().getRooms();
		assertTrue(rooms.get(0).getStatus().equals("Occupied"));
	}
	
	@Test
	public void checkOutAndPayRoom() {
		// UC 2.1.12 - CHECK OUT AND PAY ROOM
		// The user	provides booking ID, room number and his/her credit card details. The	
		// system makes	the	payment	(using	the	external	payment	service),	considering	the	room	price	and	
		// the	added extras for that room, and checks out the room.
		// Note: The difference	to	UC	2.1.4 is that a single room is	checked	out, not the entire	booking (and	
		// all rooms belonging	to	it).
		
		// Setting up banking system
		bankingComponent.removeCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm");
		assertTrue(bankingComponent.addCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm"));
		assertTrue(bankingComponent.makeDeposit("4221652621878912","143", 03, 18, "Filip", "Seholm", 1000000000) ==1000000000);
		
		// Setup rooms
		RoomType roomType = admin.getIadminroom().getRoomTypes().get(0);
		admin.addRoom(roomType, 8);
		
		// Setup bookings
		int stayLength = 3;
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		int id = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
		
		// Check in booking
		EList<Integer> rooms = receptionist.checkInBooking(id);
		assertTrue(rooms.size() == 2);
		int roomNo = rooms.get(0);
		
		// Pay and checkout
		double priceCalculation = (roomType.getPrice() * stayLength);
		double price = receptionist.initiateRoomCheckout(roomNo,id);
		assertTrue(price == priceCalculation);
		assertTrue(receptionist.payRoomDuringCheckout(roomNo, "4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
		// Pay for the other room and add extra
		roomNo = rooms.get(1);
		receptionist.addExtra(id, roomNo, "Chips", 20.00);
		price = receptionist.initiateRoomCheckout(roomNo, id);
		priceCalculation = (roomType.getPrice() * stayLength) + 20.00;
		assertTrue(price == priceCalculation);
		assertTrue(receptionist.payRoomDuringCheckout(roomNo, "4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
		
		//Pay for the same room again
		assertFalse(receptionist.payRoomDuringCheckout(roomNo, "4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
		//Pay for an invalid room
		assertFalse(receptionist.payRoomDuringCheckout(-1, "4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
		//Checkout a room that does not exist 
		price = receptionist.initiateRoomCheckout(-1, id);
		assertTrue(price == -1);
		
		//Checkout a booking that doesn't exist
		assertTrue(receptionist.initiateRoomCheckout(roomNo, -1) == -1);
	}
	
	@Test
	public void addExtraCostToRoom() {
		// UC 2.1.13 - ADD EXTRA COST TO ROOMS
		// The	user	provides	booking	ID,	room	number,	a	description	of	the	added	extra
		// and the	price	for	that	extra. The	system	adds	this	extra	to	the	room.
		
		// Setup
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.addRoomToBooking("Nice Room", id));
		assertTrue(receptionist.confirmBooking(id));
				
		Booking booking = receptionist.getBookingcontroller().getBooking(id);
		assertTrue(booking != null);
				
		// Checkin booking
		int roomNr = receptionist.checkInRoom("Nice Room", id);
		assertTrue(roomNr > 0);
		
		int priceForExtra = 10;
		String nameForExtra = "Chips";
		
		assertTrue(receptionist.addExtra(id, roomNr, nameForExtra, priceForExtra));
		
		Extra extra = receptionist.getBookingReservations(id).get(0).getExtra().get(0);
		
		assertTrue(extra != null);
		assertTrue(extra.getPrice() == priceForExtra && extra.getTypeOfExtra().equals(nameForExtra));
		
		// Continue in addExtraCostToRoomAndPay()
	}
	
	@Test
	public void addExtraCostToRoomAndPay() {
		// Continues UC 2.1.13 - ADD EXTRA COST TO ROOMS
		
		admin.startup(0);
		
		double roomPrice = 100;
		int stayLength = 10;
		
		// Add roomtype and room
		assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(admin.addRoom(type, 1));
		
		// Calculate dates
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		
		// Make booking
		int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(booking != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
		assertTrue(receptionist.confirmBooking(booking));
		EList<Integer> rooms = receptionist.checkInBooking(booking);
		assertTrue(rooms != null);
		assertTrue(rooms.size() == 1);

		// Add extra
		int priceForExtra = 10;
		String nameForExtra = "Chips";
		assertTrue(receptionist.addExtra(booking, rooms.get(0), nameForExtra, priceForExtra));
		
		// Start Checkout
		double price = receptionist.initiateCheckout(booking);
		assertTrue(price == ((roomPrice * stayLength) + priceForExtra));
		
		// Pay for Room and extra
		bankingComponent.removeCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm");
		assertTrue(bankingComponent.addCreditCard("4221652621878912", "143", 03, 18,"Filip", "Seholm"));
		assertTrue(bankingComponent.makeDeposit("4221652621878912","143", 03, 18, "Filip", "Seholm", 10000) ==10000);
		assertTrue(receptionist.payDuringCheckout("4221652621878912","143", 03, 18, "Filip", "Seholm"));
		
	}
	
	@Test
	public void addRoomToBooking(){
		admin.startup(0);
		assertTrue(admin.addRoomType("Room", 100, 2, "Test"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(type != null);
		
		//Create 1 booking and 1 room and add room to booking�, adding more fails.
		int id = receptionist.initiateBooking("Kalle", "161215", "161220", "Nisse");
		assertTrue(receptionist.getBookingcontroller().getBooking(id) != null);
		assertTrue(admin.addRoom(type,47));
		assertTrue(receptionist.addRoomToBooking(type.getName(), id));
		assertFalse(receptionist.addRoomToBooking(type.getName(),id));
		
		//Create another booking on another date with same room should work, but only for 1 room.
		int id2 = receptionist.initiateBooking("Kalle", "161222", "161224", "Nisse");
		assertTrue(receptionist.getBookingcontroller().getBooking(id2) != null);
		assertTrue(receptionist.addRoomToBooking(type.getName(), id2));
		assertFalse(receptionist.addRoomToBooking(type.getName(),id2));
		
		//Add more rooms and add them to a booking.
		assertTrue(admin.addRoom(type,49));
		assertTrue(admin.addRoom(type,48));
		assertTrue(receptionist.addRoomToBooking(type.getName(), id2));
		assertTrue(receptionist.addRoomToBooking(type.getName(), id2));
		assertFalse(receptionist.addRoomToBooking(type.getName(), id2));
		
		//Add a booking spanning another booking
		int id3 = receptionist.initiateBooking("Kalle", "161221", "161223", "Nisse");
		assertFalse(receptionist.addRoomToBooking(type.getName(), id3));
		
		//Add booking spanning multiple bookings where one booking has room but not the other
		int id4 = receptionist.initiateBooking("Kalle", "161201", "161230", "Nisse");
		assertFalse(receptionist.addRoomToBooking(type.getName(), id4));
		
		//Check over month and year break
		int id5 = receptionist.initiateBooking("Kalle", "161201", "170101", "Nisse");
		assertFalse(receptionist.addRoomToBooking(type.getName(), id5));
		
		//add booking starting same day other booking (all rooms taken that day) end
		int id6 = receptionist.initiateBooking("Kalle", "161224", "161226", "Nisse");
		assertTrue(receptionist.addRoomToBooking(type.getName(), id6));
		
		//add booking ending on full day
		int id7 = receptionist.initiateBooking("Kalle", "161221", "161222", "Nisse");
		assertTrue(receptionist.addRoomToBooking(type.getName(), id7));
		
		//Add a blocked room
		assertTrue(admin.addRoom(type, 50));
		assertTrue(admin.blockRoom(50));
		assertFalse(receptionist.addRoomToBooking(type.getName(), id2));
		
		//Add to invalid bookingID
		assertFalse(receptionist.addRoomToBooking(type.getName(), -1));
	}
	
	@Test 
	public void checkInToCheckout(){

		double roomPrice = 100;
		int stayLength = 30;
		assertTrue(admin.addRoomType("Room", roomPrice, 2, "Features"));
		RoomType type = admin.getIadminroom().getRoomTypes().get(0);
		assertTrue(admin.addRoom(type, 1));
		Calendar cal = Calendar.getInstance();
		String to = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, stayLength*-1);
		Date checkFrom = cal.getTime();
		String from = String.valueOf((cal.get(Calendar.YEAR)-2000))+String.format("%02d",cal.get(Calendar.MONTH)+1)+String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));
		int booking = receptionist.initiateBooking("Derp", from, to, "Derpalot");
		assertTrue(booking != -1);
		assertTrue(receptionist.addRoomToBooking(type.getName(), booking));
		assertTrue(receptionist.confirmBooking(booking));
		EList<Integer> rooms = receptionist.checkInBooking(booking);
		assertTrue(rooms != null);
		assertTrue(rooms.size() == 1);
		double price = receptionist.initiateCheckout(booking);
		assertTrue(price == roomPrice * stayLength);
		cal.add(Calendar.DAY_OF_MONTH, stayLength+3);
		Date checkTo = cal.getTime();
		assertTrue(receptionist.getCheckedOutBookings(checkFrom, checkTo).size() == 1);
	}
	
	private Date createDate(String dateString){
		DateFormat format = new SimpleDateFormat("yyMMdd");

        Date date = new Date();

        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            date = now.getTime();
        }
        return date;
	}

	
}
