/**
 */
package se.chalmers.cse.mdsd1617.group11.BankingModel.impl;

import java.lang.reflect.InvocationTargetException;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires;
import se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires;
import se.chalmers.cse.mdsd1617.group11.BankingModel.BankComponent;
import se.chalmers.cse.mdsd1617.group11.BankingModel.BankingModelPackage;
import se.chalmers.cse.mdsd1617.group11.BankingModel.IAdministratorProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bank Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BankComponentImpl extends MinimalEObjectImpl.Container implements BankComponent {
	CustomerRequires customerInterface;
	AdministratorRequires adminInterface;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BankComponentImpl() {
		super();
		try {
			customerInterface = CustomerRequires.instance();
			adminInterface = AdministratorRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankingModelPackage.Literals.BANK_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean makePayment(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName, double sum) {
		try {
			return customerInterface.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum);
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isCreditCardValid(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			return customerInterface.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
			
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double makeDeposit(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName, double sum) {
		try {
			return adminInterface.makeDeposit(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum);
		} catch (SOAPException e) {
			return -1;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addCreditCard(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			return adminInterface.addCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeCreditCard(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			return adminInterface.removeCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getBalance(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			return adminInterface.getBalance(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			return -1;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IAdministratorProvides.class) {
			switch (baseOperationID) {
				case BankingModelPackage.IADMINISTRATOR_PROVIDES___MAKE_DEPOSIT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE: return BankingModelPackage.BANK_COMPONENT___MAKE_DEPOSIT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE;
				case BankingModelPackage.IADMINISTRATOR_PROVIDES___ADD_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING: return BankingModelPackage.BANK_COMPONENT___ADD_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING;
				case BankingModelPackage.IADMINISTRATOR_PROVIDES___REMOVE_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING: return BankingModelPackage.BANK_COMPONENT___REMOVE_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING;
				case BankingModelPackage.IADMINISTRATOR_PROVIDES___GET_BALANCE__STRING_STRING_INT_INT_STRING_STRING: return BankingModelPackage.BANK_COMPONENT___GET_BALANCE__STRING_STRING_INT_INT_STRING_STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankingModelPackage.BANK_COMPONENT___MAKE_PAYMENT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE:
				return makePayment((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (Double)arguments.get(6));
			case BankingModelPackage.BANK_COMPONENT___IS_CREDIT_CARD_VALID__STRING_STRING_INT_INT_STRING_STRING:
				return isCreditCardValid((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BankingModelPackage.BANK_COMPONENT___MAKE_DEPOSIT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE:
				return makeDeposit((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (Double)arguments.get(6));
			case BankingModelPackage.BANK_COMPONENT___ADD_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING:
				return addCreditCard((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BankingModelPackage.BANK_COMPONENT___REMOVE_CREDIT_CARD__STRING_STRING_INT_INT_STRING_STRING:
				return removeCreditCard((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BankingModelPackage.BANK_COMPONENT___GET_BALANCE__STRING_STRING_INT_INT_STRING_STRING:
				return getBalance((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BankComponentImpl
