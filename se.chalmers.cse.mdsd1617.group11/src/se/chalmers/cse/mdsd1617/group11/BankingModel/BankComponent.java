/**
 */
package se.chalmers.cse.mdsd1617.group11.BankingModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bank Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group11.BankingModel.BankingModelPackage#getBankComponent()
 * @model
 * @generated
 */
public interface BankComponent extends ICustomerProvides, IAdministratorProvides {
} // BankComponent
