/**
 */
package se.chalmers.cse.mdsd1617.group11.Room.impl;

import java.util.Map;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage;

import se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl;

import se.chalmers.cse.mdsd1617.group11.BankingModel.BankingModelPackage;

import se.chalmers.cse.mdsd1617.group11.BankingModel.impl.BankingModelPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Group11Package;

import se.chalmers.cse.mdsd1617.group11.Query.QueryPackage;

import se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage;

import se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom;
import se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom;
import se.chalmers.cse.mdsd1617.group11.Room.Room;
import se.chalmers.cse.mdsd1617.group11.Room.RoomController;
import se.chalmers.cse.mdsd1617.group11.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group11.Room.RoomPackage;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;
import se.chalmers.cse.mdsd1617.group11.impl.Group11PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomPackageImpl extends EPackageImpl implements RoomPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomControllerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iAdminRoomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iQueryRoomEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomPackageImpl() {
		super(eNS_URI, RoomFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomPackage init() {
		if (isInited) return (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Obtain or create and register package
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group11PackageImpl theGroup11Package = (Group11PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group11Package.eNS_URI) instanceof Group11PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group11Package.eNS_URI) : Group11Package.eINSTANCE);
		ReservationPackageImpl theReservationPackage = (ReservationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReservationPackage.eNS_URI) instanceof ReservationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReservationPackage.eNS_URI) : ReservationPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		BankingModelPackageImpl theBankingModelPackage = (BankingModelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BankingModelPackage.eNS_URI) instanceof BankingModelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BankingModelPackage.eNS_URI) : BankingModelPackage.eINSTANCE);
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) : AdminPackage.eINSTANCE);
		QueryPackageImpl theQueryPackage = (QueryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(QueryPackage.eNS_URI) instanceof QueryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(QueryPackage.eNS_URI) : QueryPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomPackage.createPackageContents();
		theGroup11Package.createPackageContents();
		theReservationPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theBankingModelPackage.createPackageContents();
		theAdminPackage.createPackageContents();
		theQueryPackage.createPackageContents();

		// Initialize created meta-data
		theRoomPackage.initializePackageContents();
		theGroup11Package.initializePackageContents();
		theReservationPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theBankingModelPackage.initializePackageContents();
		theAdminPackage.initializePackageContents();
		theQueryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomPackage.eNS_URI, theRoomPackage);
		return theRoomPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomController() {
		return roomControllerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomController_RoomMap() {
		return (EReference)roomControllerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getroomMapEntry() {
		return roomMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getroomMapEntry_Key() {
		return (EReference)roomMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getroomMapEntry_Value() {
		return (EReference)roomMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Name() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Price() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_NbrOfBeds() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Features() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_RoomNbr() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Status() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIAdminRoom() {
		return iAdminRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__AddRoom__RoomType_int() {
		return iAdminRoomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__AddRoomType__String_double_int_String() {
		return iAdminRoomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__UpdateRoomType__String_String_double_int_String() {
		return iAdminRoomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__RemoveRoomType__String() {
		return iAdminRoomEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__BlockRoom__int() {
		return iAdminRoomEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__ChangeRoomTypeOfRoom__int_RoomType() {
		return iAdminRoomEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__RemoveRoom__int() {
		return iAdminRoomEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__GetRoomTypes() {
		return iAdminRoomEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__GetRooms() {
		return iAdminRoomEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__UnblockRoom__int() {
		return iAdminRoomEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__GetRoomTypeOfRoom__int() {
		return iAdminRoomEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdminRoom__Restart() {
		return iAdminRoomEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIQueryRoom() {
		return iQueryRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetRoom__int() {
		return iQueryRoomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetAvailableRooms__String() {
		return iQueryRoomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__OccupyRoom__Room() {
		return iQueryRoomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetRooms__int() {
		return iQueryRoomEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetPrice__String() {
		return iQueryRoomEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetAvailableRoom__String() {
		return iQueryRoomEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__FreeRoom__int() {
		return iQueryRoomEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetAvailableRoomNo__String() {
		return iQueryRoomEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIQueryRoom__GetRoomType__int() {
		return iQueryRoomEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomFactory getRoomFactory() {
		return (RoomFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomControllerEClass = createEClass(ROOM_CONTROLLER);
		createEReference(roomControllerEClass, ROOM_CONTROLLER__ROOM_MAP);

		roomMapEntryEClass = createEClass(ROOM_MAP_ENTRY);
		createEReference(roomMapEntryEClass, ROOM_MAP_ENTRY__KEY);
		createEReference(roomMapEntryEClass, ROOM_MAP_ENTRY__VALUE);

		roomTypeEClass = createEClass(ROOM_TYPE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NAME);
		createEAttribute(roomTypeEClass, ROOM_TYPE__PRICE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NBR_OF_BEDS);
		createEAttribute(roomTypeEClass, ROOM_TYPE__FEATURES);

		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__ROOM_NBR);
		createEAttribute(roomEClass, ROOM__STATUS);

		iAdminRoomEClass = createEClass(IADMIN_ROOM);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___ADD_ROOM__ROOMTYPE_INT);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___REMOVE_ROOM_TYPE__STRING);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___BLOCK_ROOM__INT);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___REMOVE_ROOM__INT);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___GET_ROOM_TYPES);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___GET_ROOMS);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___UNBLOCK_ROOM__INT);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___GET_ROOM_TYPE_OF_ROOM__INT);
		createEOperation(iAdminRoomEClass, IADMIN_ROOM___RESTART);

		iQueryRoomEClass = createEClass(IQUERY_ROOM);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_ROOM__INT);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_AVAILABLE_ROOMS__STRING);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___OCCUPY_ROOM__ROOM);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_ROOMS__INT);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_PRICE__STRING);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_AVAILABLE_ROOM__STRING);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___FREE_ROOM__INT);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_AVAILABLE_ROOM_NO__STRING);
		createEOperation(iQueryRoomEClass, IQUERY_ROOM___GET_ROOM_TYPE__INT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Group11Package theGroup11Package = (Group11Package)EPackage.Registry.INSTANCE.getEPackage(Group11Package.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomControllerEClass.getESuperTypes().add(this.getIAdminRoom());
		roomControllerEClass.getESuperTypes().add(this.getIQueryRoom());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomControllerEClass, RoomController.class, "RoomController", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomController_RoomMap(), this.getroomMapEntry(), null, "roomMap", null, 0, -1, RoomController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomMapEntryEClass, Map.Entry.class, "roomMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getroomMapEntry_Key(), this.getRoomType(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getroomMapEntry_Value(), this.getRoom(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomType_Name(), ecorePackage.getEString(), "name", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_NbrOfBeds(), ecorePackage.getEInt(), "nbrOfBeds", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Features(), ecorePackage.getEString(), "features", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_RoomNbr(), ecorePackage.getEInt(), "roomNbr", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_Status(), ecorePackage.getEString(), "status", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iAdminRoomEClass, IAdminRoom.class, "IAdminRoom", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIAdminRoom__AddRoom__RoomType_int(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNo", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__AddRoomType__String_double_int_String(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__UpdateRoomType__String_String_double_int_String(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "oldRoomTypeName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "newPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newNbrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newFeatures", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__BlockRoom__int(), ecorePackage.getEBoolean(), "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__ChangeRoomTypeOfRoom__int_RoomType(), ecorePackage.getEBoolean(), "changeRoomTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomType(), "newRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIAdminRoom__GetRoomTypes(), this.getRoomType(), "getRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIAdminRoom__GetRooms(), this.getRoom(), "getRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__UnblockRoom__int(), ecorePackage.getEBoolean(), "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdminRoom__GetRoomTypeOfRoom__int(), this.getRoomType(), "getRoomTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNo", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIAdminRoom__Restart(), null, "restart", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iQueryRoomEClass, IQueryRoom.class, "IQueryRoom", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIQueryRoom__GetRoom__int(), this.getRoom(), "getRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetAvailableRooms__String(), theGroup11Package.getFreeRoomTypesDTO(), "getAvailableRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__OccupyRoom__Room(), ecorePackage.getEInt(), "occupyRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoom(), "room", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetRooms__int(), theGroup11Package.getFreeRoomTypesDTO(), "getRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetPrice__String(), ecorePackage.getEDouble(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetAvailableRoom__String(), ecorePackage.getEInt(), "getAvailableRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__FreeRoom__int(), ecorePackage.getEBoolean(), "freeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNo", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetAvailableRoomNo__String(), ecorePackage.getEInt(), "getAvailableRoomNo", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDesc", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIQueryRoom__GetRoomType__int(), ecorePackage.getEString(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNo", 1, 1, IS_UNIQUE, !IS_ORDERED);
	}

} //RoomPackageImpl
