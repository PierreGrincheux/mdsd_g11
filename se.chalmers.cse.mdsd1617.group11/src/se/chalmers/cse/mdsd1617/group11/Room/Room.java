/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getRoomNbr <em>Room Nbr</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getStatus <em>Status</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Nbr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Nbr</em>' attribute.
	 * @see #setRoomNbr(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoom_RoomNbr()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomNbr();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getRoomNbr <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Nbr</em>' attribute.
	 * @see #getRoomNbr()
	 * @generated
	 */
	void setRoomNbr(int value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see #setStatus(String)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoom_Status()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(String value);

} // Room
