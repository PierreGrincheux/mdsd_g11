/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group11/Room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group11.Room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom <em>IAdmin Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getIAdminRoom()
	 * @generated
	 */
	int IADMIN_ROOM = 4;

	/**
	 * The number of structural features of the '<em>IAdmin Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___ADD_ROOM__ROOMTYPE_INT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___REMOVE_ROOM_TYPE__STRING = 3;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___BLOCK_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE = 5;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___REMOVE_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___GET_ROOM_TYPES = 7;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___GET_ROOMS = 8;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___UNBLOCK_ROOM__INT = 9;

	/**
	 * The operation id for the '<em>Get Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___GET_ROOM_TYPE_OF_ROOM__INT = 10;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM___RESTART = 11;

	/**
	 * The number of operations of the '<em>IAdmin Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_ROOM_OPERATION_COUNT = 12;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomControllerImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoomController()
	 * @generated
	 */
	int ROOM_CONTROLLER = 0;

	/**
	 * The feature id for the '<em><b>Room Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER__ROOM_MAP = IADMIN_ROOM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER_FEATURE_COUNT = IADMIN_ROOM_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___ADD_ROOM__ROOMTYPE_INT = IADMIN_ROOM___ADD_ROOM__ROOMTYPE_INT;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IADMIN_ROOM___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = IADMIN_ROOM___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___REMOVE_ROOM_TYPE__STRING = IADMIN_ROOM___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___BLOCK_ROOM__INT = IADMIN_ROOM___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE = IADMIN_ROOM___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___REMOVE_ROOM__INT = IADMIN_ROOM___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOM_TYPES = IADMIN_ROOM___GET_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOMS = IADMIN_ROOM___GET_ROOMS;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___UNBLOCK_ROOM__INT = IADMIN_ROOM___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Get Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOM_TYPE_OF_ROOM__INT = IADMIN_ROOM___GET_ROOM_TYPE_OF_ROOM__INT;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___RESTART = IADMIN_ROOM___RESTART;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOM__INT = IADMIN_ROOM_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Available Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_AVAILABLE_ROOMS__STRING = IADMIN_ROOM_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___OCCUPY_ROOM__ROOM = IADMIN_ROOM_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOMS__INT = IADMIN_ROOM_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_PRICE__STRING = IADMIN_ROOM_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Available Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_AVAILABLE_ROOM__STRING = IADMIN_ROOM_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___FREE_ROOM__INT = IADMIN_ROOM_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Available Room No</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_AVAILABLE_ROOM_NO__STRING = IADMIN_ROOM_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER___GET_ROOM_TYPE__INT = IADMIN_ROOM_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CONTROLLER_OPERATION_COUNT = IADMIN_ROOM_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.roomMapEntryImpl <em>room Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.roomMapEntryImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getroomMapEntry()
	 * @generated
	 */
	int ROOM_MAP_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = 1;

	/**
	 * The feature id for the '<em><b>Nbr Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NBR_OF_BEDS = 2;

	/**
	 * The feature id for the '<em><b>Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__FEATURES = 3;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 3;

	/**
	 * The feature id for the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_NBR = 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__STATUS = 1;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom <em>IQuery Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom
	 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getIQueryRoom()
	 * @generated
	 */
	int IQUERY_ROOM = 5;

	/**
	 * The number of structural features of the '<em>IQuery Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_ROOM__INT = 0;

	/**
	 * The operation id for the '<em>Get Available Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_AVAILABLE_ROOMS__STRING = 1;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___OCCUPY_ROOM__ROOM = 2;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_ROOMS__INT = 3;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_PRICE__STRING = 4;

	/**
	 * The operation id for the '<em>Get Available Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_AVAILABLE_ROOM__STRING = 5;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___FREE_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Get Available Room No</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_AVAILABLE_ROOM_NO__STRING = 7;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM___GET_ROOM_TYPE__INT = 8;

	/**
	 * The number of operations of the '<em>IQuery Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IQUERY_ROOM_OPERATION_COUNT = 9;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomController
	 * @generated
	 */
	EClass getRoomController();

	/**
	 * Returns the meta object for the map '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomController#getRoomMap <em>Room Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Room Map</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomController#getRoomMap()
	 * @see #getRoomController()
	 * @generated
	 */
	EReference getRoomController_RoomMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>room Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>room Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="se.chalmers.cse.mdsd1617.group11.Room.RoomType" keyRequired="true" keyOrdered="false"
	 *        valueType="se.chalmers.cse.mdsd1617.group11.Room.Room" valueMany="true" valueOrdered="false"
	 * @generated
	 */
	EClass getroomMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getroomMapEntry()
	 * @generated
	 */
	EReference getroomMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getroomMapEntry()
	 * @generated
	 */
	EReference getroomMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getNbrOfBeds <em>Nbr Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nbr Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomType#getNbrOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NbrOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Features</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomType#getFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Features();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getRoomNbr <em>Room Nbr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Nbr</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.Room#getRoomNbr()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomNbr();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Room.Room#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.Room#getStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Status();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom <em>IAdmin Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAdmin Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom
	 * @generated
	 */
	EClass getIAdminRoom();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#addRoom(se.chalmers.cse.mdsd1617.group11.Room.RoomType, int) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#addRoom(se.chalmers.cse.mdsd1617.group11.Room.RoomType, int)
	 * @generated
	 */
	EOperation getIAdminRoom__AddRoom__RoomType_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdminRoom__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdminRoom__UpdateRoomType__String_String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIAdminRoom__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#blockRoom(int)
	 * @generated
	 */
	EOperation getIAdminRoom__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#changeRoomTypeOfRoom(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#changeRoomTypeOfRoom(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType)
	 * @generated
	 */
	EOperation getIAdminRoom__ChangeRoomTypeOfRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#removeRoom(int)
	 * @generated
	 */
	EOperation getIAdminRoom__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRoomTypes() <em>Get Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRoomTypes()
	 * @generated
	 */
	EOperation getIAdminRoom__GetRoomTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRooms() <em>Get Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRooms()
	 * @generated
	 */
	EOperation getIAdminRoom__GetRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#unblockRoom(int)
	 * @generated
	 */
	EOperation getIAdminRoom__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRoomTypeOfRoom(int) <em>Get Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#getRoomTypeOfRoom(int)
	 * @generated
	 */
	EOperation getIAdminRoom__GetRoomTypeOfRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#restart() <em>Restart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Restart</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom#restart()
	 * @generated
	 */
	EOperation getIAdminRoom__Restart();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom <em>IQuery Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IQuery Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom
	 * @generated
	 */
	EClass getIQueryRoom();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRoom(int) <em>Get Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRoom(int)
	 * @generated
	 */
	EOperation getIQueryRoom__GetRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRooms(java.lang.String) <em>Get Available Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIQueryRoom__GetAvailableRooms__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#occupyRoom(se.chalmers.cse.mdsd1617.group11.Room.Room) <em>Occupy Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#occupyRoom(se.chalmers.cse.mdsd1617.group11.Room.Room)
	 * @generated
	 */
	EOperation getIQueryRoom__OccupyRoom__Room();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRooms(int) <em>Get Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRooms(int)
	 * @generated
	 */
	EOperation getIQueryRoom__GetRooms__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getPrice(java.lang.String) <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getPrice(java.lang.String)
	 * @generated
	 */
	EOperation getIQueryRoom__GetPrice__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRoom(java.lang.String) <em>Get Available Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIQueryRoom__GetAvailableRoom__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#freeRoom(int) <em>Free Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Free Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#freeRoom(int)
	 * @generated
	 */
	EOperation getIQueryRoom__FreeRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRoomNo(java.lang.String) <em>Get Available Room No</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Room No</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getAvailableRoomNo(java.lang.String)
	 * @generated
	 */
	EOperation getIQueryRoom__GetAvailableRoomNo__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRoomType(int) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom#getRoomType(int)
	 * @generated
	 */
	EOperation getIQueryRoom__GetRoomType__int();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomControllerImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoomController()
		 * @generated
		 */
		EClass ROOM_CONTROLLER = eINSTANCE.getRoomController();

		/**
		 * The meta object literal for the '<em><b>Room Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_CONTROLLER__ROOM_MAP = eINSTANCE.getRoomController_RoomMap();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.roomMapEntryImpl <em>room Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.roomMapEntryImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getroomMapEntry()
		 * @generated
		 */
		EClass ROOM_MAP_ENTRY = eINSTANCE.getroomMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MAP_ENTRY__KEY = eINSTANCE.getroomMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MAP_ENTRY__VALUE = eINSTANCE.getroomMapEntry_Value();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Nbr Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NBR_OF_BEDS = eINSTANCE.getRoomType_NbrOfBeds();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__FEATURES = eINSTANCE.getRoomType_Features();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room Nbr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_NBR = eINSTANCE.getRoom_RoomNbr();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__STATUS = eINSTANCE.getRoom_Status();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom <em>IAdmin Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getIAdminRoom()
		 * @generated
		 */
		EClass IADMIN_ROOM = eINSTANCE.getIAdminRoom();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___ADD_ROOM__ROOMTYPE_INT = eINSTANCE.getIAdminRoom__AddRoom__RoomType_int();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIAdminRoom__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = eINSTANCE.getIAdminRoom__UpdateRoomType__String_String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIAdminRoom__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___BLOCK_ROOM__INT = eINSTANCE.getIAdminRoom__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE = eINSTANCE.getIAdminRoom__ChangeRoomTypeOfRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___REMOVE_ROOM__INT = eINSTANCE.getIAdminRoom__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___GET_ROOM_TYPES = eINSTANCE.getIAdminRoom__GetRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Get Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___GET_ROOMS = eINSTANCE.getIAdminRoom__GetRooms();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___UNBLOCK_ROOM__INT = eINSTANCE.getIAdminRoom__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___GET_ROOM_TYPE_OF_ROOM__INT = eINSTANCE.getIAdminRoom__GetRoomTypeOfRoom__int();

		/**
		 * The meta object literal for the '<em><b>Restart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN_ROOM___RESTART = eINSTANCE.getIAdminRoom__Restart();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom <em>IQuery Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom
		 * @see se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl#getIQueryRoom()
		 * @generated
		 */
		EClass IQUERY_ROOM = eINSTANCE.getIQueryRoom();

		/**
		 * The meta object literal for the '<em><b>Get Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_ROOM__INT = eINSTANCE.getIQueryRoom__GetRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Available Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_AVAILABLE_ROOMS__STRING = eINSTANCE.getIQueryRoom__GetAvailableRooms__String();

		/**
		 * The meta object literal for the '<em><b>Occupy Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___OCCUPY_ROOM__ROOM = eINSTANCE.getIQueryRoom__OccupyRoom__Room();

		/**
		 * The meta object literal for the '<em><b>Get Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_ROOMS__INT = eINSTANCE.getIQueryRoom__GetRooms__int();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_PRICE__STRING = eINSTANCE.getIQueryRoom__GetPrice__String();

		/**
		 * The meta object literal for the '<em><b>Get Available Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_AVAILABLE_ROOM__STRING = eINSTANCE.getIQueryRoom__GetAvailableRoom__String();

		/**
		 * The meta object literal for the '<em><b>Free Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___FREE_ROOM__INT = eINSTANCE.getIQueryRoom__FreeRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Available Room No</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_AVAILABLE_ROOM_NO__STRING = eINSTANCE.getIQueryRoom__GetAvailableRoomNo__String();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IQUERY_ROOM___GET_ROOM_TYPE__INT = eINSTANCE.getIQueryRoom__GetRoomType__int();

	}

} //RoomPackage
