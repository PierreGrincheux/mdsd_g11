/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IAdmin Room</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getIAdminRoom()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IAdminRoom extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" roomNoRequired="true" roomNoOrdered="false"
	 * @generated
	 */
	boolean addRoom(RoomType roomType, int roomNo);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" featuresRequired="true" featuresOrdered="false"
	 * @generated
	 */
	boolean addRoomType(String name, double price, int nbrOfBeds, String features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldRoomTypeNameRequired="true" oldRoomTypeNameOrdered="false" newNameRequired="true" newNameOrdered="false" newPriceRequired="true" newPriceOrdered="false" newNbrOfBedsRequired="true" newNbrOfBedsOrdered="false" newFeaturesRequired="true" newFeaturesOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String oldRoomTypeName, String newName, double newPrice, int newNbrOfBeds, String newFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean blockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeOfRoom(int roomNbr, RoomType newRoomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean unblockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNoRequired="true" roomNoOrdered="false"
	 * @generated
	 */
	RoomType getRoomTypeOfRoom(int roomNo);
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void restart();

} // IAdminRoom
