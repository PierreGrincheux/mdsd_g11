/**
 */
package se.chalmers.cse.mdsd1617.group11.Room.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import se.chalmers.cse.mdsd1617.group11.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group11.Group11Factory;
import se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom;
import se.chalmers.cse.mdsd1617.group11.Room.Room;
import se.chalmers.cse.mdsd1617.group11.Room.RoomController;
import se.chalmers.cse.mdsd1617.group11.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group11.Room.RoomPackage;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.impl.RoomControllerImpl#getRoomMap <em>Room Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomControllerImpl extends MinimalEObjectImpl.Container implements RoomController {
	/**
	 * The cached value of the '{@link #getRoomMap() <em>Room Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<RoomType, EList<Room>> roomMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomControllerImpl() {
		getRoomMap();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<RoomType, EList<Room>> getRoomMap() {
		if (roomMap == null) {
			roomMap = new EcoreEMap<RoomType,EList<Room>>(RoomPackage.Literals.ROOM_MAP_ENTRY, roomMapEntryImpl.class, this, RoomPackage.ROOM_CONTROLLER__ROOM_MAP);
		}
		return roomMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room with a given room type. 
	 * @return false if a room with that number exists, else returns true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(RoomType roomType, int roomNo) {
		//Return false if room with that room number exists.
		if (getRoom(roomNo) != null || !roomMap.containsKey(roomType)){
			return false;
		}
		Room roomToAdd = RoomFactory.eINSTANCE.createRoom();
		roomToAdd.setRoomNbr(roomNo);
		roomToAdd.setStatus(RoomImpl.STATUS_AVAILABLE);
		roomMap.get(roomType).add(roomToAdd);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, double price, int nbrOfBeds, String features) {
		//Return false if name is null or a type with the name exists.
		if (name == null){
			return false;
		}
		for (RoomType type: roomMap.keySet()){
			if (type.getName().equals(name)) return false;
		}
		RoomType typeToAdd = RoomFactory.eINSTANCE.createRoomType();
		typeToAdd.setName(name);
		typeToAdd.setPrice(price);
		typeToAdd.setNbrOfBeds(nbrOfBeds);
		typeToAdd.setFeatures(features);
		roomMapEntryImpl entry = new roomMapEntryImpl();
		entry.setKey(typeToAdd);
		roomMap.add(entry);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Updates the values of a given room type. Returns false if the room type does not exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldRoomTypeName, String newName, double newPrice, int newNbrOfBeds, String newFeatures) {
		// First check so that roomtype name is unique
		if (getRoomType(newName) == null) {
			RoomType toChange = getRoomType(oldRoomTypeName);
			if (toChange != null){
				toChange.setName(newName);
				toChange.setPrice(newPrice);
				toChange.setNbrOfBeds(newNbrOfBeds);
				toChange.setFeatures(newFeatures);
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes the room type if no room exist for the room type. Else return false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String name) {
		RoomType type = getRoomType(name);
		if (type != null){
			EList<Room> rooms = roomMap.get(type);
			if (rooms.size()==0){
				roomMap.removeKey(type);
				return true;
			}
			
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Sets the room status to blocked if the room was available, else the status is unchanged and returns false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNbr) {
		Room roomToBlock = getRoom(roomNbr);
		if (roomToBlock.getStatus().equals(RoomImpl.STATUS_AVAILABLE)){
			roomToBlock.setStatus(RoomImpl.STATUS_BLOCKED);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Changes the room type of a given room. Return false if the room does not exist or the new room type does not exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeOfRoom(int roomNbr, RoomType newRoomType) {
		Room tempRoom = getRoom(roomNbr);
		if (tempRoom != null && roomMap.containsKey(newRoomType)){
			if (removeRoom(roomNbr)){
				roomMap.get(newRoomType).add(tempRoom);
				return true;
			}
		}
		return false;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes the room with the given room number. Returns true on success or false if a room with that room number does not exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNbr) {
		Room tempRoom = getRoom(roomNbr);
		RoomType tempType = getRoomTypeOfRoom(roomNbr);
		if (tempRoom != null && tempType != null && !tempRoom.getStatus().equals(RoomImpl.STATUS_OCCUPIED)) {
			roomMap.get(tempType).remove(tempRoom);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all saved room types.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getRoomTypes() {
		EList<RoomType> types = new BasicEList<RoomType>();
		types.addAll(roomMap.keySet());
		return types;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all the rooms for all room types. Returns an empty list if there are no rooms.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getRooms() {
		EList<Room> rooms = new BasicEList<Room>();
		for (RoomType type:roomMap.keySet()){
			rooms.addAll(roomMap.get(type));
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Sets the room status to available if it was blocked, and returns true. Else returns false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNbr) {
		Room roomToUnblock = getRoom(roomNbr);
		if (roomToUnblock.getStatus().equals(RoomImpl.STATUS_BLOCKED)){
			roomToUnblock.setStatus(RoomImpl.STATUS_AVAILABLE);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the room object with the given room number. If no room with the give number exists method will return null.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Room getRoom(int roomNbr) {
		for (RoomType type: roomMap.keySet()){
			for (Room room: roomMap.get(type)){
				if (room.getRoomNbr() == roomNbr){
					return room;
				}
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FreeRoomTypesDTO getAvailableRooms(String roomType) {
		RoomType type = getRoomType(roomType);
		if (type != null) {
			int temp = 0;
			for (Room room : roomMap.get(type)){
				if (room.getStatus().equals(RoomImpl.STATUS_AVAILABLE)){
					temp++;
				}
			}
			return createFreeRoomType(type, temp);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * CHecks if the given room is available, if so occupies the room and returns the room number. Else return -1.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int occupyRoom(Room room) {
		if (room.getStatus().equals(RoomImpl.STATUS_AVAILABLE)){
			room.setStatus(RoomImpl.STATUS_OCCUPIED);
			return room.getRoomNbr();
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of FreeRoomTypesDTO objects. The list will be empty if there are no room types with the given number or more rooms, and the objects may have 0 available rooms if none are available.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getRooms(int nbrBeds) {
		EList<FreeRoomTypesDTO> toReturn = new BasicEList<FreeRoomTypesDTO>();
		for (RoomType type : roomMap.keySet()){
			if (type.getNbrOfBeds()>=nbrBeds){
				int tempFreeRooms = 0;
				for (Room room: roomMap.get(type)){
					if (room.getStatus().equals(RoomImpl.STATUS_AVAILABLE)) tempFreeRooms++;
				}
				toReturn.add(createFreeRoomType(type, tempFreeRooms));
			}
				
		}
		return toReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the price of a given roomtype. Returns -1 if the room type does not exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getPrice(String roomType) {
		double price = -1;
		RoomType temp = getRoomType(roomType);
		if (temp != null){
			price = temp.getPrice();
		}
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getAvailableRoom(String roomType) {
		for (RoomType type : roomMap.keySet()){
			if (type.getName().equals(roomType)){
				for (Room room : roomMap.get(type)){
					if (room.getStatus().equals(RoomImpl.STATUS_AVAILABLE)) return room.getRoomNbr();
				}
			}
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean freeRoom(int roomNo) {
		Room room = getRoom(roomNo);
		if (room != null && room.getStatus().equals(RoomImpl.STATUS_OCCUPIED)){
			room.setStatus(RoomImpl.STATUS_AVAILABLE);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getAvailableRoomNo(String roomTypeDesc) {
		EList<Integer> roomNos = new BasicEList<>();
		for (RoomType type : roomMap.keySet()){
			if (type.getName().equals(roomTypeDesc)){
				for (Room room : roomMap.get(type)){
					if (room.getStatus().equals(RoomImpl.STATUS_AVAILABLE)) roomNos.add(room.getRoomNbr()) ;
				}
			}
		}
		return roomNos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getRoomType(int roomNo) {
		return getRoomTypeOfRoom(roomNo).getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RoomPackage.ROOM_CONTROLLER__ROOM_MAP:
				return ((InternalEList<?>)getRoomMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_CONTROLLER__ROOM_MAP:
				if (coreType) return getRoomMap();
				else return getRoomMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_CONTROLLER__ROOM_MAP:
				((EStructuralFeature.Setting)getRoomMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_CONTROLLER__ROOM_MAP:
				getRoomMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_CONTROLLER__ROOM_MAP:
				return roomMap != null && !roomMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IQueryRoom.class) {
			switch (baseOperationID) {
				case RoomPackage.IQUERY_ROOM___GET_ROOM__INT: return RoomPackage.ROOM_CONTROLLER___GET_ROOM__INT;
				case RoomPackage.IQUERY_ROOM___GET_AVAILABLE_ROOMS__STRING: return RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOMS__STRING;
				case RoomPackage.IQUERY_ROOM___OCCUPY_ROOM__ROOM: return RoomPackage.ROOM_CONTROLLER___OCCUPY_ROOM__ROOM;
				case RoomPackage.IQUERY_ROOM___GET_ROOMS__INT: return RoomPackage.ROOM_CONTROLLER___GET_ROOMS__INT;
				case RoomPackage.IQUERY_ROOM___GET_PRICE__STRING: return RoomPackage.ROOM_CONTROLLER___GET_PRICE__STRING;
				case RoomPackage.IQUERY_ROOM___GET_AVAILABLE_ROOM__STRING: return RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOM__STRING;
				case RoomPackage.IQUERY_ROOM___FREE_ROOM__INT: return RoomPackage.ROOM_CONTROLLER___FREE_ROOM__INT;
				case RoomPackage.IQUERY_ROOM___GET_AVAILABLE_ROOM_NO__STRING: return RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOM_NO__STRING;
				case RoomPackage.IQUERY_ROOM___GET_ROOM_TYPE__INT: return RoomPackage.ROOM_CONTROLLER___GET_ROOM_TYPE__INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_CONTROLLER___ADD_ROOM__ROOMTYPE_INT:
				return addRoom((RoomType)arguments.get(0), (Integer)arguments.get(1));
			case RoomPackage.ROOM_CONTROLLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case RoomPackage.ROOM_CONTROLLER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4));
			case RoomPackage.ROOM_CONTROLLER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE:
				return changeRoomTypeOfRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case RoomPackage.ROOM_CONTROLLER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_ROOM_TYPES:
				return getRoomTypes();
			case RoomPackage.ROOM_CONTROLLER___GET_ROOMS:
				return getRooms();
			case RoomPackage.ROOM_CONTROLLER___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_ROOM_TYPE_OF_ROOM__INT:
				return getRoomTypeOfRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___RESTART:
				restart();
				return null;
			case RoomPackage.ROOM_CONTROLLER___GET_ROOM__INT:
				return getRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOMS__STRING:
				return getAvailableRooms((String)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___OCCUPY_ROOM__ROOM:
				return occupyRoom((Room)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_ROOMS__INT:
				return getRooms((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_PRICE__STRING:
				return getPrice((String)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOM__STRING:
				return getAvailableRoom((String)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___FREE_ROOM__INT:
				return freeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_AVAILABLE_ROOM_NO__STRING:
				return getAvailableRoomNo((String)arguments.get(0));
			case RoomPackage.ROOM_CONTROLLER___GET_ROOM_TYPE__INT:
				return getRoomType((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}
	
	/**
	 * Returns the RoomType with the given name.
	 * @param nameOfRoomType
	 * @return The room type object.
	 */
	private RoomType getRoomType(String nameOfRoomType){
		for (RoomType type: roomMap.keySet()){
			if (type.getName().equals(nameOfRoomType)) return type;
		}
		return null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Returns the room type that contain the given room.
	 * @param roomNo
	 * @return the RoomType object that the room belongs to, else null.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomType getRoomTypeOfRoom(int roomNo){
		for (RoomType type: roomMap.keySet()){
			for (Room room: roomMap.get(type)){
				if (room.getRoomNbr() == roomNo){
					return type;
				}
			}
		}
		return null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void restart() {
		this.roomMap = new EcoreEMap<RoomType,EList<Room>>(RoomPackage.Literals.ROOM_MAP_ENTRY, roomMapEntryImpl.class, this, RoomPackage.ROOM_CONTROLLER__ROOM_MAP);
	}

	private FreeRoomTypesDTO createFreeRoomType(RoomType type, int noOfRooms){
		FreeRoomTypesDTO toReturn = Group11Factory.eINSTANCE.createFreeRoomTypesDTO();
		toReturn.setNumBeds(type.getNbrOfBeds());
		toReturn.setNumFreeRooms(noOfRooms);
		toReturn.setPricePerNight(type.getPrice());
		toReturn.setRoomTypeDescription(type.getName());
		return toReturn;
	}

} //RoomControllerImpl
