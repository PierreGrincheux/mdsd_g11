/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>room Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.roomMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.roomMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getroomMapEntry()
 * @model
 * @generated
 */
public interface roomMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getroomMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getKey();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.roomMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(RoomType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group11.Room.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getroomMapEntry_Value()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getValue();

} // roomMapEntry
