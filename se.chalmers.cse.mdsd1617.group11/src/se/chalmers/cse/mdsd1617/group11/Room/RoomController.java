/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.RoomController#getRoomMap <em>Room Map</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomController()
 * @model
 * @generated
 */
public interface RoomController extends IAdminRoom, IQueryRoom {
	/**
	 * Returns the value of the '<em><b>Room Map</b></em>' map.
	 * The key is of type {@link se.chalmers.cse.mdsd1617.group11.Room.RoomType},
	 * and the value is of type list of {@link se.chalmers.cse.mdsd1617.group11.Room.Room},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Map</em>' map.
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomController_RoomMap()
	 * @model mapType="se.chalmers.cse.mdsd1617.group11.Room.roomMapEntry<se.chalmers.cse.mdsd1617.group11.Room.RoomType, se.chalmers.cse.mdsd1617.group11.Room.Room>" ordered="false"
	 * @generated
	 */
	EMap<RoomType, EList<Room>> getRoomMap();


} // RoomController
