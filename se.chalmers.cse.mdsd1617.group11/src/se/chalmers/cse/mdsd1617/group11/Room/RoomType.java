/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getName <em>Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getPrice <em>Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getNbrOfBeds <em>Nbr Of Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomType()
 * @model
 * @generated
 */
public interface RoomType extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomType_Name()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price</em>' attribute.
	 * @see #setPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomType_Price()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getPrice <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price</em>' attribute.
	 * @see #getPrice()
	 * @generated
	 */
	void setPrice(double value);

	/**
	 * Returns the value of the '<em><b>Nbr Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nbr Of Beds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nbr Of Beds</em>' attribute.
	 * @see #setNbrOfBeds(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomType_NbrOfBeds()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getNbrOfBeds();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getNbrOfBeds <em>Nbr Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nbr Of Beds</em>' attribute.
	 * @see #getNbrOfBeds()
	 * @generated
	 */
	void setNbrOfBeds(int value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' attribute.
	 * @see #setFeatures(String)
	 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getRoomType_Features()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getFeatures();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Room.RoomType#getFeatures <em>Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Features</em>' attribute.
	 * @see #getFeatures()
	 * @generated
	 */
	void setFeatures(String value);

} // RoomType
