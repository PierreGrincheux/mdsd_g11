/**
 */
package se.chalmers.cse.mdsd1617.group11.Room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group11.FreeRoomTypesDTO;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IQuery Room</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group11.Room.RoomPackage#getIQueryRoom()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IQueryRoom extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	Room getRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	FreeRoomTypesDTO getAvailableRooms(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomRequired="true" roomOrdered="false"
	 * @generated
	 */
	int occupyRoom(Room room);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" nbrBedsRequired="true" nbrBedsOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> getRooms(int nbrBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	double getPrice(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int getAvailableRoom(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNoRequired="true" roomNoOrdered="false"
	 * @generated
	 */
	boolean freeRoom(int roomNo);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomTypeDescRequired="true" roomTypeDescOrdered="false"
	 * @generated
	 */
	EList<Integer> getAvailableRoomNo(String roomTypeDesc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNoRequired="true" roomNoOrdered="false"
	 * @generated
	 */
	String getRoomType(int roomNo);

} // IQueryRoom
