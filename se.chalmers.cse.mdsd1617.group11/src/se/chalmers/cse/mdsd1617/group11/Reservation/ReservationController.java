/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getReservation <em>Reservation</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getAtomicId <em>Atomic Id</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservationController()
 * @model
 * @generated
 */
public interface ReservationController extends EObject {
	/**
	 * Returns the value of the '<em><b>Reservation</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservation</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservationController_Reservation()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Reservation> getReservation();

	/**
	 * Returns the value of the '<em><b>Atomic Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Id</em>' attribute.
	 * @see #setAtomicId(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservationController_AtomicId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getAtomicId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getAtomicId <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Id</em>' attribute.
	 * @see #getAtomicId()
	 * @generated
	 */
	void setAtomicId(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<Reservation> getOccupiedReservations(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" checkInDateRequired="true" checkInDateOrdered="false"
	 * @generated
	 */
	EList<Reservation> getCheckIns(Date checkInDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" checkInDateStartRequired="true" checkInDateStartOrdered="false" checkInDateEndRequired="true" checkInDateEndOrdered="false"
	 * @generated
	 */
	EList<Reservation> getCheckIns(Date checkInDateStart, Date checkInDateEnd);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromRequired="true" fromOrdered="false" toRequired="true" toOrdered="false"
	 * @generated
	 */
	EList<Reservation> getCheckedOut(Date from, Date to);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationRequired="true" reservationOrdered="false" roomNoRequired="true" roomNoOrdered="false" checkInDateRequired="true" checkInDateOrdered="false"
	 * @generated
	 */
	boolean checkInReservation(int reservation, int roomNo, Date checkInDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypesRequired="true" roomTypesOrdered="false"
	 * @generated
	 */
	int initiateReservation(String roomTypes);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false"
	 * @generated
	 */
	boolean changeRoomType(int reservationID, String newRoomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false" checkoutDateRequired="true" checkoutDateOrdered="false"
	 * @generated
	 */
	double checkOutReservation(int reservationID, Date checkoutDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false"
	 * @generated
	 */
	String getRoomType(int reservationID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false"
	 * @generated
	 */
	boolean cancelReservation(int reservationID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false"
	 * @generated
	 */
	Reservation getReservation(int reservationID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false" priceRequired="true" priceOrdered="false" extraDescriptionRequired="true" extraDescriptionOrdered="false"
	 * @generated
	 */
	boolean addExtra(int reservationID, double price, String extraDescription);
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void restart();

} // ReservationController
