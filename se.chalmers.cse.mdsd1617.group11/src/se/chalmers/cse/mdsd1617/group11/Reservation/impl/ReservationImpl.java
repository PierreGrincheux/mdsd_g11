/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group11.Reservation.Extra;
import se.chalmers.cse.mdsd1617.group11.Reservation.Reservation;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getExtra <em>Extra</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getCheckInDate <em>Check In Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getCheckOutDate <em>Check Out Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getRoomNbr <em>Room Nbr</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl#getReservedRoomType <em>Reserved Room Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReservationImpl extends MinimalEObjectImpl.Container implements Reservation {
	/**
	 * The cached value of the '{@link #getExtra() <em>Extra</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtra()
	 * @generated
	 * @ordered
	 */
	protected EList<Extra> extra;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCheckInDate() <em>Check In Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckInDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECK_IN_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckInDate() <em>Check In Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckInDate()
	 * @generated
	 * @ordered
	 */
	protected Date checkInDate = CHECK_IN_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCheckOutDate() <em>Check Out Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckOutDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECK_OUT_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckOutDate() <em>Check Out Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckOutDate()
	 * @generated
	 * @ordered
	 */
	protected Date checkOutDate = CHECK_OUT_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomNbr() <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNbr()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_NBR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoomNbr() <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNbr()
	 * @generated
	 * @ordered
	 */
	protected int roomNbr = ROOM_NBR_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ReservationStatus STATUS_EDEFAULT = ReservationStatus.CANCEL;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected ReservationStatus status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getReservedRoomType() <em>Reserved Room Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedRoomType()
	 * @generated
	 * @ordered
	 */
	protected static final String RESERVED_ROOM_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReservedRoomType() <em>Reserved Room Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedRoomType()
	 * @generated
	 * @ordered
	 */
	protected String reservedRoomType = RESERVED_ROOM_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReservationPackage.Literals.RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extra> getExtra() {
		if (extra == null) {
			extra = new EObjectResolvingEList<Extra>(Extra.class, this, ReservationPackage.RESERVATION__EXTRA);
		}
		return extra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckInDate() {
		return checkInDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckInDate(Date newCheckInDate) {
		Date oldCheckInDate = checkInDate;
		checkInDate = newCheckInDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__CHECK_IN_DATE, oldCheckInDate, checkInDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckOutDate() {
		return checkOutDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckOutDate(Date newCheckOutDate) {
		Date oldCheckOutDate = checkOutDate;
		checkOutDate = newCheckOutDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__CHECK_OUT_DATE, oldCheckOutDate, checkOutDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomNbr() {
		return roomNbr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomNbr(int newRoomNbr) {
		int oldRoomNbr = roomNbr;
		roomNbr = newRoomNbr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__ROOM_NBR, oldRoomNbr, roomNbr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(ReservationStatus newStatus) {
		ReservationStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReservedRoomType() {
		return reservedRoomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservedRoomType(String newReservedRoomType) {
		String oldReservedRoomType = reservedRoomType;
		reservedRoomType = newReservedRoomType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION__RESERVED_ROOM_TYPE, oldReservedRoomType, reservedRoomType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ReservationPackage.RESERVATION__EXTRA:
				return getExtra();
			case ReservationPackage.RESERVATION__ID:
				return getId();
			case ReservationPackage.RESERVATION__CHECK_IN_DATE:
				return getCheckInDate();
			case ReservationPackage.RESERVATION__CHECK_OUT_DATE:
				return getCheckOutDate();
			case ReservationPackage.RESERVATION__ROOM_NBR:
				return getRoomNbr();
			case ReservationPackage.RESERVATION__STATUS:
				return getStatus();
			case ReservationPackage.RESERVATION__RESERVED_ROOM_TYPE:
				return getReservedRoomType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ReservationPackage.RESERVATION__EXTRA:
				getExtra().clear();
				getExtra().addAll((Collection<? extends Extra>)newValue);
				return;
			case ReservationPackage.RESERVATION__ID:
				setId((Integer)newValue);
				return;
			case ReservationPackage.RESERVATION__CHECK_IN_DATE:
				setCheckInDate((Date)newValue);
				return;
			case ReservationPackage.RESERVATION__CHECK_OUT_DATE:
				setCheckOutDate((Date)newValue);
				return;
			case ReservationPackage.RESERVATION__ROOM_NBR:
				setRoomNbr((Integer)newValue);
				return;
			case ReservationPackage.RESERVATION__STATUS:
				setStatus((ReservationStatus)newValue);
				return;
			case ReservationPackage.RESERVATION__RESERVED_ROOM_TYPE:
				setReservedRoomType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ReservationPackage.RESERVATION__EXTRA:
				getExtra().clear();
				return;
			case ReservationPackage.RESERVATION__ID:
				setId(ID_EDEFAULT);
				return;
			case ReservationPackage.RESERVATION__CHECK_IN_DATE:
				setCheckInDate(CHECK_IN_DATE_EDEFAULT);
				return;
			case ReservationPackage.RESERVATION__CHECK_OUT_DATE:
				setCheckOutDate(CHECK_OUT_DATE_EDEFAULT);
				return;
			case ReservationPackage.RESERVATION__ROOM_NBR:
				setRoomNbr(ROOM_NBR_EDEFAULT);
				return;
			case ReservationPackage.RESERVATION__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case ReservationPackage.RESERVATION__RESERVED_ROOM_TYPE:
				setReservedRoomType(RESERVED_ROOM_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ReservationPackage.RESERVATION__EXTRA:
				return extra != null && !extra.isEmpty();
			case ReservationPackage.RESERVATION__ID:
				return id != ID_EDEFAULT;
			case ReservationPackage.RESERVATION__CHECK_IN_DATE:
				return CHECK_IN_DATE_EDEFAULT == null ? checkInDate != null : !CHECK_IN_DATE_EDEFAULT.equals(checkInDate);
			case ReservationPackage.RESERVATION__CHECK_OUT_DATE:
				return CHECK_OUT_DATE_EDEFAULT == null ? checkOutDate != null : !CHECK_OUT_DATE_EDEFAULT.equals(checkOutDate);
			case ReservationPackage.RESERVATION__ROOM_NBR:
				return roomNbr != ROOM_NBR_EDEFAULT;
			case ReservationPackage.RESERVATION__STATUS:
				return status != STATUS_EDEFAULT;
			case ReservationPackage.RESERVATION__RESERVED_ROOM_TYPE:
				return RESERVED_ROOM_TYPE_EDEFAULT == null ? reservedRoomType != null : !RESERVED_ROOM_TYPE_EDEFAULT.equals(reservedRoomType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", checkInDate: ");
		result.append(checkInDate);
		result.append(", checkOutDate: ");
		result.append(checkOutDate);
		result.append(", roomNbr: ");
		result.append(roomNbr);
		result.append(", status: ");
		result.append(status);
		result.append(", reservedRoomType: ");
		result.append(reservedRoomType);
		result.append(')');
		return result.toString();
	}

} //ReservationImpl
