/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage;

import se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl;

import se.chalmers.cse.mdsd1617.group11.BankingModel.BankingModelPackage;

import se.chalmers.cse.mdsd1617.group11.BankingModel.impl.BankingModelPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Group11Package;

import se.chalmers.cse.mdsd1617.group11.Query.QueryPackage;

import se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl;

import se.chalmers.cse.mdsd1617.group11.Reservation.Extra;
import se.chalmers.cse.mdsd1617.group11.Reservation.Reservation;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationFactory;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage;

import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus;
import se.chalmers.cse.mdsd1617.group11.Room.RoomPackage;

import se.chalmers.cse.mdsd1617.group11.Room.impl.RoomPackageImpl;

import se.chalmers.cse.mdsd1617.group11.impl.Group11PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReservationPackageImpl extends EPackageImpl implements ReservationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reservationControllerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extraEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum reservationStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ReservationPackageImpl() {
		super(eNS_URI, ReservationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ReservationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ReservationPackage init() {
		if (isInited) return (ReservationPackage)EPackage.Registry.INSTANCE.getEPackage(ReservationPackage.eNS_URI);

		// Obtain or create and register package
		ReservationPackageImpl theReservationPackage = (ReservationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ReservationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ReservationPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group11PackageImpl theGroup11Package = (Group11PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group11Package.eNS_URI) instanceof Group11PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group11Package.eNS_URI) : Group11Package.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		BankingModelPackageImpl theBankingModelPackage = (BankingModelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BankingModelPackage.eNS_URI) instanceof BankingModelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BankingModelPackage.eNS_URI) : BankingModelPackage.eINSTANCE);
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) : AdminPackage.eINSTANCE);
		QueryPackageImpl theQueryPackage = (QueryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(QueryPackage.eNS_URI) instanceof QueryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(QueryPackage.eNS_URI) : QueryPackage.eINSTANCE);

		// Create package meta-data objects
		theReservationPackage.createPackageContents();
		theGroup11Package.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theBankingModelPackage.createPackageContents();
		theAdminPackage.createPackageContents();
		theQueryPackage.createPackageContents();

		// Initialize created meta-data
		theReservationPackage.initializePackageContents();
		theGroup11Package.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theBankingModelPackage.initializePackageContents();
		theAdminPackage.initializePackageContents();
		theQueryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theReservationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ReservationPackage.eNS_URI, theReservationPackage);
		return theReservationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReservationController() {
		return reservationControllerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReservationController_Reservation() {
		return (EReference)reservationControllerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservationController_AtomicId() {
		return (EAttribute)reservationControllerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetOccupiedReservations__Date() {
		return reservationControllerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetCheckIns__Date() {
		return reservationControllerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetCheckIns__Date_Date() {
		return reservationControllerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetCheckedOut__Date_Date() {
		return reservationControllerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__CheckInReservation__int_int_Date() {
		return reservationControllerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__InitiateReservation__String() {
		return reservationControllerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__ChangeRoomType__int_String() {
		return reservationControllerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__CheckOutReservation__int_Date() {
		return reservationControllerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetRoomType__int() {
		return reservationControllerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__CancelReservation__int() {
		return reservationControllerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__GetReservation__int() {
		return reservationControllerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__AddExtra__int_double_String() {
		return reservationControllerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReservationController__Restart() {
		return reservationControllerEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReservation() {
		return reservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReservation_Extra() {
		return (EReference)reservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_Id() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_CheckInDate() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_CheckOutDate() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_RoomNbr() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_Status() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_ReservedRoomType() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtra() {
		return extraEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_TypeOfExtra() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_Price() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReservationStatus() {
		return reservationStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationFactory getReservationFactory() {
		return (ReservationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		reservationControllerEClass = createEClass(RESERVATION_CONTROLLER);
		createEReference(reservationControllerEClass, RESERVATION_CONTROLLER__RESERVATION);
		createEAttribute(reservationControllerEClass, RESERVATION_CONTROLLER__ATOMIC_ID);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_OCCUPIED_RESERVATIONS__DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_CHECK_INS__DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_CHECK_INS__DATE_DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_CHECKED_OUT__DATE_DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___CHECK_IN_RESERVATION__INT_INT_DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___INITIATE_RESERVATION__STRING);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___CHECK_OUT_RESERVATION__INT_DATE);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_ROOM_TYPE__INT);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___CANCEL_RESERVATION__INT);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___GET_RESERVATION__INT);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___ADD_EXTRA__INT_DOUBLE_STRING);
		createEOperation(reservationControllerEClass, RESERVATION_CONTROLLER___RESTART);

		reservationEClass = createEClass(RESERVATION);
		createEReference(reservationEClass, RESERVATION__EXTRA);
		createEAttribute(reservationEClass, RESERVATION__ID);
		createEAttribute(reservationEClass, RESERVATION__CHECK_IN_DATE);
		createEAttribute(reservationEClass, RESERVATION__CHECK_OUT_DATE);
		createEAttribute(reservationEClass, RESERVATION__ROOM_NBR);
		createEAttribute(reservationEClass, RESERVATION__STATUS);
		createEAttribute(reservationEClass, RESERVATION__RESERVED_ROOM_TYPE);

		extraEClass = createEClass(EXTRA);
		createEAttribute(extraEClass, EXTRA__TYPE_OF_EXTRA);
		createEAttribute(extraEClass, EXTRA__PRICE);

		// Create enums
		reservationStatusEEnum = createEEnum(RESERVATION_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(reservationControllerEClass, ReservationController.class, "ReservationController", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReservationController_Reservation(), this.getReservation(), null, "reservation", null, 0, -1, ReservationController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservationController_AtomicId(), ecorePackage.getEInt(), "atomicId", null, 1, 1, ReservationController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = initEOperation(getReservationController__GetOccupiedReservations__Date(), this.getReservation(), "getOccupiedReservations", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__GetCheckIns__Date(), this.getReservation(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "checkInDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__GetCheckIns__Date_Date(), this.getReservation(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "checkInDateStart", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "checkInDateEnd", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__GetCheckedOut__Date_Date(), this.getReservation(), "getCheckedOut", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "from", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "to", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__CheckInReservation__int_int_Date(), ecorePackage.getEBoolean(), "checkInReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNo", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "checkInDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__InitiateReservation__String(), ecorePackage.getEInt(), "initiateReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypes", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__ChangeRoomType__int_String(), ecorePackage.getEBoolean(), "changeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__CheckOutReservation__int_Date(), ecorePackage.getEDouble(), "checkOutReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "checkoutDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__GetRoomType__int(), ecorePackage.getEString(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__CancelReservation__int(), ecorePackage.getEBoolean(), "cancelReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__GetReservation__int(), this.getReservation(), "getReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReservationController__AddExtra__int_double_String(), ecorePackage.getEBoolean(), "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "reservationID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "extraDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReservationController__Restart(), null, "restart", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(reservationEClass, Reservation.class, "Reservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReservation_Extra(), this.getExtra(), null, "extra", null, 0, -1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_CheckInDate(), ecorePackage.getEDate(), "checkInDate", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_CheckOutDate(), ecorePackage.getEDate(), "checkOutDate", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_RoomNbr(), ecorePackage.getEInt(), "roomNbr", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_Status(), this.getReservationStatus(), "status", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_ReservedRoomType(), ecorePackage.getEString(), "reservedRoomType", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(extraEClass, Extra.class, "Extra", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtra_TypeOfExtra(), ecorePackage.getEString(), "typeOfExtra", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getExtra_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(reservationStatusEEnum, ReservationStatus.class, "ReservationStatus");
		addEEnumLiteral(reservationStatusEEnum, ReservationStatus.CANCEL);
		addEEnumLiteral(reservationStatusEEnum, ReservationStatus.CLOSE);
		addEEnumLiteral(reservationStatusEEnum, ReservationStatus.OPEN);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";	
		addAnnotation
		  (reservationEClass, 
		   source, 
		   new String[] {
			 "originalName", "Reservation[]"
		   });
	}

} //ReservationPackageImpl
