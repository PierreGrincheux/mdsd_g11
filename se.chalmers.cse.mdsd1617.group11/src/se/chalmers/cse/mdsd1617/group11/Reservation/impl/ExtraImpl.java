/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group11.Reservation.Extra;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extra</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl#getTypeOfExtra <em>Type Of Extra</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl#getPrice <em>Price</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtraImpl extends MinimalEObjectImpl.Container implements Extra {
	/**
	 * The default value of the '{@link #getTypeOfExtra() <em>Type Of Extra</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOfExtra()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_OF_EXTRA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeOfExtra() <em>Type Of Extra</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOfExtra()
	 * @generated
	 * @ordered
	 */
	protected String typeOfExtra = TYPE_OF_EXTRA_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected double price = PRICE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtraImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReservationPackage.Literals.EXTRA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeOfExtra() {
		return typeOfExtra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeOfExtra(String newTypeOfExtra) {
		String oldTypeOfExtra = typeOfExtra;
		typeOfExtra = newTypeOfExtra;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.EXTRA__TYPE_OF_EXTRA, oldTypeOfExtra, typeOfExtra));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrice(double newPrice) {
		double oldPrice = price;
		price = newPrice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.EXTRA__PRICE, oldPrice, price));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ReservationPackage.EXTRA__TYPE_OF_EXTRA:
				return getTypeOfExtra();
			case ReservationPackage.EXTRA__PRICE:
				return getPrice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ReservationPackage.EXTRA__TYPE_OF_EXTRA:
				setTypeOfExtra((String)newValue);
				return;
			case ReservationPackage.EXTRA__PRICE:
				setPrice((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ReservationPackage.EXTRA__TYPE_OF_EXTRA:
				setTypeOfExtra(TYPE_OF_EXTRA_EDEFAULT);
				return;
			case ReservationPackage.EXTRA__PRICE:
				setPrice(PRICE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ReservationPackage.EXTRA__TYPE_OF_EXTRA:
				return TYPE_OF_EXTRA_EDEFAULT == null ? typeOfExtra != null : !TYPE_OF_EXTRA_EDEFAULT.equals(typeOfExtra);
			case ReservationPackage.EXTRA__PRICE:
				return price != PRICE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typeOfExtra: ");
		result.append(typeOfExtra);
		result.append(", price: ");
		result.append(price);
		result.append(')');
		return result.toString();
	}

} //ExtraImpl
