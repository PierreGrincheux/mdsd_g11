/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getExtra <em>Extra</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckInDate <em>Check In Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckOutDate <em>Check Out Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getRoomNbr <em>Room Nbr</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getReservedRoomType <em>Reserved Room Type</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation()
 * @model annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='Reservation[]'"
 * @generated
 */
public interface Reservation extends EObject {
	/**
	 * Returns the value of the '<em><b>Extra</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extra</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_Extra()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Extra> getExtra();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_Id()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Check In Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check In Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check In Date</em>' attribute.
	 * @see #setCheckInDate(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_CheckInDate()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getCheckInDate();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckInDate <em>Check In Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check In Date</em>' attribute.
	 * @see #getCheckInDate()
	 * @generated
	 */
	void setCheckInDate(Date value);

	/**
	 * Returns the value of the '<em><b>Check Out Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Out Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Out Date</em>' attribute.
	 * @see #setCheckOutDate(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_CheckOutDate()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getCheckOutDate();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckOutDate <em>Check Out Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Out Date</em>' attribute.
	 * @see #getCheckOutDate()
	 * @generated
	 */
	void setCheckOutDate(Date value);

	/**
	 * Returns the value of the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Nbr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Nbr</em>' attribute.
	 * @see #setRoomNbr(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_RoomNbr()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomNbr();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getRoomNbr <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Nbr</em>' attribute.
	 * @see #getRoomNbr()
	 * @generated
	 */
	void setRoomNbr(int value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus
	 * @see #setStatus(ReservationStatus)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_Status()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	ReservationStatus getStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(ReservationStatus value);

	/**
	 * Returns the value of the '<em><b>Reserved Room Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved Room Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved Room Type</em>' attribute.
	 * @see #setReservedRoomType(String)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getReservation_ReservedRoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getReservedRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getReservedRoomType <em>Reserved Room Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reserved Room Type</em>' attribute.
	 * @see #getReservedRoomType()
	 * @generated
	 */
	void setReservedRoomType(String value);

} // Reservation
