/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationFactory
 * @model kind="package"
 * @generated
 */
public interface ReservationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Reservation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group11/Reservation.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group11.Reservation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ReservationPackage eINSTANCE = se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservationController()
	 * @generated
	 */
	int RESERVATION_CONTROLLER = 0;

	/**
	 * The feature id for the '<em><b>Reservation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER__RESERVATION = 0;

	/**
	 * The feature id for the '<em><b>Atomic Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER__ATOMIC_ID = 1;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Occupied Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_OCCUPIED_RESERVATIONS__DATE = 0;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_CHECK_INS__DATE = 1;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_CHECK_INS__DATE_DATE = 2;

	/**
	 * The operation id for the '<em>Get Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_CHECKED_OUT__DATE_DATE = 3;

	/**
	 * The operation id for the '<em>Check In Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___CHECK_IN_RESERVATION__INT_INT_DATE = 4;

	/**
	 * The operation id for the '<em>Initiate Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___INITIATE_RESERVATION__STRING = 5;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING = 6;

	/**
	 * The operation id for the '<em>Check Out Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___CHECK_OUT_RESERVATION__INT_DATE = 7;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_ROOM_TYPE__INT = 8;

	/**
	 * The operation id for the '<em>Cancel Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___CANCEL_RESERVATION__INT = 9;

	/**
	 * The operation id for the '<em>Get Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___GET_RESERVATION__INT = 10;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___ADD_EXTRA__INT_DOUBLE_STRING = 11;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER___RESTART = 12;

	/**
	 * The number of operations of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_CONTROLLER_OPERATION_COUNT = 13;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl <em>Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservation()
	 * @generated
	 */
	int RESERVATION = 1;

	/**
	 * The feature id for the '<em><b>Extra</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__EXTRA = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__ID = 1;

	/**
	 * The feature id for the '<em><b>Check In Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__CHECK_IN_DATE = 2;

	/**
	 * The feature id for the '<em><b>Check Out Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__CHECK_OUT_DATE = 3;

	/**
	 * The feature id for the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__ROOM_NBR = 4;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__STATUS = 5;

	/**
	 * The feature id for the '<em><b>Reserved Room Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__RESERVED_ROOM_TYPE = 6;

	/**
	 * The number of structural features of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl <em>Extra</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getExtra()
	 * @generated
	 */
	int EXTRA = 2;

	/**
	 * The feature id for the '<em><b>Type Of Extra</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__TYPE_OF_EXTRA = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__PRICE = 1;

	/**
	 * The number of structural features of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservationStatus()
	 * @generated
	 */
	int RESERVATION_STATUS = 3;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController
	 * @generated
	 */
	EClass getReservationController();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getReservation <em>Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getReservation()
	 * @see #getReservationController()
	 * @generated
	 */
	EReference getReservationController_Reservation();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getAtomicId <em>Atomic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Atomic Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getAtomicId()
	 * @see #getReservationController()
	 * @generated
	 */
	EAttribute getReservationController_AtomicId();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getOccupiedReservations(java.util.Date) <em>Get Occupied Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getOccupiedReservations(java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__GetOccupiedReservations__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckIns(java.util.Date) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckIns(java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__GetCheckIns__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckIns(java.util.Date, java.util.Date) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__GetCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckedOut(java.util.Date, java.util.Date) <em>Get Checked Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getCheckedOut(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__GetCheckedOut__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#checkInReservation(int, int, java.util.Date) <em>Check In Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#checkInReservation(int, int, java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__CheckInReservation__int_int_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#initiateReservation(java.lang.String) <em>Initiate Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#initiateReservation(java.lang.String)
	 * @generated
	 */
	EOperation getReservationController__InitiateReservation__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#changeRoomType(int, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#changeRoomType(int, java.lang.String)
	 * @generated
	 */
	EOperation getReservationController__ChangeRoomType__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#checkOutReservation(int, java.util.Date) <em>Check Out Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#checkOutReservation(int, java.util.Date)
	 * @generated
	 */
	EOperation getReservationController__CheckOutReservation__int_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getRoomType(int) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getRoomType(int)
	 * @generated
	 */
	EOperation getReservationController__GetRoomType__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#cancelReservation(int) <em>Cancel Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#cancelReservation(int)
	 * @generated
	 */
	EOperation getReservationController__CancelReservation__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getReservation(int) <em>Get Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#getReservation(int)
	 * @generated
	 */
	EOperation getReservationController__GetReservation__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#addExtra(int, double, java.lang.String) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#addExtra(int, double, java.lang.String)
	 * @generated
	 */
	EOperation getReservationController__AddExtra__int_double_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#restart() <em>Restart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Restart</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController#restart()
	 * @generated
	 */
	EOperation getReservationController__Restart();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation <em>Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation
	 * @generated
	 */
	EClass getReservation();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getExtra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getExtra()
	 * @see #getReservation()
	 * @generated
	 */
	EReference getReservation_Extra();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getId()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckInDate <em>Check In Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check In Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckInDate()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_CheckInDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckOutDate <em>Check Out Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check Out Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getCheckOutDate()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_CheckOutDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getRoomNbr <em>Room Nbr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Nbr</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getRoomNbr()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_RoomNbr();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getStatus()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_Status();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getReservedRoomType <em>Reserved Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reserved Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Reservation#getReservedRoomType()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_ReservedRoomType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Extra
	 * @generated
	 */
	EClass getExtra();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getTypeOfExtra <em>Type Of Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Of Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getTypeOfExtra()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_TypeOfExtra();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getPrice()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Price();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus
	 * @generated
	 */
	EEnum getReservationStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ReservationFactory getReservationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservationController()
		 * @generated
		 */
		EClass RESERVATION_CONTROLLER = eINSTANCE.getReservationController();

		/**
		 * The meta object literal for the '<em><b>Reservation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION_CONTROLLER__RESERVATION = eINSTANCE.getReservationController_Reservation();

		/**
		 * The meta object literal for the '<em><b>Atomic Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION_CONTROLLER__ATOMIC_ID = eINSTANCE.getReservationController_AtomicId();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_OCCUPIED_RESERVATIONS__DATE = eINSTANCE.getReservationController__GetOccupiedReservations__Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_CHECK_INS__DATE = eINSTANCE.getReservationController__GetCheckIns__Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_CHECK_INS__DATE_DATE = eINSTANCE.getReservationController__GetCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Checked Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_CHECKED_OUT__DATE_DATE = eINSTANCE.getReservationController__GetCheckedOut__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Check In Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___CHECK_IN_RESERVATION__INT_INT_DATE = eINSTANCE.getReservationController__CheckInReservation__int_int_Date();

		/**
		 * The meta object literal for the '<em><b>Initiate Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___INITIATE_RESERVATION__STRING = eINSTANCE.getReservationController__InitiateReservation__String();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING = eINSTANCE.getReservationController__ChangeRoomType__int_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___CHECK_OUT_RESERVATION__INT_DATE = eINSTANCE.getReservationController__CheckOutReservation__int_Date();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_ROOM_TYPE__INT = eINSTANCE.getReservationController__GetRoomType__int();

		/**
		 * The meta object literal for the '<em><b>Cancel Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___CANCEL_RESERVATION__INT = eINSTANCE.getReservationController__CancelReservation__int();

		/**
		 * The meta object literal for the '<em><b>Get Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___GET_RESERVATION__INT = eINSTANCE.getReservationController__GetReservation__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___ADD_EXTRA__INT_DOUBLE_STRING = eINSTANCE.getReservationController__AddExtra__int_double_String();

		/**
		 * The meta object literal for the '<em><b>Restart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESERVATION_CONTROLLER___RESTART = eINSTANCE.getReservationController__Restart();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl <em>Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservation()
		 * @generated
		 */
		EClass RESERVATION = eINSTANCE.getReservation();

		/**
		 * The meta object literal for the '<em><b>Extra</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION__EXTRA = eINSTANCE.getReservation_Extra();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__ID = eINSTANCE.getReservation_Id();

		/**
		 * The meta object literal for the '<em><b>Check In Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__CHECK_IN_DATE = eINSTANCE.getReservation_CheckInDate();

		/**
		 * The meta object literal for the '<em><b>Check Out Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__CHECK_OUT_DATE = eINSTANCE.getReservation_CheckOutDate();

		/**
		 * The meta object literal for the '<em><b>Room Nbr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__ROOM_NBR = eINSTANCE.getReservation_RoomNbr();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__STATUS = eINSTANCE.getReservation_Status();

		/**
		 * The meta object literal for the '<em><b>Reserved Room Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__RESERVED_ROOM_TYPE = eINSTANCE.getReservation_ReservedRoomType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl <em>Extra</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ExtraImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getExtra()
		 * @generated
		 */
		EClass EXTRA = eINSTANCE.getExtra();

		/**
		 * The meta object literal for the '<em><b>Type Of Extra</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__TYPE_OF_EXTRA = eINSTANCE.getExtra_TypeOfExtra();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__PRICE = eINSTANCE.getExtra_Price();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus
		 * @see se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationPackageImpl#getReservationStatus()
		 * @generated
		 */
		EEnum RESERVATION_STATUS = eINSTANCE.getReservationStatus();

	}

} //ReservationPackage
