/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group11.Reservation.Extra;
import se.chalmers.cse.mdsd1617.group11.Reservation.Reservation;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationFactory;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl#getReservation <em>Reservation</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.impl.ReservationControllerImpl#getAtomicId <em>Atomic Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReservationControllerImpl extends MinimalEObjectImpl.Container implements ReservationController {
	/**
	 * The cached value of the '{@link #getReservation() <em>Reservation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservation()
	 * @generated
	 * @ordered
	 */
	protected EList<Reservation> reservation;

	/**
	 * The default value of the '{@link #getAtomicId() <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicId()
	 * @generated
	 * @ordered
	 */
	protected static final int ATOMIC_ID_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getAtomicId() <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicId()
	 * @generated
	 * @ordered
	 */
	protected int atomicId = ATOMIC_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReservationControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReservationPackage.Literals.RESERVATION_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reservation> getReservation() {
		if (reservation == null) {
			reservation = new EObjectResolvingEList<Reservation>(Reservation.class, this, ReservationPackage.RESERVATION_CONTROLLER__RESERVATION);
		}
		return reservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAtomicId() {
		return atomicId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicId(int newAtomicId) {
		int oldAtomicId = atomicId;
		atomicId = newAtomicId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReservationPackage.RESERVATION_CONTROLLER__ATOMIC_ID, oldAtomicId, atomicId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Reservation> getOccupiedReservations(Date date) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getCheckInDate().before(date) && reservation.getCheckOutDate().after(date))
			.forEach(reservation -> list.add(reservation));
		
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Reservation> getCheckIns(Date checkInDate) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getCheckInDate().equals(checkInDate))
			.forEach(reservation -> list.add(reservation));
			
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Reservation> getCheckIns(Date checkInDateStart, Date checkInDateEnd) {

		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getCheckInDate().after(checkInDateStart) && reservation.getCheckInDate().before(checkInDateEnd))
			.forEach(reservation -> list.add(reservation));
		
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Reservation> getCheckedOut(Date from, Date to) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getCheckOutDate().after(from) && reservation.getCheckOutDate().before(to))
			.forEach(reservation -> list.add(reservation));
		
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInReservation(int reservation, int roomNo, Date checkInDate) {
		Reservation res = this.getReservation(reservation);
		if (res.getCheckInDate() == null && res.getStatus() == ReservationStatus.OPEN){
			res.setCheckInDate(checkInDate);
			res.setRoomNbr(roomNo);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateReservation(String roomTypes) {
		Reservation res = ReservationFactoryImpl.eINSTANCE.createReservation();
		res.setStatus(ReservationStatus.OPEN);
		res.setReservedRoomType(roomTypes);
		int newID = this.atomicId++;
		res.setId(newID);
		
		this.getReservation().add(res);
		return newID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomType(int reservationID, String newRoomType) {
		this.getReservation(reservationID).setReservedRoomType(newRoomType);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double checkOutReservation(int reservationID, Date checkoutDate) {
 		Reservation res = this.getReservation(reservationID);
		res.setCheckOutDate(checkoutDate);
 		
 		return res.getExtra()
 			.stream()
 			.mapToDouble(extra -> extra.getPrice())
 			.sum();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getRoomType(int reservationID) {
		return this.getReservation(reservationID).getReservedRoomType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelReservation(int reservationID) {
		Reservation reservation = getReservation(reservationID);
		if (reservation != null && reservation.getStatus() == ReservationStatus.OPEN){
			reservation.setStatus(ReservationStatus.CANCEL);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Reservation getReservation(int reservationID) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getId() == reservationID)
			.forEach(reservation -> list.add(reservation));
		
		if (list.isEmpty())
			return null;
		
		return list.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtra(int reservationID, double price, String extraDescription) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservation.stream()
			.filter(reservation -> reservation.getId() == reservationID)
			.forEach(reservation -> list.add(reservation));
		
		Reservation tempReservation = list.get(0);
		
		if (tempReservation == null) {
			return false;
		}
		
		Extra newExtra = ReservationFactory.eINSTANCE.createExtra();
		newExtra.setPrice(price);
		newExtra.setTypeOfExtra(extraDescription);
		tempReservation.getExtra().add(newExtra);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void restart() {
		this.reservation = new EObjectResolvingEList<Reservation>(Reservation.class, this, ReservationPackage.RESERVATION_CONTROLLER__RESERVATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ReservationPackage.RESERVATION_CONTROLLER__RESERVATION:
				return getReservation();
			case ReservationPackage.RESERVATION_CONTROLLER__ATOMIC_ID:
				return getAtomicId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ReservationPackage.RESERVATION_CONTROLLER__RESERVATION:
				getReservation().clear();
				getReservation().addAll((Collection<? extends Reservation>)newValue);
				return;
			case ReservationPackage.RESERVATION_CONTROLLER__ATOMIC_ID:
				setAtomicId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ReservationPackage.RESERVATION_CONTROLLER__RESERVATION:
				getReservation().clear();
				return;
			case ReservationPackage.RESERVATION_CONTROLLER__ATOMIC_ID:
				setAtomicId(ATOMIC_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ReservationPackage.RESERVATION_CONTROLLER__RESERVATION:
				return reservation != null && !reservation.isEmpty();
			case ReservationPackage.RESERVATION_CONTROLLER__ATOMIC_ID:
				return atomicId != ATOMIC_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ReservationPackage.RESERVATION_CONTROLLER___GET_OCCUPIED_RESERVATIONS__DATE:
				return getOccupiedReservations((Date)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___GET_CHECK_INS__DATE:
				return getCheckIns((Date)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___GET_CHECK_INS__DATE_DATE:
				return getCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case ReservationPackage.RESERVATION_CONTROLLER___GET_CHECKED_OUT__DATE_DATE:
				return getCheckedOut((Date)arguments.get(0), (Date)arguments.get(1));
			case ReservationPackage.RESERVATION_CONTROLLER___CHECK_IN_RESERVATION__INT_INT_DATE:
				return checkInReservation((Integer)arguments.get(0), (Integer)arguments.get(1), (Date)arguments.get(2));
			case ReservationPackage.RESERVATION_CONTROLLER___INITIATE_RESERVATION__STRING:
				return initiateReservation((String)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1));
			case ReservationPackage.RESERVATION_CONTROLLER___CHECK_OUT_RESERVATION__INT_DATE:
				return checkOutReservation((Integer)arguments.get(0), (Date)arguments.get(1));
			case ReservationPackage.RESERVATION_CONTROLLER___GET_ROOM_TYPE__INT:
				return getRoomType((Integer)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___CANCEL_RESERVATION__INT:
				return cancelReservation((Integer)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___GET_RESERVATION__INT:
				return getReservation((Integer)arguments.get(0));
			case ReservationPackage.RESERVATION_CONTROLLER___ADD_EXTRA__INT_DOUBLE_STRING:
				return addExtra((Integer)arguments.get(0), (Double)arguments.get(1), (String)arguments.get(2));
			case ReservationPackage.RESERVATION_CONTROLLER___RESTART:
				restart();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (atomicId: ");
		result.append(atomicId);
		result.append(')');
		return result.toString();
	}
	
} //ReservationControllerImpl
