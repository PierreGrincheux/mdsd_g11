/**
 */
package se.chalmers.cse.mdsd1617.group11.Reservation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extra</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getTypeOfExtra <em>Type Of Extra</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getPrice <em>Price</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getExtra()
 * @model
 * @generated
 */
public interface Extra extends EObject {
	/**
	 * Returns the value of the '<em><b>Type Of Extra</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Of Extra</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Of Extra</em>' attribute.
	 * @see #setTypeOfExtra(String)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getExtra_TypeOfExtra()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getTypeOfExtra();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getTypeOfExtra <em>Type Of Extra</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Of Extra</em>' attribute.
	 * @see #getTypeOfExtra()
	 * @generated
	 */
	void setTypeOfExtra(String value);

	/**
	 * Returns the value of the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price</em>' attribute.
	 * @see #setPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group11.Reservation.ReservationPackage#getExtra_Price()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Reservation.Extra#getPrice <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price</em>' attribute.
	 * @see #getPrice()
	 * @generated
	 */
	void setPrice(double value);

} // Extra
