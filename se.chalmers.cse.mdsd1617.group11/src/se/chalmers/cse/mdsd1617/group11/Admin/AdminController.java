/**
 */
package se.chalmers.cse.mdsd1617.group11.Admin;

import org.eclipse.emf.common.util.EList;
import se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController;

import se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIadminroom <em>Iadminroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIstartupquerycontroller <em>Istartupquerycontroller</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage#getAdminController()
 * @model
 * @generated
 */
public interface AdminController extends IHotelStartupProvides {
	/**
	 * Returns the value of the '<em><b>Iadminroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iadminroom</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iadminroom</em>' reference.
	 * @see #setIadminroom(IAdminRoom)
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage#getAdminController_Iadminroom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IAdminRoom getIadminroom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIadminroom <em>Iadminroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iadminroom</em>' reference.
	 * @see #getIadminroom()
	 * @generated
	 */
	void setIadminroom(IAdminRoom value);

	/**
	 * Returns the value of the '<em><b>Istartupquerycontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Istartupquerycontroller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Istartupquerycontroller</em>' reference.
	 * @see #setIstartupquerycontroller(IStartupQueryController)
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage#getAdminController_Istartupquerycontroller()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IStartupQueryController getIstartupquerycontroller();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIstartupquerycontroller <em>Istartupquerycontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Istartupquerycontroller</em>' reference.
	 * @see #getIstartupquerycontroller()
	 * @generated
	 */
	void setIstartupquerycontroller(IStartupQueryController value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" RoomNoRequired="true" RoomNoOrdered="false"
	 * @generated
	 */
	boolean addRoom(RoomType roomType, int RoomNo);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean blockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeOfRoom(int roomNbr, RoomType newRoomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" featuresRequired="true" featuresOrdered="false"
	 * @generated
	 */
	boolean addRoomType(String name, double price, int nbrOfBeds, String features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldRoomTypeNameRequired="true" oldRoomTypeNameOrdered="false" nameRequired="true" nameOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" priceRequired="true" priceOrdered="false" newFeaturesRequired="true" newFeaturesOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String oldRoomTypeName, String name, int nbrOfBeds, double price, String newFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean unblockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomNoRequired="true" roomNoOrdered="false"
	 * @generated
	 */
	EList<RoomType> getOtherRoomTypes(int roomNo);

} // AdminController
