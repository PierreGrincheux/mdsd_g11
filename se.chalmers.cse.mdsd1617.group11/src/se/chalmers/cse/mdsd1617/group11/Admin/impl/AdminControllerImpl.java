/**
 */
package se.chalmers.cse.mdsd1617.group11.Admin.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group11.Admin.AdminController;
import se.chalmers.cse.mdsd1617.group11.Admin.AdminPackage;

import se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController;
import se.chalmers.cse.mdsd1617.group11.Query.QueryFactory;
import se.chalmers.cse.mdsd1617.group11.Room.IAdminRoom;
import se.chalmers.cse.mdsd1617.group11.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl#getIadminroom <em>Iadminroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl#getIstartupquerycontroller <em>Istartupquerycontroller</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AdminControllerImpl extends MinimalEObjectImpl.Container implements AdminController {
	
	private static final String DEFAULT_ROOM_TYPE_NAME = "Standard";
	private static final double DEFAULT_ROOM_TYPE_PRICE = 100;
	private static final int DEFAULT_ROOM_TYPE_NBROFBEDS = 2;	
	private static final String DEFAULT_ROOM_TYPE_FEATURES = "Minibar, Airconditions, Wifi";
	
	private boolean SYSTEM_STARTED = false;
	
	/**
	 * The cached value of the '{@link #getIadminroom() <em>Iadminroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIadminroom()
	 * @generated
	 * @ordered
	 */
	protected IAdminRoom iadminroom;

	/**
	 * The cached value of the '{@link #getIstartupquerycontroller() <em>Istartupquerycontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIstartupquerycontroller()
	 * @generated
	 * @ordered
	 */
	protected IStartupQueryController istartupquerycontroller;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected AdminControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdminPackage.Literals.ADMIN_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAdminRoom getIadminroom() {
		if (iadminroom != null && iadminroom.eIsProxy()) {
			InternalEObject oldIadminroom = (InternalEObject)iadminroom;
			iadminroom = (IAdminRoom)eResolveProxy(oldIadminroom);
			if (iadminroom != oldIadminroom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdminPackage.ADMIN_CONTROLLER__IADMINROOM, oldIadminroom, iadminroom));
			}
		}
		return iadminroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAdminRoom basicGetIadminroom() {
		return iadminroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIadminroom(IAdminRoom newIadminroom) {
		IAdminRoom oldIadminroom = iadminroom;
		iadminroom = newIadminroom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdminPackage.ADMIN_CONTROLLER__IADMINROOM, oldIadminroom, iadminroom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IStartupQueryController getIstartupquerycontroller() {
		if (istartupquerycontroller != null && istartupquerycontroller.eIsProxy()) {
			InternalEObject oldIstartupquerycontroller = (InternalEObject)istartupquerycontroller;
			istartupquerycontroller = (IStartupQueryController)eResolveProxy(oldIstartupquerycontroller);
			if (istartupquerycontroller != oldIstartupquerycontroller) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER, oldIstartupquerycontroller, istartupquerycontroller));
			}
		}
		return istartupquerycontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IStartupQueryController basicGetIstartupquerycontroller() {
		return istartupquerycontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIstartupquerycontroller(IStartupQueryController newIstartupquerycontroller) {
		IStartupQueryController oldIstartupquerycontroller = istartupquerycontroller;
		istartupquerycontroller = newIstartupquerycontroller;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER, oldIstartupquerycontroller, istartupquerycontroller));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Starts the hotel system and sets the number of default rooms.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		if (SYSTEM_STARTED){
			iadminroom.restart();
			istartupquerycontroller.restart();
		} else {
			iadminroom = RoomFactory.eINSTANCE.createRoomController();
			istartupquerycontroller = QueryFactory.eINSTANCE.createQueryController();
			istartupquerycontroller.startup();
			SYSTEM_STARTED = true;
		}
		
		if (numRooms > 0){
			iadminroom.addRoomType(DEFAULT_ROOM_TYPE_NAME, DEFAULT_ROOM_TYPE_PRICE, DEFAULT_ROOM_TYPE_NBROFBEDS, DEFAULT_ROOM_TYPE_FEATURES);
			for (int i = 1; i <= numRooms; i++){
				iadminroom.addRoom(iadminroom.getRoomTypes().get(0), i);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(RoomType roomType, int RoomNo) {
		return iadminroom.addRoom(roomType, RoomNo);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		return iadminroom.removeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNbr) {
		return iadminroom.blockRoom(roomNbr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeOfRoom(int roomNbr, RoomType newRoomType) {
		return iadminroom.changeRoomTypeOfRoom(roomNbr, newRoomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, double price, int nbrOfBeds, String features) {
		return iadminroom.addRoomType(name, price, nbrOfBeds, features);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String name) {
		return iadminroom.removeRoomType(name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldRoomTypeName, String name, int nbrOfBeds, double price, String newFeatures) {
		return iadminroom.updateRoomType(oldRoomTypeName, name, price, nbrOfBeds, newFeatures);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Sets the room status to available if it was blocked, and returns true. Else returns false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNbr) {
		return iadminroom.unblockRoom(roomNbr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getOtherRoomTypes(int roomNo) {
		RoomType type = iadminroom.getRoomTypeOfRoom(roomNo);
		EList<RoomType> allTypes = iadminroom.getRoomTypes();
		if (type != null && allTypes != null && allTypes.size()>0){
			allTypes.remove(type);
			return allTypes;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdminPackage.ADMIN_CONTROLLER__IADMINROOM:
				if (resolve) return getIadminroom();
				return basicGetIadminroom();
			case AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER:
				if (resolve) return getIstartupquerycontroller();
				return basicGetIstartupquerycontroller();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdminPackage.ADMIN_CONTROLLER__IADMINROOM:
				setIadminroom((IAdminRoom)newValue);
				return;
			case AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER:
				setIstartupquerycontroller((IStartupQueryController)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdminPackage.ADMIN_CONTROLLER__IADMINROOM:
				setIadminroom((IAdminRoom)null);
				return;
			case AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER:
				setIstartupquerycontroller((IStartupQueryController)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdminPackage.ADMIN_CONTROLLER__IADMINROOM:
				return iadminroom != null;
			case AdminPackage.ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER:
				return istartupquerycontroller != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AdminPackage.ADMIN_CONTROLLER___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
			case AdminPackage.ADMIN_CONTROLLER___ADD_ROOM__ROOMTYPE_INT:
				return addRoom((RoomType)arguments.get(0), (Integer)arguments.get(1));
			case AdminPackage.ADMIN_CONTROLLER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case AdminPackage.ADMIN_CONTROLLER___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case AdminPackage.ADMIN_CONTROLLER___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE:
				return changeRoomTypeOfRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case AdminPackage.ADMIN_CONTROLLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case AdminPackage.ADMIN_CONTROLLER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case AdminPackage.ADMIN_CONTROLLER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_STRING:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Double)arguments.get(3), (String)arguments.get(4));
			case AdminPackage.ADMIN_CONTROLLER___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case AdminPackage.ADMIN_CONTROLLER___GET_OTHER_ROOM_TYPES__INT:
				return getOtherRoomTypes((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AdminControllerImpl
