/**
 */
package se.chalmers.cse.mdsd1617.group11.Admin;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminFactory
 * @model kind="package"
 * @generated
 */
public interface AdminPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Admin";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group11/Admin.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group11.Admin";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdminPackage eINSTANCE = se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 1;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl#getAdminController()
	 * @generated
	 */
	int ADMIN_CONTROLLER = 0;

	/**
	 * The feature id for the '<em><b>Iadminroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER__IADMINROOM = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Istartupquerycontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___ADD_ROOM__ROOMTYPE_INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___REMOVE_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___BLOCK_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___REMOVE_ROOM_TYPE__STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___UNBLOCK_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Other Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER___GET_OTHER_ROOM_TYPES__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_CONTROLLER_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 9;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController
	 * @generated
	 */
	EClass getAdminController();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIadminroom <em>Iadminroom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iadminroom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIadminroom()
	 * @see #getAdminController()
	 * @generated
	 */
	EReference getAdminController_Iadminroom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIstartupquerycontroller <em>Istartupquerycontroller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Istartupquerycontroller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getIstartupquerycontroller()
	 * @see #getAdminController()
	 * @generated
	 */
	EReference getAdminController_Istartupquerycontroller();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#addRoom(se.chalmers.cse.mdsd1617.group11.Room.RoomType, int) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#addRoom(se.chalmers.cse.mdsd1617.group11.Room.RoomType, int)
	 * @generated
	 */
	EOperation getAdminController__AddRoom__RoomType_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#removeRoom(int)
	 * @generated
	 */
	EOperation getAdminController__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#blockRoom(int)
	 * @generated
	 */
	EOperation getAdminController__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#changeRoomTypeOfRoom(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#changeRoomTypeOfRoom(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType)
	 * @generated
	 */
	EOperation getAdminController__ChangeRoomTypeOfRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getAdminController__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getAdminController__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#updateRoomType(java.lang.String, java.lang.String, int, double, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#updateRoomType(java.lang.String, java.lang.String, int, double, java.lang.String)
	 * @generated
	 */
	EOperation getAdminController__UpdateRoomType__String_String_int_double_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#unblockRoom(int)
	 * @generated
	 */
	EOperation getAdminController__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getOtherRoomTypes(int) <em>Get Other Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Other Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.AdminController#getOtherRoomTypes(int)
	 * @generated
	 */
	EOperation getAdminController__GetOtherRoomTypes__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdminFactory getAdminFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminControllerImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl#getAdminController()
		 * @generated
		 */
		EClass ADMIN_CONTROLLER = eINSTANCE.getAdminController();

		/**
		 * The meta object literal for the '<em><b>Iadminroom</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN_CONTROLLER__IADMINROOM = eINSTANCE.getAdminController_Iadminroom();

		/**
		 * The meta object literal for the '<em><b>Istartupquerycontroller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN_CONTROLLER__ISTARTUPQUERYCONTROLLER = eINSTANCE.getAdminController_Istartupquerycontroller();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___ADD_ROOM__ROOMTYPE_INT = eINSTANCE.getAdminController__AddRoom__RoomType_int();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___REMOVE_ROOM__INT = eINSTANCE.getAdminController__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___BLOCK_ROOM__INT = eINSTANCE.getAdminController__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___CHANGE_ROOM_TYPE_OF_ROOM__INT_ROOMTYPE = eINSTANCE.getAdminController__ChangeRoomTypeOfRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getAdminController__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getAdminController__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_STRING = eINSTANCE.getAdminController__UpdateRoomType__String_String_int_double_String();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___UNBLOCK_ROOM__INT = eINSTANCE.getAdminController__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Other Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN_CONTROLLER___GET_OTHER_ROOM_TYPES__INT = eINSTANCE.getAdminController__GetOtherRoomTypes__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Admin.IHotelStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group11.Admin.impl.AdminPackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

	}

} //AdminPackage
