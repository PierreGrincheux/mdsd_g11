/**
 */
package se.chalmers.cse.mdsd1617.group11.Booking;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getAtomicId <em>Atomic Id</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBookingController()
 * @model
 * @generated
 */
public interface BookingController extends EObject {
	/**
	 * Returns the value of the '<em><b>Booking</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group11.Booking.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBookingController_Booking()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> getBooking();

	/**
	 * Returns the value of the '<em><b>Atomic Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Id</em>' attribute.
	 * @see #setAtomicId(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBookingController_AtomicId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getAtomicId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getAtomicId <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Id</em>' attribute.
	 * @see #getAtomicId()
	 * @generated
	 */
	void setAtomicId(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	Date getReservationToDate(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	Date getReservationFromDate(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false"
	 * @generated
	 */
	Booking getBookingByReservation(int reservationID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	EList<Integer> getReservations(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true" ordered="false" startDateUnique="false" startDateRequired="true" startDateOrdered="false" endDateUnique="false" endDateRequired="true" endDateOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	int initiateBooking(Date startDate, Date endDate, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIDRequired="true" reservationIDOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean addReservationToBooking(int reservationID, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" fromRequired="true" fromOrdered="false" toDateRequired="true" toDateOrdered="false"
	 * @generated
	 */
	boolean changeBookingDate(int bookingID, Date from, Date toDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	Booking getBooking(int bookingId);
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void restart();

} // BookingController
