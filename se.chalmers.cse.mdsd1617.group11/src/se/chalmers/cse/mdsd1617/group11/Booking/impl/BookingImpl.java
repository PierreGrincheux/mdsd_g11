/**
 */
package se.chalmers.cse.mdsd1617.group11.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import se.chalmers.cse.mdsd1617.group11.Booking.Booking;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group11.Booking.People;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getReservedFrom <em>Reserved From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getReservedTo <em>Reserved To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getPeople <em>People</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl#getStatus <em>Status</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getReservedFrom() <em>Reserved From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedFrom()
	 * @generated
	 * @ordered
	 */
	protected static final Date RESERVED_FROM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReservedFrom() <em>Reserved From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedFrom()
	 * @generated
	 * @ordered
	 */
	protected Date reservedFrom = RESERVED_FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getReservedTo() <em>Reserved To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedTo()
	 * @generated
	 * @ordered
	 */
	protected static final Date RESERVED_TO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReservedTo() <em>Reserved To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedTo()
	 * @generated
	 * @ordered
	 */
	protected Date reservedTo = RESERVED_TO_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReservations() <em>Reservations</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservations()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> reservations;

	/**
	 * The cached value of the '{@link #getPeople() <em>People</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeople()
	 * @generated
	 * @ordered
	 */
	protected People people;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final BookingStatus STATUS_EDEFAULT = BookingStatus.INITIATED;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected BookingStatus status = STATUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getReservedFrom() {
		return reservedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservedFrom(Date newReservedFrom) {
		Date oldReservedFrom = reservedFrom;
		reservedFrom = newReservedFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__RESERVED_FROM, oldReservedFrom, reservedFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getReservedTo() {
		return reservedTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservedTo(Date newReservedTo) {
		Date oldReservedTo = reservedTo;
		reservedTo = newReservedTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__RESERVED_TO, oldReservedTo, reservedTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getReservations() {
		if (reservations == null) {
			reservations = new EDataTypeUniqueEList<Integer>(Integer.class, this, BookingPackage.BOOKING__RESERVATIONS);
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public People getPeople() {
		if (people != null && people.eIsProxy()) {
			InternalEObject oldPeople = (InternalEObject)people;
			people = (People)eResolveProxy(oldPeople);
			if (people != oldPeople) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING__PEOPLE, oldPeople, people));
			}
		}
		return people;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public People basicGetPeople() {
		return people;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeople(People newPeople) {
		People oldPeople = people;
		people = newPeople;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__PEOPLE, oldPeople, people));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(BookingStatus newStatus) {
		BookingStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int addReservation(int reservationId) {
		this.getReservations().add(reservationId);
		
		return this.reservations.size() - 1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING__ID:
				return getId();
			case BookingPackage.BOOKING__RESERVED_FROM:
				return getReservedFrom();
			case BookingPackage.BOOKING__RESERVED_TO:
				return getReservedTo();
			case BookingPackage.BOOKING__RESERVATIONS:
				return getReservations();
			case BookingPackage.BOOKING__PEOPLE:
				if (resolve) return getPeople();
				return basicGetPeople();
			case BookingPackage.BOOKING__STATUS:
				return getStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING__ID:
				setId((Integer)newValue);
				return;
			case BookingPackage.BOOKING__RESERVED_FROM:
				setReservedFrom((Date)newValue);
				return;
			case BookingPackage.BOOKING__RESERVED_TO:
				setReservedTo((Date)newValue);
				return;
			case BookingPackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				getReservations().addAll((Collection<? extends Integer>)newValue);
				return;
			case BookingPackage.BOOKING__PEOPLE:
				setPeople((People)newValue);
				return;
			case BookingPackage.BOOKING__STATUS:
				setStatus((BookingStatus)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__ID:
				setId(ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__RESERVED_FROM:
				setReservedFrom(RESERVED_FROM_EDEFAULT);
				return;
			case BookingPackage.BOOKING__RESERVED_TO:
				setReservedTo(RESERVED_TO_EDEFAULT);
				return;
			case BookingPackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				return;
			case BookingPackage.BOOKING__PEOPLE:
				setPeople((People)null);
				return;
			case BookingPackage.BOOKING__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__ID:
				return id != ID_EDEFAULT;
			case BookingPackage.BOOKING__RESERVED_FROM:
				return RESERVED_FROM_EDEFAULT == null ? reservedFrom != null : !RESERVED_FROM_EDEFAULT.equals(reservedFrom);
			case BookingPackage.BOOKING__RESERVED_TO:
				return RESERVED_TO_EDEFAULT == null ? reservedTo != null : !RESERVED_TO_EDEFAULT.equals(reservedTo);
			case BookingPackage.BOOKING__RESERVATIONS:
				return reservations != null && !reservations.isEmpty();
			case BookingPackage.BOOKING__PEOPLE:
				return people != null;
			case BookingPackage.BOOKING__STATUS:
				return status != STATUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING___ADD_RESERVATION__INT:
				return addReservation((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", reservedFrom: ");
		result.append(reservedFrom);
		result.append(", reservedTo: ");
		result.append(reservedTo);
		result.append(", reservations: ");
		result.append(reservations);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
