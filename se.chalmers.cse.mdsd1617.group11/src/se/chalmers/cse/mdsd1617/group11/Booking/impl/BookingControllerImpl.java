/**
 */
package se.chalmers.cse.mdsd1617.group11.Booking.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group11.Booking.Booking;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingController;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group11.Booking.People;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl#getAtomicId <em>Atomic Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingControllerImpl extends MinimalEObjectImpl.Container implements BookingController {
	/**
	 * The cached value of the '{@link #getBooking() <em>Booking</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooking()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> booking;

	/**
	 * The default value of the '{@link #getAtomicId() <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicId()
	 * @generated
	 * @ordered
	 */
	protected static final int ATOMIC_ID_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getAtomicId() <em>Atomic Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicId()
	 * @generated
	 * @ordered
	 */
	protected int atomicId = ATOMIC_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBooking() {
		if (booking == null) {
			booking = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackage.BOOKING_CONTROLLER__BOOKING);
		}
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAtomicId() {
		return atomicId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicId(int newAtomicId) {
		int oldAtomicId = atomicId;
		atomicId = newAtomicId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_CONTROLLER__ATOMIC_ID, oldAtomicId, atomicId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date getReservationToDate(int bookingID) {
		Booking book = this.getBooking(bookingID);
		
		return book.getReservedTo();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date getReservationFromDate(int bookingID) {
		Booking book = this.getBooking(bookingID);
		
		return book.getReservedFrom();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBookingByReservation(int reservationID) {
		List<Booking> list = (List<Booking>) this.booking.stream()
				.filter(booking -> booking.getReservations().contains(reservationID))
				.collect(Collectors.toList());

		if (list.isEmpty())
			return null;
		
		return list.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getReservations(int bookingID) {
		Booking book = null;
		for (Booking bookToFind : booking){
			if (bookToFind.getId() == bookingID){
				book = bookToFind;
				break;
			}
		}
		
		if (book != null){
			EList<Integer> toReturn = new BasicEList<Integer>();
			toReturn.addAll(book.getReservations());
			return toReturn;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(Date startDate, Date endDate, String firstName, String lastName) {
		People p = new PeopleImpl();
		p.setName(lastName);
		p.setFirstname(firstName);
		
		Booking book = BookingFactory.eINSTANCE.createBooking();
		book.setPeople(p);
		book.setReservedFrom(startDate);
		book.setReservedTo(endDate);
		this.setAtomicId(getAtomicId()+1);
		book.setId(this.getAtomicId());
		book.setStatus(BookingStatus.INITIATED);
		this.getBooking().add(book);
		
		return book.getId();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addReservationToBooking(int reservationID, int bookingID) {
		Booking book = this.getBooking(bookingID);
		if (book == null){
			return false;
		}
		return book.getReservations().add(reservationID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeBookingDate(int bookingID, Date from, Date toDate) {
		Booking book = this.getBooking(bookingID);
		
		book.setReservedFrom(from);
		book.setReservedTo(toDate);
		
		return true;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBooking(int bookingId) {
		List<Booking> list = (List<Booking>) this.booking.stream()
			.filter(booking -> booking.getId() == bookingId)
			.collect(Collectors.toList());
		
		if (list.isEmpty())
			return null;
	
		return list.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void restart() {
		this.booking = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackage.BOOKING_CONTROLLER__BOOKING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_CONTROLLER__BOOKING:
				return getBooking();
			case BookingPackage.BOOKING_CONTROLLER__ATOMIC_ID:
				return getAtomicId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_CONTROLLER__BOOKING:
				getBooking().clear();
				getBooking().addAll((Collection<? extends Booking>)newValue);
				return;
			case BookingPackage.BOOKING_CONTROLLER__ATOMIC_ID:
				setAtomicId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_CONTROLLER__BOOKING:
				getBooking().clear();
				return;
			case BookingPackage.BOOKING_CONTROLLER__ATOMIC_ID:
				setAtomicId(ATOMIC_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_CONTROLLER__BOOKING:
				return booking != null && !booking.isEmpty();
			case BookingPackage.BOOKING_CONTROLLER__ATOMIC_ID:
				return atomicId != ATOMIC_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_CONTROLLER___GET_RESERVATION_TO_DATE__INT:
				return getReservationToDate((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CONTROLLER___GET_RESERVATION_FROM_DATE__INT:
				return getReservationFromDate((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CONTROLLER___GET_BOOKING_BY_RESERVATION__INT:
				return getBookingByReservation((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CONTROLLER___GET_RESERVATIONS__INT:
				return getReservations((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CONTROLLER___INITIATE_BOOKING__DATE_DATE_STRING_STRING:
				return initiateBooking((Date)arguments.get(0), (Date)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKING_CONTROLLER___ADD_RESERVATION_TO_BOOKING__INT_INT:
				return addReservationToBooking((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_CONTROLLER___CHANGE_BOOKING_DATE__INT_DATE_DATE:
				return changeBookingDate((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case BookingPackage.BOOKING_CONTROLLER___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_CONTROLLER___RESTART:
				restart();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (atomicId: ");
		result.append(atomicId);
		result.append(')');
		return result.toString();
	}
	
} //BookingControllerImpl
