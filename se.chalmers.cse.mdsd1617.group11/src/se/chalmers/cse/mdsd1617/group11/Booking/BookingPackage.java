/**
 */
package se.chalmers.cse.mdsd1617.group11.Booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group11/Booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group11.Booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBookingController()
	 * @generated
	 */
	int BOOKING_CONTROLLER = 0;

	/**
	 * The feature id for the '<em><b>Booking</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER__BOOKING = 0;

	/**
	 * The feature id for the '<em><b>Atomic Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER__ATOMIC_ID = 1;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Reservation To Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___GET_RESERVATION_TO_DATE__INT = 0;

	/**
	 * The operation id for the '<em>Get Reservation From Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___GET_RESERVATION_FROM_DATE__INT = 1;

	/**
	 * The operation id for the '<em>Get Booking By Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___GET_BOOKING_BY_RESERVATION__INT = 2;

	/**
	 * The operation id for the '<em>Get Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___GET_RESERVATIONS__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___INITIATE_BOOKING__DATE_DATE_STRING_STRING = 4;

	/**
	 * The operation id for the '<em>Add Reservation To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___ADD_RESERVATION_TO_BOOKING__INT_INT = 5;

	/**
	 * The operation id for the '<em>Change Booking Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___CHANGE_BOOKING_DATE__INT_DATE_DATE = 6;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___GET_BOOKING__INT = 7;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER___RESTART = 8;

	/**
	 * The number of operations of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_CONTROLLER_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ID = 0;

	/**
	 * The feature id for the '<em><b>Reserved From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVED_FROM = 1;

	/**
	 * The feature id for the '<em><b>Reserved To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVED_TO = 2;

	/**
	 * The feature id for the '<em><b>Reservations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVATIONS = 3;

	/**
	 * The feature id for the '<em><b>People</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__PEOPLE = 4;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__STATUS = 5;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Add Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_RESERVATION__INT = 0;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 1;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.PeopleImpl <em>People</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.PeopleImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getPeople()
	 * @generated
	 */
	int PEOPLE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__FIRSTNAME = 1;

	/**
	 * The number of structural features of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBookingStatus()
	 * @generated
	 */
	int BOOKING_STATUS = 3;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController
	 * @generated
	 */
	EClass getBookingController();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBooking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBooking()
	 * @see #getBookingController()
	 * @generated
	 */
	EReference getBookingController_Booking();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getAtomicId <em>Atomic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Atomic Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getAtomicId()
	 * @see #getBookingController()
	 * @generated
	 */
	EAttribute getBookingController_AtomicId();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservationToDate(int) <em>Get Reservation To Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservation To Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservationToDate(int)
	 * @generated
	 */
	EOperation getBookingController__GetReservationToDate__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservationFromDate(int) <em>Get Reservation From Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservation From Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservationFromDate(int)
	 * @generated
	 */
	EOperation getBookingController__GetReservationFromDate__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBookingByReservation(int) <em>Get Booking By Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking By Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBookingByReservation(int)
	 * @generated
	 */
	EOperation getBookingController__GetBookingByReservation__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservations(int) <em>Get Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getReservations(int)
	 * @generated
	 */
	EOperation getBookingController__GetReservations__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#initiateBooking(java.util.Date, java.util.Date, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#initiateBooking(java.util.Date, java.util.Date, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBookingController__InitiateBooking__Date_Date_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#addReservationToBooking(int, int) <em>Add Reservation To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Reservation To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#addReservationToBooking(int, int)
	 * @generated
	 */
	EOperation getBookingController__AddReservationToBooking__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#changeBookingDate(int, java.util.Date, java.util.Date) <em>Change Booking Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Booking Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#changeBookingDate(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBookingController__ChangeBookingDate__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#getBooking(int)
	 * @generated
	 */
	EOperation getBookingController__GetBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingController#restart() <em>Restart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Restart</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingController#restart()
	 * @generated
	 */
	EOperation getBookingController__Restart();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedFrom <em>Reserved From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reserved From</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedFrom()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_ReservedFrom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedTo <em>Reserved To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reserved To</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedTo()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_ReservedTo();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservations <em>Reservations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Reservations</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservations()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Reservations();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getPeople <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>People</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getPeople()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_People();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#getStatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Status();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#addReservation(int) <em>Add Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.Booking#addReservation(int)
	 * @generated
	 */
	EOperation getBooking__AddReservation__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Booking.People <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>People</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.People
	 * @generated
	 */
	EClass getPeople();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.People#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.People#getName()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Booking.People#getFirstname <em>Firstname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Firstname</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.People#getFirstname()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Firstname();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus
	 * @generated
	 */
	EEnum getBookingStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingControllerImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBookingController()
		 * @generated
		 */
		EClass BOOKING_CONTROLLER = eINSTANCE.getBookingController();

		/**
		 * The meta object literal for the '<em><b>Booking</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_CONTROLLER__BOOKING = eINSTANCE.getBookingController_Booking();

		/**
		 * The meta object literal for the '<em><b>Atomic Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_CONTROLLER__ATOMIC_ID = eINSTANCE.getBookingController_AtomicId();

		/**
		 * The meta object literal for the '<em><b>Get Reservation To Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___GET_RESERVATION_TO_DATE__INT = eINSTANCE.getBookingController__GetReservationToDate__int();

		/**
		 * The meta object literal for the '<em><b>Get Reservation From Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___GET_RESERVATION_FROM_DATE__INT = eINSTANCE.getBookingController__GetReservationFromDate__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking By Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___GET_BOOKING_BY_RESERVATION__INT = eINSTANCE.getBookingController__GetBookingByReservation__int();

		/**
		 * The meta object literal for the '<em><b>Get Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___GET_RESERVATIONS__INT = eINSTANCE.getBookingController__GetReservations__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___INITIATE_BOOKING__DATE_DATE_STRING_STRING = eINSTANCE.getBookingController__InitiateBooking__Date_Date_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Reservation To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___ADD_RESERVATION_TO_BOOKING__INT_INT = eINSTANCE.getBookingController__AddReservationToBooking__int_int();

		/**
		 * The meta object literal for the '<em><b>Change Booking Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___CHANGE_BOOKING_DATE__INT_DATE_DATE = eINSTANCE.getBookingController__ChangeBookingDate__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___GET_BOOKING__INT = eINSTANCE.getBookingController__GetBooking__int();

		/**
		 * The meta object literal for the '<em><b>Restart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_CONTROLLER___RESTART = eINSTANCE.getBookingController__Restart();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__ID = eINSTANCE.getBooking_Id();

		/**
		 * The meta object literal for the '<em><b>Reserved From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__RESERVED_FROM = eINSTANCE.getBooking_ReservedFrom();

		/**
		 * The meta object literal for the '<em><b>Reserved To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__RESERVED_TO = eINSTANCE.getBooking_ReservedTo();

		/**
		 * The meta object literal for the '<em><b>Reservations</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__RESERVATIONS = eINSTANCE.getBooking_Reservations();

		/**
		 * The meta object literal for the '<em><b>People</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__PEOPLE = eINSTANCE.getBooking_People();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__STATUS = eINSTANCE.getBooking_Status();

		/**
		 * The meta object literal for the '<em><b>Add Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_RESERVATION__INT = eINSTANCE.getBooking__AddReservation__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.impl.PeopleImpl <em>People</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.PeopleImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getPeople()
		 * @generated
		 */
		EClass PEOPLE = eINSTANCE.getPeople();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__NAME = eINSTANCE.getPeople_Name();

		/**
		 * The meta object literal for the '<em><b>Firstname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__FIRSTNAME = eINSTANCE.getPeople_Firstname();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus
		 * @see se.chalmers.cse.mdsd1617.group11.Booking.impl.BookingPackageImpl#getBookingStatus()
		 * @generated
		 */
		EEnum BOOKING_STATUS = eINSTANCE.getBookingStatus();

	}

} //BookingPackage
