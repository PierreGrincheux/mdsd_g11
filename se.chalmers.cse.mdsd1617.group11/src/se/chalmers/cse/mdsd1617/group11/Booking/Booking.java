/**
 */
package se.chalmers.cse.mdsd1617.group11.Booking;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedFrom <em>Reserved From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedTo <em>Reserved To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getPeople <em>People</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getStatus <em>Status</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking()
 * @model
 * @generated
 */
public interface Booking extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_Id()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Reserved From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved From</em>' attribute.
	 * @see #setReservedFrom(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_ReservedFrom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getReservedFrom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedFrom <em>Reserved From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reserved From</em>' attribute.
	 * @see #getReservedFrom()
	 * @generated
	 */
	void setReservedFrom(Date value);

	/**
	 * Returns the value of the '<em><b>Reserved To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved To</em>' attribute.
	 * @see #setReservedTo(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_ReservedTo()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getReservedTo();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getReservedTo <em>Reserved To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reserved To</em>' attribute.
	 * @see #getReservedTo()
	 * @generated
	 */
	void setReservedTo(Date value);

	/**
	 * Returns the value of the '<em><b>Reservations</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservations</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservations</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_Reservations()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Integer> getReservations();

	/**
	 * Returns the value of the '<em><b>People</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>People</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>People</em>' reference.
	 * @see #setPeople(People)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_People()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	People getPeople();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getPeople <em>People</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>People</em>' reference.
	 * @see #getPeople()
	 * @generated
	 */
	void setPeople(People value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus
	 * @see #setStatus(BookingStatus)
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingPackage#getBooking_Status()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingStatus getStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Booking.Booking#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(BookingStatus value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" reservationIdRequired="true" reservationIdOrdered="false"
	 * @generated
	 */
	int addReservation(int reservationId);

} // Booking
