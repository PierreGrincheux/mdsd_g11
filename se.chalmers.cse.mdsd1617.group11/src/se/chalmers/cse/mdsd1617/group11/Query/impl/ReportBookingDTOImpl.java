/**
 */
package se.chalmers.cse.mdsd1617.group11.Query.impl;

import java.util.Date;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group11.Query.QueryPackage;
import se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Report Booking DTO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl#getFrom <em>From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl#getTo <em>To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl#getRoommapentry <em>Roommapentry</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReportBookingDTOImpl extends MinimalEObjectImpl.Container implements ReportBookingDTO {
	/**
	 * The default value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected int bookingID = BOOKING_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrom() <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected static final Date FROM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected Date from = FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getTo() <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected static final Date TO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected Date to = TO_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoommapentry() <em>Roommapentry</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoommapentry()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Integer> roommapentry;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReportBookingDTOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QueryPackage.Literals.REPORT_BOOKING_DTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingID() {
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingID(int newBookingID) {
		int oldBookingID = bookingID;
		bookingID = newBookingID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.REPORT_BOOKING_DTO__BOOKING_ID, oldBookingID, bookingID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(Date newFrom) {
		Date oldFrom = from;
		from = newFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.REPORT_BOOKING_DTO__FROM, oldFrom, from));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(Date newTo) {
		Date oldTo = to;
		to = newTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.REPORT_BOOKING_DTO__TO, oldTo, to));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, Integer> getRoommapentry() {
		if (roommapentry == null) {
			roommapentry = new EcoreEMap<String,Integer>(QueryPackage.Literals.BOOKING_REPORT_MAP_ENTRY, bookingReportMapEntryImpl.class, this, QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY);
		}
		return roommapentry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY:
				return ((InternalEList<?>)getRoommapentry()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QueryPackage.REPORT_BOOKING_DTO__BOOKING_ID:
				return getBookingID();
			case QueryPackage.REPORT_BOOKING_DTO__FROM:
				return getFrom();
			case QueryPackage.REPORT_BOOKING_DTO__TO:
				return getTo();
			case QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY:
				if (coreType) return getRoommapentry();
				else return getRoommapentry().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QueryPackage.REPORT_BOOKING_DTO__BOOKING_ID:
				setBookingID((Integer)newValue);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__FROM:
				setFrom((Date)newValue);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__TO:
				setTo((Date)newValue);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY:
				((EStructuralFeature.Setting)getRoommapentry()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QueryPackage.REPORT_BOOKING_DTO__BOOKING_ID:
				setBookingID(BOOKING_ID_EDEFAULT);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__FROM:
				setFrom(FROM_EDEFAULT);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__TO:
				setTo(TO_EDEFAULT);
				return;
			case QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY:
				getRoommapentry().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QueryPackage.REPORT_BOOKING_DTO__BOOKING_ID:
				return bookingID != BOOKING_ID_EDEFAULT;
			case QueryPackage.REPORT_BOOKING_DTO__FROM:
				return FROM_EDEFAULT == null ? from != null : !FROM_EDEFAULT.equals(from);
			case QueryPackage.REPORT_BOOKING_DTO__TO:
				return TO_EDEFAULT == null ? to != null : !TO_EDEFAULT.equals(to);
			case QueryPackage.REPORT_BOOKING_DTO__ROOMMAPENTRY:
				return roommapentry != null && !roommapentry.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bookingID: ");
		result.append(bookingID);
		result.append(", from: ");
		result.append(from);
		result.append(", to: ");
		result.append(to);
		result.append(')');
		return result.toString();
	}

} //ReportBookingDTOImpl
