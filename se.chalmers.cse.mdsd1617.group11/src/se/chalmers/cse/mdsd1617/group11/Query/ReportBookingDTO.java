/**
 */
package se.chalmers.cse.mdsd1617.group11.Query;

import java.util.Date;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Report Booking DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getFrom <em>From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getTo <em>To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getRoommapentry <em>Roommapentry</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportBookingDTO()
 * @model
 * @generated
 */
public interface ReportBookingDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking ID</em>' attribute.
	 * @see #setBookingID(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportBookingDTO_BookingID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getBookingID <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking ID</em>' attribute.
	 * @see #getBookingID()
	 * @generated
	 */
	void setBookingID(int value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' attribute.
	 * @see #setFrom(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportBookingDTO_From()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getFrom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getFrom <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' attribute.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Date value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' attribute.
	 * @see #setTo(Date)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportBookingDTO_To()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getTo();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getTo <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' attribute.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Date value);

	/**
	 * Returns the value of the '<em><b>Roommapentry</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roommapentry</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roommapentry</em>' map.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportBookingDTO_Roommapentry()
	 * @model mapType="se.chalmers.cse.mdsd1617.group11.Query.bookingReportMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EIntegerObject>" ordered="false"
	 * @generated
	 */
	EMap<String, Integer> getRoommapentry();

} // ReportBookingDTO
