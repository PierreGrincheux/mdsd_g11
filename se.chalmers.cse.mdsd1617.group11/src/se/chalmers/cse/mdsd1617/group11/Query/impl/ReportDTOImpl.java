/**
 */
package se.chalmers.cse.mdsd1617.group11.Query.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import se.chalmers.cse.mdsd1617.group11.Query.QueryPackage;
import se.chalmers.cse.mdsd1617.group11.Query.ReportDTO;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Report DTO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl#getBookings <em>Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReportDTOImpl extends MinimalEObjectImpl.Container implements ReportDTO {
	/**
	 * The default value of the '{@link #getBookings() <em>Bookings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKINGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected int bookings = BOOKINGS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> rooms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReportDTOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QueryPackage.Literals.REPORT_DTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookings() {
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookings(int newBookings) {
		int oldBookings = bookings;
		bookings = newBookings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.REPORT_DTO__BOOKINGS, oldBookings, bookings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getRooms() {
		if (rooms == null) {
			rooms = new EDataTypeUniqueEList<Integer>(Integer.class, this, QueryPackage.REPORT_DTO__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QueryPackage.REPORT_DTO__BOOKINGS:
				return getBookings();
			case QueryPackage.REPORT_DTO__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QueryPackage.REPORT_DTO__BOOKINGS:
				setBookings((Integer)newValue);
				return;
			case QueryPackage.REPORT_DTO__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QueryPackage.REPORT_DTO__BOOKINGS:
				setBookings(BOOKINGS_EDEFAULT);
				return;
			case QueryPackage.REPORT_DTO__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QueryPackage.REPORT_DTO__BOOKINGS:
				return bookings != BOOKINGS_EDEFAULT;
			case QueryPackage.REPORT_DTO__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Bookings: ");
		result.append(bookings);
		result.append(", Rooms: ");
		result.append(rooms);
		result.append(')');
		return result.toString();
	}

} //ReportDTOImpl
