/**
 */
package se.chalmers.cse.mdsd1617.group11.Query;

import org.eclipse.emf.common.util.EList;
import se.chalmers.cse.mdsd1617.group11.BankingModel.ICustomerProvides;

import se.chalmers.cse.mdsd1617.group11.Booking.BookingController;

import se.chalmers.cse.mdsd1617.group11.Reservation.Reservation;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController;

import se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getReservationcontroller <em>Reservationcontroller</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingcontroller <em>Bookingcontroller</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIqueryroom <em>Iqueryroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIcustomerprovides <em>Icustomerprovides</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getQueryController()
 * @model
 * @generated
 */
public interface QueryController extends IReceptionist, IStartupQueryController, IHotelCustomerProvides {
	/**
	 * Returns the value of the '<em><b>Reservationcontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservationcontroller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservationcontroller</em>' reference.
	 * @see #setReservationcontroller(ReservationController)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getQueryController_Reservationcontroller()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ReservationController getReservationcontroller();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getReservationcontroller <em>Reservationcontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reservationcontroller</em>' reference.
	 * @see #getReservationcontroller()
	 * @generated
	 */
	void setReservationcontroller(ReservationController value);

	/**
	 * Returns the value of the '<em><b>Bookingcontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookingcontroller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookingcontroller</em>' reference.
	 * @see #setBookingcontroller(BookingController)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getQueryController_Bookingcontroller()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingController getBookingcontroller();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingcontroller <em>Bookingcontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookingcontroller</em>' reference.
	 * @see #getBookingcontroller()
	 * @generated
	 */
	void setBookingcontroller(BookingController value);

	/**
	 * Returns the value of the '<em><b>Iqueryroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iqueryroom</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iqueryroom</em>' reference.
	 * @see #setIqueryroom(IQueryRoom)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getQueryController_Iqueryroom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IQueryRoom getIqueryroom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIqueryroom <em>Iqueryroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iqueryroom</em>' reference.
	 * @see #getIqueryroom()
	 * @generated
	 */
	void setIqueryroom(IQueryRoom value);

	/**
	 * Returns the value of the '<em><b>Icustomerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icustomerprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icustomerprovides</em>' reference.
	 * @see #setIcustomerprovides(ICustomerProvides)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getQueryController_Icustomerprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ICustomerProvides getIcustomerprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIcustomerprovides <em>Icustomerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icustomerprovides</em>' reference.
	 * @see #getIcustomerprovides()
	 * @generated
	 */
	void setIcustomerprovides(ICustomerProvides value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	EList<Reservation> getBookingReservations(int bookingId);

} // QueryController
