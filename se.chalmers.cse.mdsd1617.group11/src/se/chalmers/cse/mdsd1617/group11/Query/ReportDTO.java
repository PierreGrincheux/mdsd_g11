/**
 */
package se.chalmers.cse.mdsd1617.group11.Query;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Report DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getBookings <em>Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportDTO()
 * @model
 * @generated
 */
public interface ReportDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Bookings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookings</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookings</em>' attribute.
	 * @see #setBookings(int)
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportDTO_Bookings()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookings();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getBookings <em>Bookings</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookings</em>' attribute.
	 * @see #getBookings()
	 * @generated
	 */
	void setBookings(int value);

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getReportDTO_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Integer> getRooms();

} // ReportDTO
