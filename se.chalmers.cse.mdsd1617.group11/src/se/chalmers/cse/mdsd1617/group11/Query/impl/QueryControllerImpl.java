/**
 */
package se.chalmers.cse.mdsd1617.group11.Query.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import se.chalmers.cse.mdsd1617.group11.BankingModel.BankingModelFactory;
import se.chalmers.cse.mdsd1617.group11.BankingModel.ICustomerProvides;

import se.chalmers.cse.mdsd1617.group11.Booking.Booking;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingController;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group11.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group11.FreeRoomTypesDTO;

import se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController;
import se.chalmers.cse.mdsd1617.group11.Query.QueryController;
import se.chalmers.cse.mdsd1617.group11.Query.QueryFactory;
import se.chalmers.cse.mdsd1617.group11.Query.QueryPackage;

import se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO;
import se.chalmers.cse.mdsd1617.group11.Query.ReportDTO;
import se.chalmers.cse.mdsd1617.group11.Reservation.Reservation;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationController;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationFactory;
import se.chalmers.cse.mdsd1617.group11.Reservation.ReservationStatus;
import se.chalmers.cse.mdsd1617.group11.Room.IQueryRoom;
import se.chalmers.cse.mdsd1617.group11.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl#getReservationcontroller <em>Reservationcontroller</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl#getBookingcontroller <em>Bookingcontroller</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl#getIqueryroom <em>Iqueryroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl#getIcustomerprovides <em>Icustomerprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QueryControllerImpl extends MinimalEObjectImpl.Container implements QueryController {
	//Variables for pay booking
	private static double checkoutPrice = 0;
	private static boolean paied = true;
	
	//Variables for pay room
	private static Map<Integer, Double> roomsToPay = new TreeMap<>();
	/**
	 * The cached value of the '{@link #getReservationcontroller() <em>Reservationcontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservationcontroller()
	 * @generated
	 * @ordered
	 */
	protected ReservationController reservationcontroller;

	/**
	 * The cached value of the '{@link #getBookingcontroller() <em>Bookingcontroller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingcontroller()
	 * @generated
	 * @ordered
	 */
	protected BookingController bookingcontroller;

	/**
	 * The cached value of the '{@link #getIqueryroom() <em>Iqueryroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIqueryroom()
	 * @generated
	 * @ordered
	 */
	protected IQueryRoom iqueryroom;

	/**
	 * The cached value of the '{@link #getIcustomerprovides() <em>Icustomerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIcustomerprovides()
	 * @generated
	 * @ordered
	 */
	protected ICustomerProvides icustomerprovides;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueryControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QueryPackage.Literals.QUERY_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationController getReservationcontroller() {
		if (reservationcontroller != null && reservationcontroller.eIsProxy()) {
			InternalEObject oldReservationcontroller = (InternalEObject)reservationcontroller;
			reservationcontroller = (ReservationController)eResolveProxy(oldReservationcontroller);
			if (reservationcontroller != oldReservationcontroller) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER, oldReservationcontroller, reservationcontroller));
			}
		}
		return reservationcontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationController basicGetReservationcontroller() {
		return reservationcontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservationcontroller(ReservationController newReservationcontroller) {
		ReservationController oldReservationcontroller = reservationcontroller;
		reservationcontroller = newReservationcontroller;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER, oldReservationcontroller, reservationcontroller));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingController getBookingcontroller() {
		if (bookingcontroller != null && bookingcontroller.eIsProxy()) {
			InternalEObject oldBookingcontroller = (InternalEObject)bookingcontroller;
			bookingcontroller = (BookingController)eResolveProxy(oldBookingcontroller);
			if (bookingcontroller != oldBookingcontroller) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER, oldBookingcontroller, bookingcontroller));
			}
		}
		return bookingcontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingController basicGetBookingcontroller() {
		return bookingcontroller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingcontroller(BookingController newBookingcontroller) {
		BookingController oldBookingcontroller = bookingcontroller;
		bookingcontroller = newBookingcontroller;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER, oldBookingcontroller, bookingcontroller));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IQueryRoom getIqueryroom() {
		if (iqueryroom != null && iqueryroom.eIsProxy()) {
			InternalEObject oldIqueryroom = (InternalEObject)iqueryroom;
			iqueryroom = (IQueryRoom)eResolveProxy(oldIqueryroom);
			if (iqueryroom != oldIqueryroom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QueryPackage.QUERY_CONTROLLER__IQUERYROOM, oldIqueryroom, iqueryroom));
			}
		}
		return iqueryroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IQueryRoom basicGetIqueryroom() {
		return iqueryroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIqueryroom(IQueryRoom newIqueryroom) {
		IQueryRoom oldIqueryroom = iqueryroom;
		iqueryroom = newIqueryroom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.QUERY_CONTROLLER__IQUERYROOM, oldIqueryroom, iqueryroom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ICustomerProvides getIcustomerprovides() {
		if (icustomerprovides != null && icustomerprovides.eIsProxy()) {
			InternalEObject oldIcustomerprovides = (InternalEObject)icustomerprovides;
			icustomerprovides = (ICustomerProvides)eResolveProxy(oldIcustomerprovides);
			if (icustomerprovides != oldIcustomerprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES, oldIcustomerprovides, icustomerprovides));
			}
		}
		return icustomerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ICustomerProvides basicGetIcustomerprovides() {
		return icustomerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIcustomerprovides(ICustomerProvides newIcustomerprovides) {
		ICustomerProvides oldIcustomerprovides = icustomerprovides;
		icustomerprovides = newIcustomerprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES, oldIcustomerprovides, icustomerprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> getConfirmedBookings() {
		EList<Booking> list = new BasicEList<Booking>();
		bookingcontroller.getBooking()
				.stream()
				.filter(booking -> booking.getStatus() == BookingStatus.CONFIRMED)
				.forEach(booking -> list.add(booking));

		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> getRoomTypes(int bookingID) {
		EList<String> list = new BasicEList<String>();

		this.getBookingReservations(bookingID)
			.stream()
			.map(reservation -> reservation.getReservedRoomType())
			.forEach(reservation -> list.add(reservation));

		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date getCheckedInDate(int bookingID) {
		return this.bookingcontroller.getBooking(bookingID).getReservedFrom();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Date getCheckedOutDate(int bookingID) {
		return this.bookingcontroller.getBooking(bookingID).getReservedTo();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ReportDTO> getOccupiedRooms(Date date) {
		//Get all reservations that are checked in the given date.
		
		EList<Booking> bookings = this.bookingcontroller.getBooking();
		EList<Reservation> list = new BasicEList<Reservation>();
		
		for (Booking booking : bookings) {
			if (booking.getReservedFrom().before(date) && booking.getReservedTo().after(date)) {
				list.addAll(this.getBookingReservations(booking.getId()));
			}
		}

		return createReport(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ReportDTO> getCheckIns(Date checkInDate) {
		Date toCheck = cleanDateToDay(checkInDate);
		Date today = new Date();
		
		EList<Reservation> list = new BasicEList<>();
		
		// Check if date is in future then return empty list
		if (toCheck.after(today)) {
			return createReport(list);
		}
		
		EList<Reservation> reservations = this.reservationcontroller.getReservation();
		
		
		for (Reservation reservation : reservations) {
			if (reservation.getCheckInDate() != null) {
				if (reservation.getStatus() != ReservationStatus.CANCEL && reservation.getCheckInDate().equals(toCheck)) {
					list.add(reservation);
				}
			}
		}

		return createReport(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ReportDTO> getCheckIns(Date checkInDateStart, Date checkInDateEnd) {
		EList<Reservation> list = new BasicEList<Reservation>();
		this.reservationcontroller.getReservation()
			.stream()
			.filter(reservation -> reservation.getStatus() != ReservationStatus.CANCEL)
			.filter(reservation -> reservation.getCheckInDate().after(checkInDateStart) && reservation.getCheckInDate().before(checkInDateEnd))
			.forEach(reservation -> list.add(reservation));

		return createReport(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ReportDTO> getCheckedOutBookings(Date from, Date to) {
		Date fromCheck = cleanDateToDay(from);
		Date toCheck = cleanDateToDay(to);
		Date today = new Date();
		
		EList<Reservation> list = new BasicEList<Reservation>();
		// Check if date is in future then return empty list
		if (toCheck.after(today)) {
			to= today;
		}
		if (fromCheck.after(today)) {
			from=today;
		}
		
		EList<Reservation> reservations = this.reservationcontroller.getReservation();
		
		for (Reservation reservation : reservations) {
			if (reservation.getCheckOutDate() != null) {
				if (reservation.getStatus() != ReservationStatus.CANCEL && ( reservation.getCheckOutDate().equals(from)|| 
						reservation.getCheckOutDate().equals(to) ||(reservation.getCheckOutDate().after(from) && 
						(reservation.getCheckOutDate().before(to) )))) {
					list.add(reservation);
				}
			}
		}
		return createReport(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> checkInBooking(int bookingId) {
		Booking booking = bookingcontroller.getBooking(bookingId);
		if (booking != null && (booking.getStatus() == BookingStatus.CHECKEDIN || booking.getStatus() == BookingStatus.CONFIRMED)){
			EList<Integer> list = new BasicEList<Integer>();
			for (int reservationId : booking.getReservations()){
				Reservation reserv = reservationcontroller.getReservation(reservationId);
				if (reserv.getStatus().equals(ReservationStatus.OPEN)) {
					int roomNo = checkInRoom(reservationcontroller.getRoomType(reservationId),bookingId);
					if (roomNo != -1){
						list.add(roomNo);
					}
				}
			}
			if (list.size() > 0){
				booking.setStatus(BookingStatus.CHECKEDIN);
				return list;
			} else return null;

		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeReservationDate(int bookingID, Date newCheckInDate, Date newCheckOutDate) {
		if(newCheckInDate.after(newCheckOutDate)) return false;
		
		Booking booking = this.getBookingcontroller().getBooking(bookingID);
		
		if(booking.getStatus() != BookingStatus.INITIATED) return false;
		
		booking.setReservedFrom(newCheckInDate);
		booking.setReservedTo(newCheckOutDate);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomType(int bookingID, String oldRoomType, String newRoomType, int nbrRooms) {
		Booking booking = bookingcontroller.getBooking(bookingID);
		
		if(booking.getStatus() != BookingStatus.INITIATED) return false;
		
		List<Integer> possibleUpdates = new ArrayList<>();
		
		FreeRoomTypesDTO rooms = iqueryroom.getAvailableRooms(newRoomType);
		int freeRooms = 0;
		if (rooms != null){
			freeRooms = rooms.getNumFreeRooms();
			freeRooms = freeRooms - getNoOfReservedRooms(booking.getReservedFrom(), booking.getReservedTo(), newRoomType);
		} else return false; // If there aren't any rooms with that room type
		if (freeRooms < nbrRooms){ // Checks whether there are nbrRooms available rooms with that room type for the specified date in the booking.
			return false;
		}
		
		if (booking != null){
			for (int resID : booking.getReservations()){
				if (reservationcontroller.getRoomType(resID).equals(oldRoomType)){
					possibleUpdates.add(resID);
				}
				
			}
			if (possibleUpdates.size() >= nbrRooms){
				for (int i = 0; i<nbrRooms;i++){
					reservationcontroller.changeRoomType(possibleUpdates.get(i), newRoomType);
				}
				return true;
			}
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomType(int bookingID, String oldRoomType, String newRoomType) {
		EList<Reservation> reservations = this.getBookingReservations(bookingID);


		Stream<Reservation> reservationsToUpdate = reservations.stream()
			.filter(reservation -> reservation.getStatus() != ReservationStatus.CANCEL)
			.filter(reservation -> reservation.getReservedRoomType().equals(oldRoomType));

		reservationsToUpdate.forEach(reservation -> {
			reservation.setReservedRoomType(newRoomType);
		});


		return reservationsToUpdate.count() > 0;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeNumberOfRooms(int bookingID, RoomType roomType, int nbrOfRooms) {
		if(roomType == null) return false;
		
		Booking book = this.bookingcontroller.getBooking(bookingID);
		
		if(book.getStatus() != BookingStatus.INITIATED) return false;
		
		EList<Reservation> reservations = new BasicEList<Reservation>();

		this.getBookingReservations(bookingID).stream()
			.filter(reservation -> reservation.getStatus() != ReservationStatus.CANCEL)
			.filter(reservation -> reservation.getReservedRoomType().equals(roomType.getName()))
			.forEach(reservation -> reservations.add(reservation));

		int count = reservations.size();
		
		if (count == nbrOfRooms) {
			return false;

		} else if (count < nbrOfRooms) {
			int quantity = nbrOfRooms - count;
			
			for (int i = 0; i < quantity; i++) {
				int reservationID = this.reservationcontroller.initiateReservation(roomType.getName());
				book.addReservation(reservationID);
			}

		} else {
			if (nbrOfRooms < 0) return false;
			int quantity = count - nbrOfRooms;

			for (int i = 0; i < quantity; i++) {
				this.reservationcontroller.cancelReservation(reservations.get(i).getId());
			}

		}

		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		Booking book = this.bookingcontroller.getBooking(bookingID);

		book.getReservations().forEach(reservationId -> this.reservationcontroller.cancelReservation(reservationId));

		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNbrOfRooms(int bookingID, String roomType) {
		EList<Reservation> reservations = this.getBookingReservations(bookingID);

		long count = reservations.stream()
			.filter(reservation -> reservation.getStatus() != ReservationStatus.CANCEL)
			.filter(reservation -> reservation.getReservedRoomType().equals(roomType))
			.count();

		return Math.toIntExact(count);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBookingWithIDAndRoomNbr(int id, int roomNbr) {
		if (iqueryroom.occupyRoom(iqueryroom.getRoom(roomNbr)) != -1){
			EList<Integer> reservations = bookingcontroller.getReservations(id);
			String roomTypeOfRoom = iqueryroom.getRoomType(roomNbr);
			for (int res : reservations){
				if (reservationcontroller.getRoomType(res).equals(roomTypeOfRoom) && reservationcontroller.checkInReservation(res, roomNbr, bookingcontroller.getReservationFromDate(id))){
					bookingcontroller.getBooking(id).setStatus(BookingStatus.CHECKEDIN);
					return true;
				}
				
			}
			//If no reservation was checked in, release the room again.
			iqueryroom.freeRoom(roomNbr);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getFreeRoomsForBookingID(int id) {
		EList<Integer> reservations = bookingcontroller.getReservations(id);
		Set<String> roomTypes = new HashSet<String>();
		for (int res : reservations){
			roomTypes.add(reservationcontroller.getRoomType(res));
		}
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		for (String type : roomTypes){
			roomNumbers.addAll(iqueryroom.getAvailableRoomNo(type));
		}
		return roomNumbers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtra(int bookingID, int room, String extraDescription, double price) {
		Booking book = bookingcontroller.getBooking(bookingID);
		if (book != null){
			EList<Integer> reservations = book.getReservations();
			for (int resID: reservations){
				Reservation res = reservationcontroller.getReservation(resID);
				if (res != null && res.getRoomNbr() == room){
					reservationcontroller.addExtra(resID, price, extraDescription);
					return true;
				}
			}

		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ReportBookingDTO> listBookings() {
		EList<Booking> allBookings= bookingcontroller.getBooking();
		EList<ReportBookingDTO> toReturn = new BasicEList<>();
		
		for (Booking book : allBookings){
			if (book.getStatus() == BookingStatus.CONFIRMED){
				ReportBookingDTO temp = QueryFactory.eINSTANCE.createReportBookingDTO();
				temp.setBookingID(book.getId());
				temp.setFrom(book.getReservedFrom());
				temp.setTo(book.getReservedTo());
				for (int res : book.getReservations()){
					String type = reservationcontroller.getRoomType(res);
					if (temp.getRoommapentry().keySet().contains(type)){
						temp.getRoommapentry().put(type, temp.getRoommapentry().get(type)+1);
					} else {
						temp.getRoommapentry().put(type, 1);
					}
				}
				toReturn.add(temp);
			}
		}
		return toReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup() {
		reservationcontroller = ReservationFactory.eINSTANCE.createReservationController();
		bookingcontroller = BookingFactory.eINSTANCE.createBookingController();
		icustomerprovides = BankingModelFactory.eINSTANCE.createBankComponent();
		iqueryroom = RoomFactory.eINSTANCE.createRoomController();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void restart() {
		reservationcontroller.restart();
		bookingcontroller.restart();
		paied = true;
		checkoutPrice = 0;
		roomsToPay = new TreeMap<>();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		Date start = createDate(startDate);
		Date end = createDate(endDate);
		if (start.before(end)){
			EList<FreeRoomTypesDTO> rooms = iqueryroom.getRooms(numBeds);
			for (FreeRoomTypesDTO data : rooms){
				data.setNumFreeRooms(data.getNumFreeRooms()-getNoOfReservedRooms(start, end, data.getRoomTypeDescription()));
			}
			return rooms;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @throws ParseException
	 * @generated NOT
	 */

	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {		
		Date start, end;
		
		start = createDate(startDate);
		end = createDate(endDate);
		
		if(end.after(start) && !(firstName == "" || lastName == "" || lastName == null || firstName == null )) {
			
			int bookingID = this.getBookingcontroller().initiateBooking(start, end, firstName, lastName);
			return bookingID;
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking book = this.bookingcontroller.getBooking(bookingID);
		if (book != null && book.getStatus() == BookingStatus.INITIATED) {
			FreeRoomTypesDTO rooms = iqueryroom.getAvailableRooms(roomTypeDescription);
			int freeRooms = 0;
			if (rooms != null){

				freeRooms = rooms.getNumFreeRooms();
				freeRooms = freeRooms - getNoOfReservedRooms(book.getReservedFrom(), book.getReservedTo(), roomTypeDescription);
			}
			if (freeRooms <= 0){
				return false;
			}
			int reservationId = this.reservationcontroller.initiateReservation(roomTypeDescription);
			bookingcontroller.addReservationToBooking(reservationId, bookingID);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		Booking tempB = this.bookingcontroller.getBooking(bookingID);
		if (tempB == null){
			return false;
		}
		EList<Integer> tempLR = this.bookingcontroller.getBooking(bookingID).getReservations();
		if(tempLR != null && tempLR.size() > 0){
			tempB.setStatus(BookingStatus.CONFIRMED);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		Booking booking = bookingcontroller.getBooking(bookingID);
		if (booking != null){
			BookingStatus status = booking.getStatus();
			
			if (paied && status != BookingStatus.CHECKEDOUT){
				EList<Integer> tempLR = booking.getReservations();
				double price = 0;
				for(int reservation : tempLR) {
					Reservation res = reservationcontroller.getReservation(reservation);
					//Check if reservation is already checked out.
					if (res != null && res.getCheckOutDate() == null){
						double extraPrice = reservationcontroller.checkOutReservation(reservation, booking.getReservedTo());
						long stayLength = ((res.getCheckOutDate().getTime() - res.getCheckInDate().getTime()) / (1000 * 60 * 60 * 24));
						String roomType = res.getReservedRoomType();
						double roomPrice = iqueryroom.getPrice(roomType) * stayLength;
						price += roomPrice;
						price += extraPrice;
					}

					
				}
				checkoutPrice = price;
				paied = false;
				this.bookingcontroller.getBooking(bookingID).setStatus(BookingStatus.CHECKEDOUT);
				return price;
			}
		}
		

		return -1;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		if (!paied){
			if (icustomerprovides.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, checkoutPrice)){
				paied = true;
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		Booking booking = bookingcontroller.getBooking(bookingId);
		if (booking != null) {
			if (booking.getStatus() == BookingStatus.CHECKEDIN || booking.getStatus() == BookingStatus.CHECKEDOUT){
				EList<Integer> reservations = booking.getReservations();
				for (int res : reservations){
					Reservation reservation = reservationcontroller.getReservation(res);
					if (reservation != null && reservation.getRoomNbr() == roomNumber && reservation.getCheckOutDate() == null){
						//Try to free the room, if so calculate price and return
						if (iqueryroom.freeRoom(reservation.getRoomNbr())){
							double price = reservationcontroller.checkOutReservation(res, booking.getReservedTo());
							long stayLength = ((reservation.getCheckOutDate().getTime() - reservation.getCheckInDate().getTime()) / (1000 * 60 * 60 * 24));
							double roomCost = iqueryroom.getPrice(reservation.getReservedRoomType());
							price += (roomCost * stayLength);
							
							//Put the payment in the list of payments to do
							roomsToPay.put(reservation.getRoomNbr(), price);
							
							boolean allCheckedOut = true;
							for (int resCheck : booking.getReservations()){
								if (reservationcontroller.getReservation(resCheck).getCheckOutDate() == null){
									allCheckedOut =false;
									break;
								}
							}
							if (allCheckedOut) booking.setStatus(BookingStatus.CHECKEDOUT);
							return price;
						} else return -1;
					}
				}
			}
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String _ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		if (roomsToPay.containsKey(roomNumber) && icustomerprovides.makePayment(ccNumber, _ccv, expiryMonth, expiryYear, firstName, lastName, roomsToPay.get(roomNumber))){
			roomsToPay.remove(roomNumber);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Finds the first reservation in the booking with the given room type and checks it in. Returns the room no if success else -1.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		Booking booking = bookingcontroller.getBooking(bookingId);
		//Check if booking exists, room type is not null and is of the correct status.
		if (booking != null && roomTypeDescription != null && (booking.getStatus() == BookingStatus.CONFIRMED || booking.getStatus() == BookingStatus.CHECKEDIN)){
			for (int res : booking.getReservations()){
				//Check if the reservation is of the correct room type, else check next.
				if (reservationcontroller.getRoomType(res).equals(roomTypeDescription) && reservationcontroller.getReservation(res).getCheckInDate() == null){
					Reservation reservation = reservationcontroller.getReservation(res);
					int roomNo = -1;
					if (reservation != null){
						roomNo = iqueryroom.getAvailableRoom(roomTypeDescription);
						roomNo = iqueryroom.occupyRoom(iqueryroom.getRoom(roomNo));
					} else break;
					//Check if valid room, if so do check in, else break as there are no such rooms available or reservation do not exist.
					if (roomNo != -1 && reservationcontroller.checkInReservation(res, roomNo, booking.getReservedFrom())){
						booking.setStatus(BookingStatus.CHECKEDIN);
						return roomNo;
					} else {
						//Free room if room failed, will do nothing if the room was not occupied.
						iqueryroom.freeRoom(roomNo);
						break;
					}
				}
			}
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Reservation> getBookingReservations(int bookingId) {
		Booking book = this.bookingcontroller.getBooking(bookingId);

		List<Integer> resNumbers = book.getReservations();
		List<Reservation> reservations = this.reservationcontroller.getReservation();

		EList<Reservation> list = new BasicEList<Reservation>();
		reservations.stream()
			.filter(reservation -> reservation.getStatus() != ReservationStatus.CANCEL)
			.filter(reservation -> resNumbers.contains(reservation.getId()))
			.forEach(reservation -> list.add(reservation));

		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER:
				if (resolve) return getReservationcontroller();
				return basicGetReservationcontroller();
			case QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER:
				if (resolve) return getBookingcontroller();
				return basicGetBookingcontroller();
			case QueryPackage.QUERY_CONTROLLER__IQUERYROOM:
				if (resolve) return getIqueryroom();
				return basicGetIqueryroom();
			case QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES:
				if (resolve) return getIcustomerprovides();
				return basicGetIcustomerprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER:
				setReservationcontroller((ReservationController)newValue);
				return;
			case QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER:
				setBookingcontroller((BookingController)newValue);
				return;
			case QueryPackage.QUERY_CONTROLLER__IQUERYROOM:
				setIqueryroom((IQueryRoom)newValue);
				return;
			case QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES:
				setIcustomerprovides((ICustomerProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER:
				setReservationcontroller((ReservationController)null);
				return;
			case QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER:
				setBookingcontroller((BookingController)null);
				return;
			case QueryPackage.QUERY_CONTROLLER__IQUERYROOM:
				setIqueryroom((IQueryRoom)null);
				return;
			case QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES:
				setIcustomerprovides((ICustomerProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QueryPackage.QUERY_CONTROLLER__RESERVATIONCONTROLLER:
				return reservationcontroller != null;
			case QueryPackage.QUERY_CONTROLLER__BOOKINGCONTROLLER:
				return bookingcontroller != null;
			case QueryPackage.QUERY_CONTROLLER__IQUERYROOM:
				return iqueryroom != null;
			case QueryPackage.QUERY_CONTROLLER__ICUSTOMERPROVIDES:
				return icustomerprovides != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IStartupQueryController.class) {
			switch (baseOperationID) {
				case QueryPackage.ISTARTUP_QUERY_CONTROLLER___STARTUP: return QueryPackage.QUERY_CONTROLLER___STARTUP;
				case QueryPackage.ISTARTUP_QUERY_CONTROLLER___RESTART: return QueryPackage.QUERY_CONTROLLER___RESTART;
				default: return -1;
			}
		}
		if (baseClass == IHotelCustomerProvides.class) {
			switch (baseOperationID) {
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING: return QueryPackage.QUERY_CONTROLLER___GET_FREE_ROOMS__INT_STRING_STRING;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING: return QueryPackage.QUERY_CONTROLLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT: return QueryPackage.QUERY_CONTROLLER___ADD_ROOM_TO_BOOKING__STRING_INT;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT: return QueryPackage.QUERY_CONTROLLER___CONFIRM_BOOKING__INT;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT: return QueryPackage.QUERY_CONTROLLER___INITIATE_CHECKOUT__INT;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING: return QueryPackage.QUERY_CONTROLLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT: return QueryPackage.QUERY_CONTROLLER___INITIATE_ROOM_CHECKOUT__INT_INT;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING: return QueryPackage.QUERY_CONTROLLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;
				case QueryPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT: return QueryPackage.QUERY_CONTROLLER___CHECK_IN_ROOM__STRING_INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case QueryPackage.QUERY_CONTROLLER___GET_CONFIRMED_BOOKINGS:
				return getConfirmedBookings();
			case QueryPackage.QUERY_CONTROLLER___GET_ROOM_TYPES__INT:
				return getRoomTypes((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_CHECKED_IN_DATE__INT:
				return getCheckedInDate((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_CHECKED_OUT_DATE__INT:
				return getCheckedOutDate((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_OCCUPIED_ROOMS__DATE:
				return getOccupiedRooms((Date)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_CHECK_INS__DATE:
				return getCheckIns((Date)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_CHECK_INS__DATE_DATE:
				return getCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___GET_CHECKED_OUT_BOOKINGS__DATE_DATE:
				return getCheckedOutBookings((Date)arguments.get(0), (Date)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___CHANGE_RESERVATION_DATE__INT_DATE_DATE:
				return changeReservationDate((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case QueryPackage.QUERY_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING_STRING_INT:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3));
			case QueryPackage.QUERY_CONTROLLER___CHANGE_NUMBER_OF_ROOMS__INT_ROOMTYPE_INT:
				return changeNumberOfRooms((Integer)arguments.get(0), (RoomType)arguments.get(1), (Integer)arguments.get(2));
			case QueryPackage.QUERY_CONTROLLER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___GET_NBR_OF_ROOMS__INT_STRING:
				return getNbrOfRooms((Integer)arguments.get(0), (String)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___CHECK_IN_BOOKING_WITH_ID_AND_ROOM_NBR__INT_INT:
				return checkInBookingWithIDAndRoomNbr((Integer)arguments.get(0), (Integer)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___GET_FREE_ROOMS_FOR_BOOKING_ID__INT:
				return getFreeRoomsForBookingID((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___ADD_EXTRA__INT_INT_STRING_DOUBLE:
				return addExtra((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case QueryPackage.QUERY_CONTROLLER___LIST_BOOKINGS:
				return listBookings();
			case QueryPackage.QUERY_CONTROLLER___STARTUP:
				startup();
				return null;
			case QueryPackage.QUERY_CONTROLLER___RESTART:
				restart();
				return null;
			case QueryPackage.QUERY_CONTROLLER___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case QueryPackage.QUERY_CONTROLLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case QueryPackage.QUERY_CONTROLLER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case QueryPackage.QUERY_CONTROLLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case QueryPackage.QUERY_CONTROLLER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case QueryPackage.QUERY_CONTROLLER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case QueryPackage.QUERY_CONTROLLER___GET_BOOKING_RESERVATIONS__INT:
				return getBookingReservations((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	private EList<ReportDTO> createReport(EList<Reservation> reservations){
		//Populate the report using the reservations.
		EList<ReportDTO> toReturn = new BasicEList<ReportDTO>();
		if (reservations.size() == 0) {
			return toReturn;
		}
		for (Reservation res : reservations){
			int bookingID = bookingcontroller.getBookingByReservation(res.getId()).getId();
			//Check if the booking already exists in the return list, if so add the room to that object.
			boolean bookingExists = false;
			for (ReportDTO reportObj : toReturn){
				if (reportObj.getBookings() == bookingID){
					reportObj.getRooms().add(res.getRoomNbr());
					bookingExists = true;
					break;
				}
			}
			//Else create a new report object.
			if (!bookingExists){
				ReportDTO temp = QueryFactory.eINSTANCE.createReportDTO();
				temp.setBookings(bookingID);
				temp.getRooms().add(res.getRoomNbr());
				toReturn.add(temp);
			}

		}
		return toReturn;
	}

	private Date createDate(String dateString){
		DateFormat format = new SimpleDateFormat("yyMMdd");

        Date date = new Date();

        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            date = now.getTime();
        }
        return date;
	}
	
	private int getNoOfReservedRooms(Date from, Date to, String roomTypedesc){
		EList<Booking> bookings = bookingcontroller.getBooking();
		EList<Booking> sortedBookings = new BasicEList<Booking>();
		sortedBookings.addAll(bookings);
		sortedBookings.sort(new BookingDateComparator());
		int maxReservedRooms = 0;
		int counter = 0;
		Date temp = null;
		for (Booking book : sortedBookings){
			if (book.getReservedFrom().before(to) && !(book.getReservedTo().before(from) || book.getReservedTo().equals(from))){
				if (temp == null){
					//This is the first relevant booking
					counter = getOpenReservations(book, roomTypedesc);
					maxReservedRooms = Integer.max(counter, maxReservedRooms);
					temp = book.getReservedTo();
				} else if (book.getReservedFrom().after(temp)){
					//New interval
					maxReservedRooms = Integer.max(counter, maxReservedRooms);
					counter = getOpenReservations(book, roomTypedesc);
					temp = book.getReservedTo();
				} else if (book.getReservedFrom().before(temp) || book.getReservedFrom().equals(temp)){
					//The booking exist during the interval.
					counter += getOpenReservations(book, roomTypedesc);
					maxReservedRooms = Integer.max(counter, maxReservedRooms);
				} else {
					break; //All remaining bookings are non relevant as they are after the interval
				}
			}

		}
		return maxReservedRooms;
	}
	
	private int getOpenReservations(Booking booking, String roomTypeDesc){
		int toReturn = 0;
		for (int reservation : booking.getReservations()){
			Reservation res = reservationcontroller.getReservation(reservation);
			if (res.getStatus() == ReservationStatus.OPEN && 
					res.getReservedRoomType().equals(roomTypeDesc)){
				toReturn++;
			}
		}
		return toReturn;
	}
	
	private Date cleanDateToDay(Date dateToClean){
		Date newDate;
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateToClean);
	
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		newDate = cal.getTime();
		return newDate;
	}
	
	private class BookingDateComparator implements Comparator<Booking>{

		@Override
		public int compare(Booking arg0, Booking arg1) {
			if (arg0.getReservedTo().before(arg1.getReservedTo())){
				return -1;
			} else if (arg0.getReservedTo().after(arg1.getReservedTo())){
				return 1;
			} else return 0;
		}
		
	}

} //QueryControllerImpl
