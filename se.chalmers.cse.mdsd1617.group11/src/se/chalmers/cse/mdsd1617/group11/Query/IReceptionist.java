/**
 */
package se.chalmers.cse.mdsd1617.group11.Query;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group11.Booking.Booking;
import se.chalmers.cse.mdsd1617.group11.Room.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IReceptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryPackage#getIReceptionist()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IReceptionist extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<Booking> getConfirmedBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	EList<String> getRoomTypes(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	Date getCheckedInDate(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	Date getCheckedOutDate(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<ReportDTO> getOccupiedRooms(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" checkInDateRequired="true" checkInDateOrdered="false"
	 * @generated
	 */
	EList<ReportDTO> getCheckIns(Date checkInDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" checkInDateStartRequired="true" checkInDateStartOrdered="false" checkInDateEndRequired="true" checkInDateEndOrdered="false"
	 * @generated
	 */
	EList<ReportDTO> getCheckIns(Date checkInDateStart, Date checkInDateEnd);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromRequired="true" fromOrdered="false" toRequired="true" toOrdered="false"
	 * @generated
	 */
	EList<ReportDTO> getCheckedOutBookings(Date from, Date to);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	EList<Integer> checkInBooking(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" newCheckInDateRequired="true" newCheckInDateOrdered="false" newCheckOutDateRequired="true" newCheckOutDateOrdered="false"
	 * @generated
	 */
	boolean changeReservationDate(int bookingID, Date newCheckInDate, Date newCheckOutDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" oldRoomTypeRequired="true" oldRoomTypeOrdered="false" newRoomTypeRequired="true" newRoomTypeOrdered="false" nbrRoomsRequired="true" nbrRoomsOrdered="false"
	 * @generated
	 */
	boolean changeRoomType(int bookingID, String oldRoomType, String newRoomType, int nbrRooms);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeRequired="true" roomTypeOrdered="false" nbrOfRoomsRequired="true" nbrOfRoomsOrdered="false"
	 * @generated
	 */
	boolean changeNumberOfRooms(int bookingID, RoomType roomType, int nbrOfRooms);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean cancelBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int getNbrOfRooms(int bookingID, String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" idRequired="true" idOrdered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean checkInBookingWithIDAndRoomNbr(int id, int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" idRequired="true" idOrdered="false"
	 * @generated
	 */
	EList<Integer> getFreeRoomsForBookingID(int id);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomRequired="true" roomOrdered="false" extraDescriptionRequired="true" extraDescriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	boolean addExtra(int bookingID, int room, String extraDescription, double price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<ReportBookingDTO> listBookings();

} // IReceptionist
