/**
 */
package se.chalmers.cse.mdsd1617.group11.Query;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryFactory
 * @model kind="package"
 * @generated
 */
public interface QueryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Query";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group11/Query.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group11.Query";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	QueryPackage eINSTANCE = se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist <em>IReceptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIReceptionist()
	 * @generated
	 */
	int IRECEPTIONIST = 0;

	/**
	 * The number of structural features of the '<em>IReceptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CONFIRMED_BOOKINGS = 0;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_ROOM_TYPES__INT = 1;

	/**
	 * The operation id for the '<em>Get Checked In Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CHECKED_IN_DATE__INT = 2;

	/**
	 * The operation id for the '<em>Get Checked Out Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CHECKED_OUT_DATE__INT = 3;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_OCCUPIED_ROOMS__DATE = 4;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CHECK_INS__DATE = 5;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CHECK_INS__DATE_DATE = 6;

	/**
	 * The operation id for the '<em>Get Checked Out Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_CHECKED_OUT_BOOKINGS__DATE_DATE = 7;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHECK_IN_BOOKING__INT = 8;

	/**
	 * The operation id for the '<em>Change Reservation Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHANGE_RESERVATION_DATE__INT_DATE_DATE = 9;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING_INT = 10;

	/**
	 * The operation id for the '<em>Change Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHANGE_NUMBER_OF_ROOMS__INT_ROOMTYPE_INT = 11;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CANCEL_BOOKING__INT = 12;

	/**
	 * The operation id for the '<em>Get Nbr Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_NBR_OF_ROOMS__INT_STRING = 13;

	/**
	 * The operation id for the '<em>Check In Booking With ID And Room Nbr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___CHECK_IN_BOOKING_WITH_ID_AND_ROOM_NBR__INT_INT = 14;

	/**
	 * The operation id for the '<em>Get Free Rooms For Booking ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___GET_FREE_ROOMS_FOR_BOOKING_ID__INT = 15;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_DOUBLE = 16;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST___LIST_BOOKINGS = 17;

	/**
	 * The number of operations of the '<em>IReceptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_OPERATION_COUNT = 18;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl <em>Report DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getReportDTO()
	 * @generated
	 */
	int REPORT_DTO = 1;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_DTO__BOOKINGS = 0;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_DTO__ROOMS = 1;

	/**
	 * The number of structural features of the '<em>Report DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_DTO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Report DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getQueryController()
	 * @generated
	 */
	int QUERY_CONTROLLER = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl <em>Report Booking DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getReportBookingDTO()
	 * @generated
	 */
	int REPORT_BOOKING_DTO = 2;

	/**
	 * The feature id for the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO__FROM = 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO__TO = 2;

	/**
	 * The feature id for the '<em><b>Roommapentry</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO__ROOMMAPENTRY = 3;

	/**
	 * The number of structural features of the '<em>Report Booking DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Report Booking DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_BOOKING_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.bookingReportMapEntryImpl <em>booking Report Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.bookingReportMapEntryImpl
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getbookingReportMapEntry()
	 * @generated
	 */
	int BOOKING_REPORT_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_REPORT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_REPORT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>booking Report Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_REPORT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>booking Report Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_REPORT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The feature id for the '<em><b>Reservationcontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER__RESERVATIONCONTROLLER = IRECEPTIONIST_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookingcontroller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER__BOOKINGCONTROLLER = IRECEPTIONIST_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Iqueryroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER__IQUERYROOM = IRECEPTIONIST_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Icustomerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER__ICUSTOMERPROVIDES = IRECEPTIONIST_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER_FEATURE_COUNT = IRECEPTIONIST_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CONFIRMED_BOOKINGS = IRECEPTIONIST___GET_CONFIRMED_BOOKINGS;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_ROOM_TYPES__INT = IRECEPTIONIST___GET_ROOM_TYPES__INT;

	/**
	 * The operation id for the '<em>Get Checked In Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CHECKED_IN_DATE__INT = IRECEPTIONIST___GET_CHECKED_IN_DATE__INT;

	/**
	 * The operation id for the '<em>Get Checked Out Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CHECKED_OUT_DATE__INT = IRECEPTIONIST___GET_CHECKED_OUT_DATE__INT;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_OCCUPIED_ROOMS__DATE = IRECEPTIONIST___GET_OCCUPIED_ROOMS__DATE;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CHECK_INS__DATE = IRECEPTIONIST___GET_CHECK_INS__DATE;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CHECK_INS__DATE_DATE = IRECEPTIONIST___GET_CHECK_INS__DATE_DATE;

	/**
	 * The operation id for the '<em>Get Checked Out Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_CHECKED_OUT_BOOKINGS__DATE_DATE = IRECEPTIONIST___GET_CHECKED_OUT_BOOKINGS__DATE_DATE;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHECK_IN_BOOKING__INT = IRECEPTIONIST___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Change Reservation Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHANGE_RESERVATION_DATE__INT_DATE_DATE = IRECEPTIONIST___CHANGE_RESERVATION_DATE__INT_DATE_DATE;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHANGE_ROOM_TYPE__INT_STRING_STRING_INT = IRECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING_INT;

	/**
	 * The operation id for the '<em>Change Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHANGE_NUMBER_OF_ROOMS__INT_ROOMTYPE_INT = IRECEPTIONIST___CHANGE_NUMBER_OF_ROOMS__INT_ROOMTYPE_INT;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CANCEL_BOOKING__INT = IRECEPTIONIST___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Nbr Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_NBR_OF_ROOMS__INT_STRING = IRECEPTIONIST___GET_NBR_OF_ROOMS__INT_STRING;

	/**
	 * The operation id for the '<em>Check In Booking With ID And Room Nbr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHECK_IN_BOOKING_WITH_ID_AND_ROOM_NBR__INT_INT = IRECEPTIONIST___CHECK_IN_BOOKING_WITH_ID_AND_ROOM_NBR__INT_INT;

	/**
	 * The operation id for the '<em>Get Free Rooms For Booking ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_FREE_ROOMS_FOR_BOOKING_ID__INT = IRECEPTIONIST___GET_FREE_ROOMS_FOR_BOOKING_ID__INT;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___ADD_EXTRA__INT_INT_STRING_DOUBLE = IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___LIST_BOOKINGS = IRECEPTIONIST___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___STARTUP = IRECEPTIONIST_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___RESTART = IRECEPTIONIST_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_FREE_ROOMS__INT_STRING_STRING = IRECEPTIONIST_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IRECEPTIONIST_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___ADD_ROOM_TO_BOOKING__STRING_INT = IRECEPTIONIST_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CONFIRM_BOOKING__INT = IRECEPTIONIST_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___INITIATE_CHECKOUT__INT = IRECEPTIONIST_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IRECEPTIONIST_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___INITIATE_ROOM_CHECKOUT__INT_INT = IRECEPTIONIST_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IRECEPTIONIST_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___CHECK_IN_ROOM__STRING_INT = IRECEPTIONIST_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Get Booking Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER___GET_BOOKING_RESERVATIONS__INT = IRECEPTIONIST_OPERATION_COUNT + 11;

	/**
	 * The number of operations of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_CONTROLLER_OPERATION_COUNT = IRECEPTIONIST_OPERATION_COUNT + 12;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController <em>IStartup Query Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController
	 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIStartupQueryController()
	 * @generated
	 */
	int ISTARTUP_QUERY_CONTROLLER = 6;

	/**
	 * The number of structural features of the '<em>IStartup Query Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTARTUP_QUERY_CONTROLLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTARTUP_QUERY_CONTROLLER___STARTUP = 0;

	/**
	 * The operation id for the '<em>Restart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTARTUP_QUERY_CONTROLLER___RESTART = 1;

	/**
	 * The number of operations of the '<em>IStartup Query Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTARTUP_QUERY_CONTROLLER_OPERATION_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist <em>IReceptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IReceptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist
	 * @generated
	 */
	EClass getIReceptionist();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getConfirmedBookings() <em>Get Confirmed Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Confirmed Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getConfirmedBookings()
	 * @generated
	 */
	EOperation getIReceptionist__GetConfirmedBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getRoomTypes(int) <em>Get Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getRoomTypes(int)
	 * @generated
	 */
	EOperation getIReceptionist__GetRoomTypes__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedInDate(int) <em>Get Checked In Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked In Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedInDate(int)
	 * @generated
	 */
	EOperation getIReceptionist__GetCheckedInDate__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedOutDate(int) <em>Get Checked Out Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked Out Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedOutDate(int)
	 * @generated
	 */
	EOperation getIReceptionist__GetCheckedOutDate__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getOccupiedRooms(java.util.Date) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__GetOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckIns(java.util.Date) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckIns(java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__GetCheckIns__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckIns(java.util.Date, java.util.Date) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__GetCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedOutBookings(java.util.Date, java.util.Date) <em>Get Checked Out Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked Out Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getCheckedOutBookings(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__GetCheckedOutBookings__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#checkInBooking(int)
	 * @generated
	 */
	EOperation getIReceptionist__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeReservationDate(int, java.util.Date, java.util.Date) <em>Change Reservation Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Reservation Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeReservationDate(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIReceptionist__ChangeReservationDate__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeRoomType(int, java.lang.String, java.lang.String, int) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeRoomType(int, java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getIReceptionist__ChangeRoomType__int_String_String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeNumberOfRooms(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType, int) <em>Change Number Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Number Of Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#changeNumberOfRooms(int, se.chalmers.cse.mdsd1617.group11.Room.RoomType, int)
	 * @generated
	 */
	EOperation getIReceptionist__ChangeNumberOfRooms__int_RoomType_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#cancelBooking(int)
	 * @generated
	 */
	EOperation getIReceptionist__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getNbrOfRooms(int, java.lang.String) <em>Get Nbr Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Nbr Of Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getNbrOfRooms(int, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionist__GetNbrOfRooms__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#checkInBookingWithIDAndRoomNbr(int, int) <em>Check In Booking With ID And Room Nbr</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking With ID And Room Nbr</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#checkInBookingWithIDAndRoomNbr(int, int)
	 * @generated
	 */
	EOperation getIReceptionist__CheckInBookingWithIDAndRoomNbr__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getFreeRoomsForBookingID(int) <em>Get Free Rooms For Booking ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms For Booking ID</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#getFreeRoomsForBookingID(int)
	 * @generated
	 */
	EOperation getIReceptionist__GetFreeRoomsForBookingID__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#addExtra(int, int, java.lang.String, double) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#addExtra(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIReceptionist__AddExtra__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist#listBookings()
	 * @generated
	 */
	EOperation getIReceptionist__ListBookings();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO <em>Report DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Report DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportDTO
	 * @generated
	 */
	EClass getReportDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bookings</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getBookings()
	 * @see #getReportDTO()
	 * @generated
	 */
	EAttribute getReportDTO_Bookings();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportDTO#getRooms()
	 * @see #getReportDTO()
	 * @generated
	 */
	EAttribute getReportDTO_Rooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController
	 * @generated
	 */
	EClass getQueryController();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getReservationcontroller <em>Reservationcontroller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reservationcontroller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController#getReservationcontroller()
	 * @see #getQueryController()
	 * @generated
	 */
	EReference getQueryController_Reservationcontroller();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingcontroller <em>Bookingcontroller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookingcontroller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingcontroller()
	 * @see #getQueryController()
	 * @generated
	 */
	EReference getQueryController_Bookingcontroller();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIqueryroom <em>Iqueryroom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iqueryroom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIqueryroom()
	 * @see #getQueryController()
	 * @generated
	 */
	EReference getQueryController_Iqueryroom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIcustomerprovides <em>Icustomerprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Icustomerprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController#getIcustomerprovides()
	 * @see #getQueryController()
	 * @generated
	 */
	EReference getQueryController_Icustomerprovides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingReservations(int) <em>Get Booking Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.QueryController#getBookingReservations(int)
	 * @generated
	 */
	EOperation getQueryController__GetBookingReservations__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO <em>Report Booking DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Report Booking DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO
	 * @generated
	 */
	EClass getReportBookingDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getBookingID <em>Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getBookingID()
	 * @see #getReportBookingDTO()
	 * @generated
	 */
	EAttribute getReportBookingDTO_BookingID();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getFrom()
	 * @see #getReportBookingDTO()
	 * @generated
	 */
	EAttribute getReportBookingDTO_From();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getTo()
	 * @see #getReportBookingDTO()
	 * @generated
	 */
	EAttribute getReportBookingDTO_To();

	/**
	 * Returns the meta object for the map '{@link se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getRoommapentry <em>Roommapentry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Roommapentry</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.ReportBookingDTO#getRoommapentry()
	 * @see #getReportBookingDTO()
	 * @generated
	 */
	EReference getReportBookingDTO_Roommapentry();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>booking Report Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>booking Report Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true" keyOrdered="false"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject" valueRequired="true" valueOrdered="false"
	 * @generated
	 */
	EClass getbookingReportMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getbookingReportMapEntry()
	 * @generated
	 */
	EAttribute getbookingReportMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getbookingReportMapEntry()
	 * @generated
	 */
	EAttribute getbookingReportMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController <em>IStartup Query Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IStartup Query Controller</em>'.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController
	 * @generated
	 */
	EClass getIStartupQueryController();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController#startup() <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController#startup()
	 * @generated
	 */
	EOperation getIStartupQueryController__Startup();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController#restart() <em>Restart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Restart</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController#restart()
	 * @generated
	 */
	EOperation getIStartupQueryController__Restart();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	QueryFactory getQueryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IReceptionist <em>IReceptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.IReceptionist
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIReceptionist()
		 * @generated
		 */
		EClass IRECEPTIONIST = eINSTANCE.getIReceptionist();

		/**
		 * The meta object literal for the '<em><b>Get Confirmed Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CONFIRMED_BOOKINGS = eINSTANCE.getIReceptionist__GetConfirmedBookings();

		/**
		 * The meta object literal for the '<em><b>Get Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_ROOM_TYPES__INT = eINSTANCE.getIReceptionist__GetRoomTypes__int();

		/**
		 * The meta object literal for the '<em><b>Get Checked In Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CHECKED_IN_DATE__INT = eINSTANCE.getIReceptionist__GetCheckedInDate__int();

		/**
		 * The meta object literal for the '<em><b>Get Checked Out Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CHECKED_OUT_DATE__INT = eINSTANCE.getIReceptionist__GetCheckedOutDate__int();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_OCCUPIED_ROOMS__DATE = eINSTANCE.getIReceptionist__GetOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CHECK_INS__DATE = eINSTANCE.getIReceptionist__GetCheckIns__Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CHECK_INS__DATE_DATE = eINSTANCE.getIReceptionist__GetCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Checked Out Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_CHECKED_OUT_BOOKINGS__DATE_DATE = eINSTANCE.getIReceptionist__GetCheckedOutBookings__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHECK_IN_BOOKING__INT = eINSTANCE.getIReceptionist__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Change Reservation Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHANGE_RESERVATION_DATE__INT_DATE_DATE = eINSTANCE.getIReceptionist__ChangeReservationDate__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING_INT = eINSTANCE.getIReceptionist__ChangeRoomType__int_String_String_int();

		/**
		 * The meta object literal for the '<em><b>Change Number Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHANGE_NUMBER_OF_ROOMS__INT_ROOMTYPE_INT = eINSTANCE.getIReceptionist__ChangeNumberOfRooms__int_RoomType_int();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CANCEL_BOOKING__INT = eINSTANCE.getIReceptionist__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Nbr Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_NBR_OF_ROOMS__INT_STRING = eINSTANCE.getIReceptionist__GetNbrOfRooms__int_String();

		/**
		 * The meta object literal for the '<em><b>Check In Booking With ID And Room Nbr</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___CHECK_IN_BOOKING_WITH_ID_AND_ROOM_NBR__INT_INT = eINSTANCE.getIReceptionist__CheckInBookingWithIDAndRoomNbr__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms For Booking ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___GET_FREE_ROOMS_FOR_BOOKING_ID__INT = eINSTANCE.getIReceptionist__GetFreeRoomsForBookingID__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___ADD_EXTRA__INT_INT_STRING_DOUBLE = eINSTANCE.getIReceptionist__AddExtra__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST___LIST_BOOKINGS = eINSTANCE.getIReceptionist__ListBookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl <em>Report DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.ReportDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getReportDTO()
		 * @generated
		 */
		EClass REPORT_DTO = eINSTANCE.getReportDTO();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_DTO__BOOKINGS = eINSTANCE.getReportDTO_Bookings();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_DTO__ROOMS = eINSTANCE.getReportDTO_Rooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryControllerImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getQueryController()
		 * @generated
		 */
		EClass QUERY_CONTROLLER = eINSTANCE.getQueryController();

		/**
		 * The meta object literal for the '<em><b>Reservationcontroller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUERY_CONTROLLER__RESERVATIONCONTROLLER = eINSTANCE.getQueryController_Reservationcontroller();

		/**
		 * The meta object literal for the '<em><b>Bookingcontroller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUERY_CONTROLLER__BOOKINGCONTROLLER = eINSTANCE.getQueryController_Bookingcontroller();

		/**
		 * The meta object literal for the '<em><b>Iqueryroom</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUERY_CONTROLLER__IQUERYROOM = eINSTANCE.getQueryController_Iqueryroom();

		/**
		 * The meta object literal for the '<em><b>Icustomerprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUERY_CONTROLLER__ICUSTOMERPROVIDES = eINSTANCE.getQueryController_Icustomerprovides();

		/**
		 * The meta object literal for the '<em><b>Get Booking Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation QUERY_CONTROLLER___GET_BOOKING_RESERVATIONS__INT = eINSTANCE.getQueryController__GetBookingReservations__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl <em>Report Booking DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.ReportBookingDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getReportBookingDTO()
		 * @generated
		 */
		EClass REPORT_BOOKING_DTO = eINSTANCE.getReportBookingDTO();

		/**
		 * The meta object literal for the '<em><b>Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_BOOKING_DTO__BOOKING_ID = eINSTANCE.getReportBookingDTO_BookingID();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_BOOKING_DTO__FROM = eINSTANCE.getReportBookingDTO_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_BOOKING_DTO__TO = eINSTANCE.getReportBookingDTO_To();

		/**
		 * The meta object literal for the '<em><b>Roommapentry</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPORT_BOOKING_DTO__ROOMMAPENTRY = eINSTANCE.getReportBookingDTO_Roommapentry();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.impl.bookingReportMapEntryImpl <em>booking Report Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.bookingReportMapEntryImpl
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getbookingReportMapEntry()
		 * @generated
		 */
		EClass BOOKING_REPORT_MAP_ENTRY = eINSTANCE.getbookingReportMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_REPORT_MAP_ENTRY__KEY = eINSTANCE.getbookingReportMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_REPORT_MAP_ENTRY__VALUE = eINSTANCE.getbookingReportMapEntry_Value();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController <em>IStartup Query Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group11.Query.IStartupQueryController
		 * @see se.chalmers.cse.mdsd1617.group11.Query.impl.QueryPackageImpl#getIStartupQueryController()
		 * @generated
		 */
		EClass ISTARTUP_QUERY_CONTROLLER = eINSTANCE.getIStartupQueryController();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISTARTUP_QUERY_CONTROLLER___STARTUP = eINSTANCE.getIStartupQueryController__Startup();

		/**
		 * The meta object literal for the '<em><b>Restart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISTARTUP_QUERY_CONTROLLER___RESTART = eINSTANCE.getIStartupQueryController__Restart();

	}

} //QueryPackage
