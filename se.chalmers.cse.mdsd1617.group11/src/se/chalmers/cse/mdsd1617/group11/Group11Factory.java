/**
 */
package se.chalmers.cse.mdsd1617.group11;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group11.Group11Package
 * @generated
 */
public interface Group11Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group11Factory eINSTANCE = se.chalmers.cse.mdsd1617.group11.impl.Group11FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Free Room Types DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Free Room Types DTO</em>'.
	 * @generated
	 */
	FreeRoomTypesDTO createFreeRoomTypesDTO();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Group11Package getGroup11Package();

} //Group11Factory
