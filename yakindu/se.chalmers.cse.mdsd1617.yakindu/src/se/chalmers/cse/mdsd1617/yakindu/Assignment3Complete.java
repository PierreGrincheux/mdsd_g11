package se.chalmers.cse.mdsd1617.yakindu;

import java.util.HashMap;
import java.util.Map;

public class Assignment3Complete {
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;

	private final int MAX_ROOMS = 2;
	
	private final int FREE = 0;
	private final int ASSIGNED = 1;
	private final int OCCUPIED = 2;
	
	private Map<Long,Boolean> confirmedBookings = new HashMap<Long,Boolean>();
	private long[] reservations = new long[MAX_ROOMS];
	private boolean[] reservationsCheckedIn = new boolean[MAX_ROOMS];
	private long[] rooms = new long[MAX_ROOMS];
	private boolean[] payed = new boolean[MAX_ROOMS];
	private boolean[] reservationsCheckedOut = new boolean[MAX_ROOMS];

	public long initiateBooking() {
		return ++currentBookingId;
	}

	public boolean addRoomToBooking(long bookingId) {
		if (bookingId < 1 || bookingId > currentBookingId) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		}  else if (currentReservationNumber > 1) {
			return false;
		} else {
			for(int i = 0; i< MAX_ROOMS; i++) {
				if(reservations[i] == 0) {
					reservations[i] = bookingId;
					break;
				}
			}
			++currentReservationNumber;
			return true;
		}
	}
	
	public boolean checkInBooking(long id) {
		boolean result = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == id){
				reservationsCheckedIn[i] = true;
				result = true;
			}
		}
		return result;
	}
	
	public boolean checkInReservation(long id) {
		if(!reservationsCheckedIn[(int) id]) {
			reservationsCheckedIn[(int) id] = true;
			return true;
		}
		return false;
	}
	public boolean checkOutReservation(long id){
		if(reservationsCheckedIn[(int) id]){
			reservationsCheckedOut[(int) id] = true;
			return true;
		}
		return false;
	}
	
	public boolean finalizeReservation(long id){
		if(reservations[(int) id]!=0){
			reservations[(int) id] = 0;
			//--currentReservationNumber;
			return true;
		}
		return false;
	}
	
	public boolean initiateCheckout(long id) {
		boolean result = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == id){
				reservationsCheckedOut[i] = true;
				result = true;
			}
		}
		return result;
	}
	public boolean payDuringCheckout(long id) {
		boolean result = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == id){
				 reservations[i] = 0;
				result = true;
				//--currentReservationNumber;
			}
		}
		return result;
	}
	public long getReservationForBookingId(long id) {
		for(int i = 0; i< MAX_ROOMS; i++) {
			if(reservations[i] == id && !reservationsCheckedIn[i]) {
				return (long) i;
			}
		}
		return (long) -1;
	}
	
	public long getBookingIdForReservationId(long id) {
		return reservations[(int) id];
	}
	
	public boolean isReservationCheckIn(long id) {
		return reservationsCheckedIn[(int) id];
	}
	
	public boolean assignRoom() {
		for(int i = 0; i< MAX_ROOMS; i++) {
			if(rooms[i] == FREE) {
				rooms[i] = ASSIGNED;	
				return true;
			}
		}
		return false;
	}
	
	public long occupyRoom() {
		for(int i = 0; i< MAX_ROOMS; i++) {
			if(rooms[i] == ASSIGNED) {
				rooms[i] = OCCUPIED;	
				return i;
			}
		}
		return -1;
	}
	public void freeRoom(long id) {
		rooms[(int) id] = FREE;
	}
	
	public long getStatus(long id) {
		return (long) rooms[(int) id];
	}
	
	public boolean isAllCheckedIn(long bookingId){
		boolean result = true;
		boolean found = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == bookingId){
				result = reservationsCheckedIn[i] && result ;
				found = true;
			}
		}
		return result && found;
	}
	public boolean isAllCheckedOut(long bookingId){
		boolean result = true;
		boolean found = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == bookingId){
				result = reservationsCheckedOut[i] && result ;
				found = true;
			}
		}
		return result && found;
	}
	
	public boolean setBookingConfirmed(long bookingId){
		confirmedBookings.put((Long)bookingId, true);
		return true;
	}
	
	public boolean isConfirmed(long id){
		if (confirmedBookings.get(reservations[(int)id])==null){
			return false;
		}else{
			return true;
		}
	}
	
	public boolean isAllPayed(long bookingId){
		boolean result = true;
		boolean found = false;
		for(int i = 0; i<MAX_ROOMS; i++) {
			 if (reservations[i] == bookingId){
				result = payed[i] && result ;
				found = true;
			}
		}
		return result && found;
	}
	
	public boolean payForReservation(long id) {
		if(!payed[(int) id]) {
			payed[(int) id] = true;
			return true;
		}
		return false;
	}
	public boolean isReservationCheckOut(long id) {
		return reservationsCheckedOut[(int) id];
	}
	
	
}
